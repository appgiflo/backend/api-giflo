FROM maven:3-jdk-8-alpine
VOLUME /tmp
ADD ./target/api-giflo-0.0.1-SNAPSHOT.jar api-giflo.jar
ENTRYPOINT [ "java", "-Xmx300m","-jar", "/api-giflo.jar" ]
