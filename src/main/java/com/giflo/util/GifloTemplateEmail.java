package com.giflo.util;

import java.util.HashMap;

import com.giflo.model.enums.TemplateEmailEnum;

public class GifloTemplateEmail {

	/**
	 * Encryption
	 *
	 * @param content
	 * @param key
	 * @return
	 */
	public static String buildEmail(HashMap<String, String> data, TemplateEmailEnum id) {
		StringBuilder b = new StringBuilder();
		b.append(GifloTemplateEmail.contenedorAbre());
		switch (id) {
		case REGISTRO:
			String links = data.get("links");
			b.append(GifloTemplateEmail.titulo("Bienvenido a Giflo"));
			b.append(GifloTemplateEmail.saludo(String.format("Saludos %1s", data.get("usuario"))));
			b.append(GifloTemplateEmail.contenido("Confirmamos su registro en nuestra plataforma"));
			b.append(GifloTemplateEmail.contenido(
					"Ahora ya podrás dar a conocer tu negocio y establecer brechas comerciales con demás empresas registradas en nuestra plataforma."));

			if (!links.isEmpty()) {
				b.append("<br/>");
				b.append(GifloTemplateEmail.contenido("Te sugerimos ver los siguientes videos de como usar el app"));
				String[] linksArray = links.split(",");
				for (int i = 0; i < linksArray.length; i++) {
					b.append(GifloTemplateEmail.boton(linksArray[i], linksArray[i]));
				}
			}
			break;
		case CAMBIO_CLAVE:
			b.append(GifloTemplateEmail.titulo("Recuperación de Contraseña"));
			b.append(GifloTemplateEmail.saludo(String.format("Saludos %1s", data.get("usuario"))));
			b.append(GifloTemplateEmail.contenido(String.format(
					"Para recuperar tu contraseña debes ingresar con la siguiente clave temporal <strong>%1s</strong> valida hasta %2s",
					data.get("clave"), data.get("tiempo"))));
			break;
		case ACTUALIZACION:
			b.append(GifloTemplateEmail.titulo("Actualización de Datos"));
			b.append(GifloTemplateEmail.saludo(String.format("Saludos %1s", data.get("usuario"))));
			b.append(GifloTemplateEmail.contenido("Se ha actualizado tus datos exitosamente"));
			b.append(GifloTemplateEmail.contenido("Si tú no lo has realizado, comunícate con nosotros."));
			break;
		case ENVIO_OTP:
			b.append(GifloTemplateEmail.titulo("Código de Verificación"));
			b.append(GifloTemplateEmail.saludo("Saludos"));
			b.append(GifloTemplateEmail.contenido(String.format(
					"Copie este código <strong style='font-size:20px;'>%1s</strong> e ingrese en la aplicación para continuar con su registro. Válida hasta %2s",
					data.get("clave"), data.get("tiempo"))));
			b.append(GifloTemplateEmail.contenido("Si tú no lo has realizado, comunícate con nosotros"));
			break;
		case CAMBIO_PASS_EXITO:
			b.append(GifloTemplateEmail.titulo("Confirmación de cambio de Contraseña"));
			b.append(GifloTemplateEmail.saludo(String.format("Saludos %1s", data.get("usuario"))));
			b.append(GifloTemplateEmail.contenido("Tú contraseña de actualizo correctamente"));
			b.append(GifloTemplateEmail.contenido("Si tú no lo has realizado, comunícate con nosotros"));
		default:
			break;
		}
		b.append(GifloTemplateEmail.footer1());
		b.append(GifloTemplateEmail.footer2());
		b.append(GifloTemplateEmail.contenedorCierra());
		return b.toString();

	}

	static String titulo(String valor) {
		if (valor != null && !valor.isEmpty())
			return String.format("<h1 style=\"text-align: center;padding: 10px;font-family: monospace;\">%1s</h1>",
					valor);
		return "";
	}

	static String saludo(String valor) {
		if (valor != null && !valor.isEmpty())
			return String.format("<div style=\"padding: 0px 20px;font-family: system-ui; color: #363636;\">%1s</div>",
					valor);
		return "";
	}

	static String contenido(String valor) {
		if (valor != null && !valor.isEmpty())
			return String.format("<div style=\"padding: 10px 20px;font-family: system-ui; color: #363636;\">%1s</div>",
					valor);
		return "";
	}

	static String boton(String texto, String url) {
		if (texto != null && !texto.isEmpty())
			return String.format(
					"<div style=\"padding: 20px 20px;\"><a href=\"%1s\" style=\"background-color: #680281;display: block;width: auto;text-decoration: none;color: #ffffff;font-size: 17px;font-family: ArialMT,Arial,sans-serif;font-weight: normal;border-radius: 5px;padding: 12px 10px;margin: 0;text-align: center;\">%2s</a></div>",
					url, texto);
		return "";
	}

	static String footer1() {
		return "<div style=\"border-top: 1px solid #f14c4c;padding: 0px 20px;border-spacing: 0;color: #363636;font-family: ArialMT,Arial,sans-serif;font-size: 15px;width: auto;font-size: 12px;\">Giflo</div>";
	}

	static String footer2() {
		return "<div style=\"padding:0px 0px 20px 20px;border-spacing: 0;color: #363636;font-family: ArialMT,Arial,sans-serif;font-size: 15px;width: auto;font-size: 12px;\">\"Siempre innovando\"</div>";
	}

	static String contenedorAbre() {
		return "<div style=\"margin: 2vh 8vw; border: 0px solid #1C6EA4;border-radius: 15px;-webkit-box-shadow: 0px 0px 37px 12px rgba(210,160,160,0.78);box-shadow: 0px 0px 37px 12px rgba(210,160,160,0.78);background: #fd9a9a;background: -moz-linear-gradient(-45deg, #fd9a9a 0%, #FFE5F2 80%, #FFFBF6 100%);background: -webkit-linear-gradient(-45deg, #fd9a9a 0%, #FFE5F2 80%, #FFFBF6 100%);background: linear-gradient(135deg, #fd9a9a 0%, #FFE5F2 80%, #FFFBF6 100%);width: auto;\">";
	}

	static String contenedorCierra() {
		return "</div>";
	}

}
