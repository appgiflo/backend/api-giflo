package com.giflo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giflo.model.dto.RegistroDTO;
import com.giflo.model.dto.UsuarioDTO;
import com.giflo.view.service.IUsuarioService;

@RestController
public class UsuarioController {

	@Autowired
	private IUsuarioService iUsuarioService;

	@GetMapping("/rest/user/find/{username}")
	public UsuarioDTO findByCorreoAndActive(@PathVariable("username") String username) {
		try {
			return iUsuarioService.findByUsername(username);
		} catch (Exception e) {
			return null;
		}
	}
	@GetMapping("/rest/user/validtoken")
	public String validtoken(@RequestParam("device") String device) {
		try {
			return iUsuarioService.validToken(device);
		} catch (Exception e) {
			return null;
		}
	}

	@GetMapping("/rest/user/findid")
	public ResponseEntity<?> getInfo(@RequestParam("idUser") Integer idUser, @RequestParam("token") String token,
			@RequestParam("device") String device) {
		try {
			return new ResponseEntity<>(iUsuarioService.findByIdCustom(idUser, token, device), HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.GONE);
		}
	}

	@GetMapping("/rest/user/token/{idUser}")
	public ResponseEntity<?> findToken(@PathVariable("idUser") Integer idUser) {
		try {
			return new ResponseEntity<>(iUsuarioService.findTokens(idUser), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.GONE);
		}
	}

	@PostMapping("/rest/user/register")
	public ResponseEntity<?> register(@RequestBody RegistroDTO register) {
		try {
			return new ResponseEntity<>(iUsuarioService.register(register), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.GONE);
		}
	}

	@PostMapping("/rest/user/update")
	public ResponseEntity<?> update(@RequestBody RegistroDTO register) {
		try {
			return new ResponseEntity<>(iUsuarioService.actualizar(register), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.GONE);
		}
	}

	@GetMapping("/rest/user/recover")
	public ResponseEntity<?> recuperarContrasenia(@RequestParam(value = "username") String username) {
		try {
			return new ResponseEntity<>(iUsuarioService.recuperarContrasenia(username), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.GONE);
		}
	}

	@GetMapping("/rest/user/passwordconfirm")
	public ResponseEntity<?> cambiarContrasenia(@RequestParam("username") String username,
			@RequestParam("password") String password) {
		try {
			return new ResponseEntity<>(iUsuarioService.cambiarContrasenia(username, password), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.GONE);
		}
	}

	@GetMapping("/rest/user/removedevice")
	public ResponseEntity<?> removeDevice(@RequestParam(value = "idUser") Integer idUser,
			@RequestParam(value = "device") String device) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(iUsuarioService.removeDevice(idUser, device));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/user/findByEmpresa/{idEmpresa}/{idUser}")
	public ResponseEntity<?> findByIdEmpresa(@PathVariable("idEmpresa") Integer idEmpresa,
			@PathVariable("idUser") Integer idUser) {
		try {
			return new ResponseEntity<>(iUsuarioService.findByIdEmpresa(idEmpresa, idUser), HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.GONE);
		}
	}

	
	
	@GetMapping("/rest/user/updateLocation")
	public ResponseEntity<?> updateLocation(@RequestParam("idUser") Integer idUser,
			@RequestParam("latitud") String latitud,@RequestParam("longitud") String longitud) {
		try {
			return new ResponseEntity<>(iUsuarioService.updateLocation(idUser, longitud, latitud), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.GONE);
		}
	}
	@GetMapping("/rest/user/search")
	public ResponseEntity<?> search(@RequestParam("filter") String filter,@RequestParam("idUser") Integer idUser) {
		try {
			return new ResponseEntity<>(iUsuarioService.search(filter, idUser), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.GONE);
		}
	}
	@GetMapping("/rest/user/searchactive")
	public ResponseEntity<?> searchActive(@RequestParam("filter") String filter) {
		try {
			return new ResponseEntity<>(iUsuarioService.searchActive(filter), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.GONE);
		}
	}
}
