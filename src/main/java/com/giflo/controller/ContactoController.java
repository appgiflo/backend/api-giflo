package com.giflo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giflo.view.service.IContactoService;

@RestController
public class ContactoController {

	@Autowired
	private IContactoService contactoService;

	@GetMapping("/rest/contacto/crear")
	public ResponseEntity<?> addByOrigenAndDestino(@RequestParam(value = "c1") Integer origen,
			@RequestParam(value = "c2") Integer destino) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(contactoService.crear(origen, destino));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/contacto/eliminar")
	public ResponseEntity<?> eliminar(@RequestParam(value = "c1") Integer origen,
			@RequestParam(value = "c2") Integer destino) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(contactoService.eliminar(origen, destino));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	

}
