package com.giflo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giflo.model.entity.Disponibilidad;
import com.giflo.model.enums.CatalogoEnum;
import com.giflo.view.service.IDisponibilidadService;

@RestController
public class DisponibilidadController {

	@Autowired
	private IDisponibilidadService disponibilidadService;

	@Autowired
	SimpMessagingTemplate template;

	@PostMapping("/rest/disponibilidad/guardar")
	public ResponseEntity<?> guardar(@RequestBody List<Disponibilidad> listD) {
		try {
			List<Disponibilidad> resultad = disponibilidadService.guardar(listD);
			template.convertAndSend("/topic/getDisponibilidadByTallo",
					disponibilidadService.getDisponibilidadPorTallos());
			template.convertAndSend("/topic/getDisponibilidadByBonches",
					disponibilidadService.getDisponibilidadPorBonches());
			return ResponseEntity.status(HttpStatus.CREATED).body(resultad);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/disponibilidad/obtener")
	public ResponseEntity<?> eliminar(@RequestParam(value = "c1") Integer idEmpresa,
			@RequestParam(value = "c2") CatalogoEnum unidadMedida) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED)
					.body(disponibilidadService.obtener(idEmpresa, unidadMedida));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/disponibilidad/obtener-por-tallo")
	public ResponseEntity<?> obtenerPorTallo(@RequestParam(value = "c1") Integer idVariedad,
			@RequestParam(value = "c2") Integer idPais) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED)
					.body(disponibilidadService.obtenerDisponiblidadDetallePorTallos(idVariedad, idPais));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/disponibilidad/obtener-por-bonche")
	public ResponseEntity<?> obtenerPorBonche(@RequestParam(value = "c1") Integer idVariedad,
			@RequestParam(value = "c2") Integer idPais, @RequestParam(value = "c3") Integer idLongitud) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED)
					.body(disponibilidadService.obtenerDisponiblidadDetallePorBonches(idVariedad, idPais, idLongitud));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/disponibilidad/disponibilidadHoy")
	public ResponseEntity<?> disponibilidadHoy(@RequestParam(value = "c1") Integer idEmpresa) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(disponibilidadService.disponibilidadHoy(idEmpresa));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
