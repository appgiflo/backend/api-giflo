package com.giflo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.giflo.model.dto.MessageDTO;
import com.giflo.view.service.IFirebaseMessageService;

@RestController
public class FirebaseMessageController {

	@Autowired
	private IFirebaseMessageService firebaseMessageService;

	
	
	@PutMapping("/rest/firebase/send")
	public ResponseEntity<?> sendNotification(@RequestBody MessageDTO messageDTO ) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(firebaseMessageService.sendPushMessage(messageDTO,null));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	
}
