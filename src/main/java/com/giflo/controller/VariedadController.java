package com.giflo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.giflo.model.entity.Variedad;
import com.giflo.view.service.IVariedadService;

@RestController
public class VariedadController {

	@Autowired
	private IVariedadService iVariedadService;

	@PostMapping("/rest/variedad/save")
	public ResponseEntity<?> registerSubscription(@RequestBody Variedad r) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(iVariedadService.save(r));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}


}
