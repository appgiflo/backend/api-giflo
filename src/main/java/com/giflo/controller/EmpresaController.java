package com.giflo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giflo.view.service.IEmpresaService;

@RestController
public class EmpresaController {
	@Autowired
	private IEmpresaService iEmpresaService;

	@GetMapping("/rest/empresa/lista/{idTipo}/{page}/{size}/")
	public ResponseEntity<?> findByTipoAndName(@PathVariable("idTipo") Integer idTipo, @RequestParam("q") String search,
			@RequestParam("order") String orderBy, @PathVariable("page") Integer page,
			@PathVariable("size") Integer size) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(iEmpresaService.findByTipoAndName(idTipo, search, orderBy, page, size));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/empresa/lista/map")
	public ResponseEntity<?> findByTypeNameMap() {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(iEmpresaService.findByTipoAndNameMap());
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
