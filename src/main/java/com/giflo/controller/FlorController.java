package com.giflo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giflo.model.entity.Flor;
import com.giflo.view.service.IFlorService;

@RestController
public class FlorController {

	@Autowired
	private IFlorService iFlorService;

	@GetMapping("/rest/flor/crear")
	public ResponseEntity<?> getInfo(@RequestParam("idEmpresa") Integer idEmpresa,
			@RequestParam("idVariedad") Integer idVariedad) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(iFlorService.crear(idEmpresa, idVariedad));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PutMapping("/rest/flor/{id}")
	public ResponseEntity<?> replaceEmployee(@RequestBody Flor flor, @PathVariable Integer id) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(iFlorService.actualziar(flor, id));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
}
