package com.giflo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giflo.view.service.IOtpService;

@RestController
public class OtpController {

	@Autowired
	private IOtpService iOtpService;

	@GetMapping("/rest/otp/enviar")
	public ResponseEntity<?> findByCorreoAndActive(@RequestParam(value = "c") String contacto) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(iOtpService.enviarOTP(contacto));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	
	@GetMapping("/rest/otp/validar")
	public ResponseEntity<?> findByCorreoAndActive(@RequestParam(value = "c1") String contacto,@RequestParam(value = "c2") String codigo) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(iOtpService.validarOTP(contacto, codigo));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	
}
