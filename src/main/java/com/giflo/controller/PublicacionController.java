package com.giflo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giflo.model.dto.PublicacionDTO;
import com.giflo.model.entity.Publicacion;
import com.giflo.view.service.IPublicacionDTOService;

@RestController
public class PublicacionController {
	@Autowired
	private IPublicacionDTOService iPublicacionDTOService;

	@GetMapping("/rest/publicacion/lista/{idTipoPublicacion}/{idColor}/{page}/{size}/")
	public ResponseEntity<?> findByColorAndPublicacion(@PathVariable("idTipoPublicacion") Integer idTipoPublicacion,
			@PathVariable("idColor") Integer idColor, @RequestParam("q") String search,
			@PathVariable("page") Integer page, @PathVariable("size") Integer size) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(
					iPublicacionDTOService.findByFlorAndPublicacion(idTipoPublicacion, idColor, search, page, size));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	@GetMapping("/rest/publicacion/filtro/{idTipoPublicacion}/{idColor}/{page}/{size}/{idTallo}/")
	public ResponseEntity<?> findByColorAndPublicacionAndLargo(@PathVariable("idTipoPublicacion") Integer idTipoPublicacion,
			@PathVariable("idColor") Integer idColor, @RequestParam("q") String search,
			@PathVariable("page") Integer page, @PathVariable("size") Integer size,@PathVariable("idTallo") Integer idTallo) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(
					iPublicacionDTOService.findByFlorAndPublicacionAndTallo(idTipoPublicacion, idColor, search, page, size,idTallo));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/publicacion/filter/{page}/{size}/")
	public ResponseEntity<?> findByFilter(@RequestParam("q") String search, @PathVariable("page") Integer page,
			@PathVariable("size") Integer size) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(iPublicacionDTOService.findByFilter(search, page, size));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/publicacion/lista/usuario/vigente/{idUsuario}/")
	public ResponseEntity<?> findByUsuarioVigente(@PathVariable("idUsuario") Integer idUsuario) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(iPublicacionDTOService.findByUsuarioVigente(idUsuario));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/publicacion/lista/usuario/all/{idUsuario}/")
	public ResponseEntity<?> findAllByUser(@PathVariable("idUsuario") Integer idUsuario) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(iPublicacionDTOService.findAllByUser(idUsuario));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/publicacion/lista/empresa/{idEmpresa}/{idPublicacion}/")
	public ResponseEntity<?> findByEmpresaVigente(@PathVariable("idEmpresa") Integer idEmpresa,
			@PathVariable("idPublicacion") Integer idPublicacion) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(iPublicacionDTOService.findByEmpresaVigente(idEmpresa, idPublicacion));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/rest/publicacion/crear")
	public ResponseEntity<?> crearPublicacion(@RequestBody List<Publicacion> ps) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(iPublicacionDTOService.crear(ps));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PutMapping("/rest/publicacion/actualizar/{id}")
	public ResponseEntity<?> activarInaPublicacion(@PathVariable("id") Integer id, @RequestBody PublicacionDTO p) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(iPublicacionDTOService.activarInactivar(id, p));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/publicacion/elimina/{idPublicacion}/")
	public ResponseEntity<?> findByEmpresaVigente(@PathVariable("idPublicacion") Integer idPublicacion) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(iPublicacionDTOService.eliminarPublicacion(idPublicacion));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	@GetMapping("/rest/publicacion/notificar/{idPublicacion}/")
	public ResponseEntity<?> enviarNotificacionPush(@PathVariable("idPublicacion") Integer idPublicacion) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(iPublicacionDTOService.enviarNotificacionPush(idPublicacion));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
