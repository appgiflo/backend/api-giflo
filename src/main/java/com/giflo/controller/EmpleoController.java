package com.giflo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giflo.model.entity.Empleo;
import com.giflo.view.service.IEmpleoService;

@RestController
public class EmpleoController {

	@Autowired
	private IEmpleoService empleoService;

	@PostMapping("/rest/empleo/save")
	public ResponseEntity<?> save(@RequestBody Empleo r) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(empleoService.save(r));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/empleo/status")
	public ResponseEntity<?> save(@RequestParam(value = "c1") Integer id, @RequestParam(value = "c2") String estado) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(empleoService.cambiarEstado(id, estado));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/empleo/notificar")
	public ResponseEntity<?> notificar(@RequestParam(value = "c1") Integer idEmpleo) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(empleoService.enviarNotificacion(idEmpleo));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/empleo/filterActive/{page}/{size}/")
	public ResponseEntity<?> filterActive(@RequestParam("q") String search, @PathVariable("page") Integer page,
			@PathVariable("size") Integer size) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(empleoService.filterActive(search, page, size));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/empleo/filterByUsuario/{page}/{size}/{idUsuario}")
	public ResponseEntity<?> filterByUsuario(@RequestParam("q") String search,
			@PathVariable("idUsuario") Integer idUsuario, @PathVariable("page") Integer page,
			@PathVariable("size") Integer size) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(empleoService.filterByUsuario(search, idUsuario, page, size));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
