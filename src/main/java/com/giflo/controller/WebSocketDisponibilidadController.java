package com.giflo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RestController;

import com.giflo.model.entity.Disponibilidad;
import com.giflo.view.service.IDisponibilidadService;

@RestController
public class WebSocketDisponibilidadController {

	@Autowired
	SimpMessagingTemplate template;

	@Autowired
	private IDisponibilidadService disponibilidadService;

	@MessageMapping("/verificarDisponiblidad")
	public void receiveMessage() {
		template.convertAndSend("/topic/getDisponibilidadByTallo", disponibilidadService.getDisponibilidadPorTallos());
		template.convertAndSend("/topic/getDisponibilidadByBonches",
				disponibilidadService.getDisponibilidadPorBonches());
	}

	@SendTo("/topic/getDisponibilidadByTallo")
	public List<Disponibilidad> getDisponibilidadByTallo(@Payload List<Disponibilidad> disponilidadList) {
		return disponilidadList;
	}

	@SendTo("/topic/getDisponibilidadByBonches")
	public List<Disponibilidad> getDisponibilidadByBonches(@Payload List<Disponibilidad> disponilidadList) {
		return disponilidadList;
	}

	@SendTo("/topic/getPriceByBonches")
	public List<Disponibilidad> getPriceByBonches(@Payload List<Disponibilidad> disponilidadList) {
		return disponilidadList;
	}

	@SendTo("/topic/getPriceByTallos")
	public List<Disponibilidad> getPriceByTallos(@Payload List<Disponibilidad> disponilidadList) {
		return disponilidadList;
	}
}
