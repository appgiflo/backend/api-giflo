package com.giflo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giflo.model.dto.PagoPlanDTO;
import com.giflo.view.service.IPagoPlanService;

@RestController
public class PagoSaldoController {

	@Autowired
	private IPagoPlanService iPagoPlanService;

	@PostMapping("/rest/pagoplan/register")
	public ResponseEntity<?> registerSubscription(@RequestBody PagoPlanDTO r) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(iPagoPlanService.registrarPago(r));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/pagoplan/recover")
	public ResponseEntity<?> recoverSubscription(@RequestParam(value = "c") Integer idUsuario) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(iPagoPlanService.obtenerPago(idUsuario));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/pagoplan/eliminar")
	public ResponseEntity<?> eliminarSubscription(@RequestParam(value = "c") Integer idPagoPlan) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(iPagoPlanService.eliminar(idPagoPlan));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/rest/pagoplan/state")
	public ResponseEntity<?> stateSubscription(@RequestBody PagoPlanDTO r) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(iPagoPlanService.stateSubscription(r));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/rest/pagoplan/coupon")
	public ResponseEntity<?> couponApplied(@RequestParam(value = "c1") Integer idPagoPlan,
			@RequestParam(value = "c2") String codigoCupon) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED)
					.body(iPagoPlanService.couponApplied(idPagoPlan, codigoCupon));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
