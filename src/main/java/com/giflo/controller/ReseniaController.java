package com.giflo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giflo.model.dto.ReseniaDTO;
import com.giflo.view.service.IReseniaService;

@RestController
public class ReseniaController {

	
	@Autowired
	private IReseniaService iReseniaService;

	@PostMapping("/rest/review/register")
	public ResponseEntity<?> registerReview(@RequestBody ReseniaDTO register) {
		try {
			return new ResponseEntity<>(iReseniaService.registerReview(register), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.GONE);
		}
	}

	
	@GetMapping("/rest/review/delete")
	public ResponseEntity<?> deleteReview(@RequestParam(value = "c1") Integer idResenia) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(iReseniaService.deleteReview(idResenia));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	
	@GetMapping("/rest/review/recover")
	public ResponseEntity<?> recoverReview(@RequestParam(value = "c1") Integer idEmpresa, @RequestParam(value = "c2") Integer idUsuario) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(iReseniaService.recoverReview(idEmpresa, idUsuario));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	
	@GetMapping("/rest/review/lista/{idEmpresa}/{page}/{size}/")
	public ResponseEntity<?> findByEmpresaResenia(@PathVariable("idEmpresa") Integer idEmpresa,
			@PathVariable("page") Integer page, @PathVariable("size") Integer size) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(
					iReseniaService.findByEmpresa(idEmpresa, page, size));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	
	
}
