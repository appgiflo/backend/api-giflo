package com.giflo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giflo.model.entity.Producto;
import com.giflo.view.service.IProductoService;

@RestController
public class ProductoController {

	@Autowired
	private IProductoService iProductoService;

	@PostMapping("/rest/producto/crear")
	public ResponseEntity<?> crearProducto(@RequestBody Producto p) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(iProductoService.guardar(p));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/producto/por-empresa")
	public ResponseEntity<?> eliminar(@RequestParam(value = "p") Integer idEmpresa) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(iProductoService.buscarPorEmpresa(idEmpresa));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/producto/lista/{page}/{size}/")
	public ResponseEntity<?> findByColorAndPublicacion(@RequestParam("q") String search,
			@PathVariable("page") Integer page, @PathVariable("size") Integer size) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(iProductoService.buscarPorFiltro(search, page, size));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
