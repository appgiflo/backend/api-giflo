package com.giflo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giflo.view.service.INotificacionPushService;

@RestController
public class NotificacionPushController {

	@Autowired
	private INotificacionPushService iNotificacionPushService;

	@GetMapping("/rest/notifypush/crear")
	public ResponseEntity<?> addByOrigenAndDestino(@RequestParam(value = "c1") Integer origen,
			@RequestParam(value = "c2") Integer destino) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(iNotificacionPushService.crear(origen, destino));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/notifypush/eliminar")
	public ResponseEntity<?> removeByOrigenAndDestino(@RequestParam(value = "c1") Integer origen,
			@RequestParam(value = "c2") Integer destino) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(iNotificacionPushService.eliminar(origen, destino));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/notifypush/by-origen")
	public ResponseEntity<?> findByOrigen(@RequestParam(value = "c1") Integer origen) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(iNotificacionPushService.findByOrigen(origen));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/notifypush/by-destino")
	public ResponseEntity<?> findByDestino(@RequestParam(value = "c1") Integer destino) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(iNotificacionPushService.findByDestino(destino));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
