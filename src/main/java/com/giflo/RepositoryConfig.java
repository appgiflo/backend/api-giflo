package com.giflo;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

import com.giflo.model.entity.Catalogo;
import com.giflo.model.entity.Contacto;
import com.giflo.model.entity.Cupon;
import com.giflo.model.entity.Disponibilidad;
import com.giflo.model.entity.Empresa;
import com.giflo.model.entity.EmpresaFoto;
import com.giflo.model.entity.Flor;
import com.giflo.model.entity.Parametro;
import com.giflo.model.entity.Perfil;
import com.giflo.model.entity.Plan;
import com.giflo.model.entity.PagoPlan;
import com.giflo.model.entity.Usuario;
import com.giflo.model.entity.Variedad;

@Configuration
public class RepositoryConfig implements RepositoryRestConfigurer {

	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		config.exposeIdsFor(Empresa.class, Usuario.class, Perfil.class, Catalogo.class, Parametro.class, Variedad.class,
				Flor.class, Empresa.class, EmpresaFoto.class,Plan.class,Cupon.class,PagoPlan.class,Contacto.class,Disponibilidad.class);
		config.getCorsRegistry().addMapping("/**").allowedOrigins("*").allowedHeaders("*")
				.allowedMethods("POST", "GET", "PUT", "DELETE","OPTIONS").allowCredentials(true).maxAge(3600);
	}

}
