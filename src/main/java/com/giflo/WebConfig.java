package com.giflo;

import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.giflo.view.service.FirebaseMessageServiceImpl;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

@Configuration
public class WebConfig {

	@Autowired
	private Environment env;
	
	private Logger log = LoggerFactory.getLogger(WebConfig.class);
	
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("*").allowedHeaders("*")
						.allowedMethods("POST", "GET", "PUT", "DELETE","OPTIONS").maxAge(3600);
			}

		};
	}
	@Bean 
	public FirebaseApp initFirebase(){
		try {
			String nameClient = env.getProperty("firebase.sdk.secret", String.class);
			InputStream resourceAsStream = FirebaseMessageServiceImpl.class.getClassLoader()
					.getResourceAsStream(nameClient);
			GoogleCredentials credential = GoogleCredentials.fromStream(resourceAsStream);
			FirebaseOptions options = FirebaseOptions.builder().setCredentials(credential).build();
			return FirebaseApp.initializeApp(options);
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
	}
}