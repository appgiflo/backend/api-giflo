package com.giflo.security.event;

import java.sql.Timestamp;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import com.giflo.model.entity.Usuario;
import com.giflo.model.enums.ValorSiNoEnum;
import com.giflo.security.services.IUserService;
import com.giflo.security.util.AES;

@Component
public class AuthenticationSuccessErrorHandler implements AuthenticationEventPublisher {

	private Logger log = LoggerFactory.getLogger(AuthenticationSuccessErrorHandler.class);

	@Autowired
	private Environment env;

	@Autowired
	private IUserService iUsuarioService;

	@Override
	public void publishAuthenticationSuccess(Authentication authentication) {
		String username = "";
		try {
			if (authentication.getDetails() instanceof OAuth2AuthenticationDetails
					|| authentication.getDetails() instanceof WebAuthenticationDetails) {
				return;
			}
			username = (new AES()).decrypt(authentication.getName());
			log.info(String.format("Login success : %1s", username));
			Usuario usuarioDb = iUsuarioService.findByUsername(username);
			log.info(String.format("El usuario %1s existe en la base", username));
			if (usuarioDb.getIntentos() != null && usuarioDb.getIntentos() > 1) {
				usuarioDb.setIntentos(0);
			}
			usuarioDb.setFechaLogin(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
			iUsuarioService.update(usuarioDb, usuarioDb.getId());

		} catch (Exception e) {
			log.error(String.format("El usuario %1s no existe en el sistema", username));
		}

	}

	@Override
	public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
		String username = "";
		try {
			String anonymousUser = env.getProperty("config.security.oauth.anonymous");
			String nameClient = env.getProperty("config.security.oauth.client.id");
			log.error(String.format("Mensaje Erro Login: %1s", exception.getMessage()));
			username = (new AES()).decrypt(authentication.getName());
			log.info(String.format("Erro para el usuario: %1s", username));

			if (username.equalsIgnoreCase(nameClient) || username.equalsIgnoreCase(anonymousUser)) {
				return; // si es igual a frontendapp se salen del método! o un anonimo
			}
			Usuario usuario = iUsuarioService.findByUsername(username);
			if (usuario != null) {
				usuario.setIntentos(usuario.getIntentos() != null ? usuario.getIntentos() + 1 : 0);
				if (usuario.getIntentos() >= 3) {
					usuario.setActivo(ValorSiNoEnum.NO);
				}
				iUsuarioService.update(usuario, usuario.getId());
			}
		} catch (AuthenticationException e) {
			log.error(String.format("Error el usuario %1s no existe en el sistema", username));
		}

	}

}
