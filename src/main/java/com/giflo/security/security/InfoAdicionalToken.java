package com.giflo.security.security;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import com.giflo.model.dto.UsuarioDTO;
import com.giflo.security.services.IUserService;
import com.giflo.security.util.AES;

@Component
public class InfoAdicionalToken implements TokenEnhancer {

	private Logger log = LoggerFactory.getLogger(InfoAdicionalToken.class);

	@Autowired
	private IUserService iUsuarioService;
	
	

	@Autowired
	private Environment env;

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		Map<String, Object> info = new HashMap<>();
		String usernameEncrypted = authentication.getName();
		String username = (new AES()).decrypt(usernameEncrypted);
		log.info(String.format("Username OAuth2 : %1s",username ));
		String anonymousUser = env.getProperty("config.security.oauth.anonymous");
		if (!anonymousUser.equalsIgnoreCase(username)) {
			UsuarioDTO user = iUsuarioService.findDTOByUsername(username);
			if (user != null) {
				info.put("nombres", user.getNombres());
				info.put("apellidos", user.getApellidos());
				info.put("correo", user.getCorreo());
				info.put("isAnonymous", user.getIsAnonymous());
				info.put("perfiles", user.getPerfiles());
				info.put("idUser", user.getId());
				info.put("recuperar", user.getRecuperacion());
				info.put("hasSuscription", user.getPlanVigente());
				info.put("isAdmin", user.getEsAdmin());
				info.put("contactosDisponibles", user.getContactosDisponibles());
				info.put("contactosRecarga", user.getContactosRecarga());
				info.put("tieneDisponibilidad", user.getTieneDisponibilidad());
			}
		} else {
			info.put("isAnonymous", "S");
			info.put("hasSuscription", false);
			info.put("contactosDisponibles", 0);
		}
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
		return accessToken;
	}

}
