package com.giflo.security.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.giflo.model.dto.UsuarioDTO;
import com.giflo.model.entity.Usuario;
import com.giflo.model.repository.UsuarioRepository;
import com.giflo.security.util.AES;
import com.giflo.view.service.IUsuarioService;


@Service
public class UserDetailService implements UserDetailsService, IUserService {

	private Logger log = LoggerFactory.getLogger(UserDetailService.class);

	@Autowired
	private Environment env;

	@Autowired
	private IUsuarioService iUsuarioService;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public UserDetails loadUserByUsername(String usernameEncrypted) throws UsernameNotFoundException {
		String username = "";
		try {
			String anonymousUser = env.getProperty("config.security.oauth.anonymous");
			String anonymousPass = env.getProperty("config.security.oauth.anonymous.pass");
			String anonymousRole = env.getProperty("config.security.oauth.anonymous.role");
			username = (new AES()).decrypt(usernameEncrypted);
			log.info(String.format("Username Login -> %1s", username));
			if (anonymousUser.equalsIgnoreCase(username)) {
				List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
				authorities.add(new SimpleGrantedAuthority(anonymousRole));
				return new User(usernameEncrypted, anonymousPass, true, true, true, true, authorities);
			} else {
				try {
					UsuarioDTO usuario = iUsuarioService.findByUsername(username);
					if (usuario != null) {
						if ("SI".equalsIgnoreCase(usuario.getActivo())) {
							List<GrantedAuthority> authorities = usuario.getPerfiles().stream()
									.map(per -> new SimpleGrantedAuthority(per.getNombre()))
									.peek(rol -> log.info(String.format("Rol: %1s", rol.getAuthority())))
									.collect(Collectors.toList());
							return new User(usernameEncrypted, usuario.getContrasenia(),
									"SI".equalsIgnoreCase(usuario.getActivo()), true, true, true, authorities);
						} else {
							throw new UsernameNotFoundException("El usuario se encuentra INACTIVO");
						}
					} else {
						throw new UsernameNotFoundException("No existe el usuario");
					}
				} catch (Exception e) {
					e.printStackTrace();
					throw new UsernameNotFoundException("No existe el usuario");
				}
			}
		} catch (AuthenticationException e) {
			e.printStackTrace();
			log.error(String.format("No existe el usuario con la identificación %1s$s", username));
			throw new UsernameNotFoundException(
					String.format("No existe el usuario con la identificación %1s$s", username));
		}

	}

	@Override
	public UsuarioDTO findDTOByUsername(String username) {

		return iUsuarioService.findByUsername(username);
	}

	@Override
	public Usuario update(Usuario usuario, Integer id) {
		usuario.setId(id);
		return usuarioRepository.save(usuario);
	}

	@Override
	public Usuario findByUsername(String username) {

		return usuarioRepository.findByUsername(username);
	}

}
