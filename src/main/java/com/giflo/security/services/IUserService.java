package com.giflo.security.services;

import org.springframework.web.bind.annotation.PathVariable;

import com.giflo.model.dto.UsuarioDTO;
import com.giflo.model.entity.Usuario;

public interface IUserService {

	public UsuarioDTO findDTOByUsername(String username);

	public Usuario update(Usuario usuario, @PathVariable Integer id);

	public Usuario findByUsername(	String username);
}
