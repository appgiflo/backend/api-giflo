package com.giflo.security.util;

import java.security.Security;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AES {

	private final String CHARSET_NAME = "UTF-8";
	private final String AES_NAME = "AES";
	// encryption mode
	private final String ALGORITHM = "AES/CBC/PKCS7Padding";
	// Key
	private final String KEY = "07CElMzjold3WC4oqCZwZcy1maq/yxTOj3t0K2cb8k4=";
	// Offset
	private final String IV = "8+zWLJamuU6AD9Bm6woWTg==";
	private Logger log = LoggerFactory.getLogger(AES.class);

	public AES() {
		Security.addProvider(new BouncyCastleProvider());
	}

	/**
	 * Encryption
	 *
	 * @param content
	 * @param key
	 * @return
	 */
	public String encrypt(String content) {
		byte[] result = null;
		try {
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			byte[] keyByte = Base64.decodeBase64(KEY);
			SecretKeySpec keySpec = new SecretKeySpec(keyByte, AES_NAME);
			byte[] byteIv = Base64.decodeBase64(IV);
			AlgorithmParameterSpec paramSpec = new IvParameterSpec(byteIv);
			cipher.init(Cipher.ENCRYPT_MODE, keySpec, paramSpec);
			result = cipher.doFinal(content.getBytes(CHARSET_NAME));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Base64.encodeBase64String(result);
	}

	/**
	 * Decryption
	 *
	 * @param content
	 * @param key
	 * @return
	 */
	public String decrypt(String content) {
		try {
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			byte[] keyByte = Base64.decodeBase64(KEY);
			SecretKeySpec keySpec = new SecretKeySpec(keyByte, AES_NAME);
			byte[] byteIv = Base64.decodeBase64(IV);
			AlgorithmParameterSpec paramSpec = new IvParameterSpec(byteIv);
			cipher.init(Cipher.DECRYPT_MODE, keySpec, paramSpec);
			return new String(cipher.doFinal(Base64.decodeBase64(content)), CHARSET_NAME);
		} catch (Exception e) {
			log.warn(String.format("Error Decrypt: %1s Dato %2s", e.getMessage(), content));
			return content;
		}
	}
}
