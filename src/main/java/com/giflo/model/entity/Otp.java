package com.giflo.model.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.giflo.model.enums.ValorEstadoOtpEnum;

/**
 * The persistent class for the a_catalogo database table.
 * 
 */
@Entity
@Table(name = "f_otp")
public class Otp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2824188751803068257L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ot_id")
	private Integer id;

	@Column(name = "ot_contacto")
	private String contacto;

	@Column(name = "ot_estado")
	@Enumerated(EnumType.STRING)
	private ValorEstadoOtpEnum activo;

	@Column(name = "ot_codigo")
	private String codigo;

	@Column(name = "ot_validez")
	private Timestamp validez;

	@Temporal(TemporalType.DATE)
	@Column(name = "ot_fecha_registro")
	private Date fechaRegistro;

	
	public Otp() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public ValorEstadoOtpEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorEstadoOtpEnum activo) {
		this.activo = activo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Timestamp getValidez() {
		return validez;
	}

	public void setValidez(Timestamp validez) {
		this.validez = validez;
	}

	
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

}