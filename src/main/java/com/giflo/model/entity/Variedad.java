package com.giflo.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the a_variedad database table.
 * 
 */
@Entity
@Table(name = "a_variedad")
public class Variedad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "av_id")
	private Integer id;

	@Column(name = "av_activo")
	private String activo;

	@Column(name = "av_indicaciones")
	private String indicaciones;

	@Column(name = "av_nombre")
	private String nombre;

	@Column(name = "av_nombre_foto")
	private String nombreFoto;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_color")
	private Catalogo color;

	@Column(name = "av_foto_pequenia")
	private String fotoPequenia;

	@Transient
	private Integer idColor;

	@Transient
	private String nombreColor;

	public Variedad() {
	}

	public Variedad(Integer id, String indicaciones, String nombre, String nombreFoto, String fotoPequenia) {
		super();
		this.id = id;
		this.indicaciones = indicaciones;
		this.nombre = nombre;
		this.nombreFoto = nombreFoto;
		this.fotoPequenia = fotoPequenia;
	}

	public Variedad(Catalogo color,Integer id, String indicaciones, String nombre, String nombreFoto, String fotoPequenia,
			String activo) {
		super();
		this.id = id;
		this.indicaciones = indicaciones;
		this.nombre = nombre;
		this.nombreFoto = nombreFoto;
		this.fotoPequenia = fotoPequenia;
		this.activo = activo;
		this.idColor = color.getId();
		this.nombreColor = color.getNombre();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getActivo() {
		return this.activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getIndicaciones() {
		return this.indicaciones;
	}

	public void setIndicaciones(String indicaciones) {
		this.indicaciones = indicaciones;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreFoto() {
		return this.nombreFoto;
	}

	public void setNombreFoto(String nombreFoto) {
		this.nombreFoto = nombreFoto;
	}

	public Catalogo getColor() {
		return color;
	}

	public void setColor(Catalogo color) {
		this.color = color;
	}

	public String getFotoPequenia() {
		return fotoPequenia;
	}

	public void setFotoPequenia(String fotoPequenia) {
		this.fotoPequenia = fotoPequenia;
	}

	public Integer getIdColor() {
		return idColor;
	}

	public void setIdColor(Integer idColor) {
		this.idColor = idColor;
	}

	public String getNombreColor() {
		return nombreColor;
	}

	public void setNombreColor(String nombreColor) {
		this.nombreColor = nombreColor;
	}
	

}