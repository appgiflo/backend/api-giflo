package com.giflo.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.giflo.model.enums.ValorSiNoEnum;

/**
 * The persistent class for the a_catalogo database table.
 * 
 */
@Entity
@Table(name = "a_catalogo")
public class Catalogo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "aca_id")
	private Integer id;

	@Column(name = "aca_foto_nombre")
	private String fotoNombre;

	@Column(name = "aca_activo")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum activo;

	@Column(name = "aca_grupo")
	private String grupo;

	@Column(name = "aca_nombre")
	private String nombre;

	@Column(name = "aca_orden")
	private Integer orden;

	@Column(name = "aca_codigo")
	private String codigo;

	public Catalogo() {
	}
	

	public Catalogo(Integer id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}


	public Catalogo(Catalogo c) {
		super();
		this.id = c.id;
		this.fotoNombre = c.fotoNombre;
		this.nombre = c.nombre;
		this.codigo = c.codigo;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFotoNombre() {
		return this.fotoNombre;
	}

	public void setFotoNombre(String fotoNombre) {
		this.fotoNombre = fotoNombre;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

	public String getGrupo() {
		return this.grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getOrden() {
		return this.orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

}