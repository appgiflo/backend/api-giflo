package com.giflo.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The persistent class for the f_detalle_publicacion database table.
 * 
 */
@Entity
@Table(name = "f_detalle_publicacion")
public class DetallePublicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "fdp_id")
	private Integer id;

	@Column(name = "fdp_cantidad")
	private BigDecimal cantidad;

	@Column(name = "fdp_estado")
	private String estado;

	@Column(name = "fdp_precio")
	private BigDecimal precio;

	// uni-directional many-to-one association to Catalogo
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_largo_tallo")
	private Catalogo largoTallo;

	// uni-directional many-to-one association to Catalogo
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_tamanio_boton")
	private Catalogo tamanioBoton;

	// bi-directional many-to-one association to Publicacion
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_publicacion")
	private Publicacion publicacion;

	@Transient
	private String largoTalloS;
	@Transient
	private String tamanioBotonS;
	@Transient
	private Integer idLargoTallo;
	@Transient
	private Integer idTamanioBoton;

	public DetallePublicacion() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Catalogo getLargoTallo() {
		return this.largoTallo;
	}

	public void setLargoTallo(Catalogo largoTallo) {
		this.largoTallo = largoTallo;
	}

	public Catalogo getTamanioBoton() {
		return this.tamanioBoton;
	}

	public void setTamanioBoton(Catalogo tamanioBoton) {
		this.tamanioBoton = tamanioBoton;
	}

	public Publicacion getPublicacion() {
		return this.publicacion;
	}

	public void setPublicacion(Publicacion publicacion) {
		this.publicacion = publicacion;
	}

	public Integer getIdLargoTallo() {
		return idLargoTallo;
	}

	public void setIdLargoTallo(Integer idLargoTallo) {
		this.idLargoTallo = idLargoTallo;
	}

	public Integer getIdTamanioBoton() {
		return idTamanioBoton;
	}

	public void setIdTamanioBoton(Integer idTamanioBoton) {
		this.idTamanioBoton = idTamanioBoton;
	}

	public String getLargoTalloS() {
		return largoTalloS;
	}

	public void setLargoTalloS(String largoTalloS) {
		this.largoTalloS = largoTalloS;
	}

	public String getTamanioBotonS() {
		return tamanioBotonS;
	}

	public void setTamanioBotonS(String tamanioBotonS) {
		this.tamanioBotonS = tamanioBotonS;
	}

	

}