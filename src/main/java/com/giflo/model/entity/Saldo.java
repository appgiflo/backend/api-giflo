package com.giflo.model.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.giflo.model.enums.GrupoSaldoEnum;

/**
 * The persistent class for the s_usuario database table.
 * 
 */
@Entity
@Table(name = "f_saldo")
public class Saldo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "fs_id")
	private Integer id;

	// uni-directional many-to-one association to Usuario
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fs_id_usuario")
	private Usuario usuario;

	@Column(name = "fs_grupo")
	@Enumerated(EnumType.STRING)
	private GrupoSaldoEnum grupo;

	@Column(name = "fs_fecha_registro")
	private Timestamp fechaRegistro;

	@Column(name = "fs_fecha_recarga")
	private Timestamp fechaRecarga;

	@Column(name = "fs_recarga")
	private Integer recarga;

	@Column(name = "fs_disponible")
	private Integer disponible;

	@Transient
	private Integer idUsuario;

	public Saldo() {
	}

	public Saldo(Saldo s) {
		id = s.getId();
		idUsuario = s.getUsuario().getId();
		grupo = s.getGrupo();
		recarga = s.getRecarga();
		disponible = s.getDisponible();
	}

	public Saldo(Integer recarga, Integer disponible) {
		super();
		this.recarga = recarga;
		this.disponible = disponible;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public GrupoSaldoEnum getGrupo() {
		return grupo;
	}

	public void setGrupo(GrupoSaldoEnum grupo) {
		this.grupo = grupo;
	}

	public Timestamp getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Timestamp fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Timestamp getFechaRecarga() {
		return fechaRecarga;
	}

	public void setFechaRecarga(Timestamp fechaRecarga) {
		this.fechaRecarga = fechaRecarga;
	}

	public Integer getRecarga() {
		return recarga;
	}

	public void setRecarga(Integer recarga) {
		this.recarga = recarga;
	}

	public Integer getDisponible() {
		return disponible;
	}

	public void setDisponible(Integer disponible) {
		this.disponible = disponible;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

}