package com.giflo.model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.giflo.model.enums.ValorSiNoEnum;

/**
 * The persistent class for the a_flor database table.
 * 
 */
@Entity
@Table(name = "a_flor")
public class Flor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "af_id")
	private Integer id;

	@Column(name = "af_disponible")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum disponible;

	// bi-directional many-to-one association to Empresa
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_empresa")
	private Empresa empresa;

	// uni-directional many-to-one association to Variedad
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_variedad")
	private Variedad variedad;

	// bi-directional many-to-one association to Publicacion
	@OneToMany(mappedBy = "flor")
	private List<Publicacion> publicaciones;

	@Transient
	private String nombre;

	@Transient
	private String foto;

	@Transient
	private String fotoPequenia;

	@Transient
	private String indicaciones;

	public Flor() {
	}

	public Flor(Integer id, ValorSiNoEnum disponible, String nombre, String foto, String indicaciones,
			String fotoPequenia) {
		super();
		this.id = id;
		this.disponible = disponible;
		this.nombre = nombre;
		this.foto = foto;
		this.indicaciones = indicaciones;
		this.fotoPequenia = fotoPequenia;
	}

	public Flor(Empresa e, Flor f) {
		super();
		this.id = f.getId();
		this.disponible = f.getDisponible();
		this.nombre = f.getNombre();
		this.variedad = f.getVariedad();
		this.empresa = new Empresa(e, e.getUsuarios());
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ValorSiNoEnum getDisponible() {
		return this.disponible;
	}

	public void setDisponible(ValorSiNoEnum disponible) {
		this.disponible = disponible;
	}

	public Empresa getEmpresa() {
		return this.empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Variedad getVariedad() {
		return this.variedad;
	}

	public void setVariedad(Variedad variedad) {
		this.variedad = variedad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public List<Publicacion> getPublicaciones() {
		return publicaciones;
	}

	public void setPublicaciones(List<Publicacion> publicaciones) {
		this.publicaciones = publicaciones;
	}

	public String getIndicaciones() {
		return indicaciones;
	}

	public void setIndicaciones(String indicaciones) {
		this.indicaciones = indicaciones;
	}

	public String getFotoPequenia() {
		return fotoPequenia;
	}

	public void setFotoPequenia(String fotoPequenia) {
		this.fotoPequenia = fotoPequenia;
	}

}