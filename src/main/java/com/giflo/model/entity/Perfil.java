package com.giflo.model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.giflo.model.enums.ValorSiNoEnum;

/**
 * The persistent class for the s_perfil database table.
 * 
 */
@Entity
@Table(name = "s_perfil")
public class Perfil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sp_id")
	private Integer id;

	@Column(name = "sp_activo")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum activo;

	@Column(name = "sp_nombre")
	private String nombre;

	// bi-directional many-to-one association to UsuarioPerfil
	@OneToMany(mappedBy = "perfil")
	private List<UsuarioPerfil> usuarioPerfiles;

	public Perfil() {
	}

	public Perfil(Integer id, ValorSiNoEnum activo, String nombre) {
		super();
		this.id = id;
		this.activo = activo;
		this.nombre = nombre;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<UsuarioPerfil> getUsuarioPerfiles() {
		return this.usuarioPerfiles;
	}

	public void setUsuarioPerfiles(List<UsuarioPerfil> usuarioPerfiles) {
		this.usuarioPerfiles = usuarioPerfiles;
	}

	public UsuarioPerfil addUsuarioPerfile(UsuarioPerfil usuarioPerfile) {
		getUsuarioPerfiles().add(usuarioPerfile);
		usuarioPerfile.setPerfil(this);

		return usuarioPerfile;
	}

	public UsuarioPerfil removeUsuarioPerfile(UsuarioPerfil usuarioPerfile) {
		getUsuarioPerfiles().remove(usuarioPerfile);
		usuarioPerfile.setPerfil(null);

		return usuarioPerfile;
	}

}