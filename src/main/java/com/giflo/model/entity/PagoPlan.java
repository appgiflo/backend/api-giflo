package com.giflo.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.giflo.model.enums.ValorEstadoPagoPlanEnum;

@Entity
@Table(name = "a_pago_plan")
public class PagoPlan implements Serializable {

	private static final long serialVersionUID = -2824188751803068257L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "su_id")
	private Integer id;

	// uni-directional many-to-one association to Plan
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_plan")
	private Plan plan;

	// uni-directional many-to-one association to Usuario
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;

	@Column(name = "su_fecha_registro")
	private Timestamp fechaRegistro;

	@Column(name = "su_fecha_pago")
	private Timestamp fechaPago;

	@Column(name = "su_codigo_pago")
	private String codigoPago;

	@Column(name = "su_documento")
	private String documento;

	@Column(name = "su_cupon")
	private String cupon;

	@Column(name = "su_descuento")
	private BigDecimal descuento;

	@Column(name = "su_subtotal")
	private BigDecimal subtotal;

	@Column(name = "su_iva")
	private BigDecimal iva;

	@Column(name = "su_total")
	private BigDecimal total;

	@Column(name = "su_estado")
	@Enumerated(EnumType.STRING)
	private ValorEstadoPagoPlanEnum estado;

	@Transient
	private String apellidos;

	@Transient
	private String nombres;

	@Transient
	private String planNombre;

	@Column(name = "su_referencia_ptp")
	private String referenciaPlaceToPay;

	public PagoPlan() {
	}

	public PagoPlan(PagoPlan sus) {
		super();
		this.id = sus.id;
		this.plan = sus.plan;
		this.usuario = sus.usuario;
		this.fechaRegistro = sus.fechaRegistro;
		this.fechaPago = sus.fechaPago;
		this.codigoPago = sus.codigoPago;
		this.documento = sus.documento;
		this.cupon = sus.cupon;
		this.descuento = sus.descuento;
		this.subtotal = sus.subtotal;
		this.iva = sus.iva;
		this.total = sus.total;
		this.estado = sus.estado;
		this.apellidos = sus.usuario.getApellidos();
		this.nombres = sus.usuario.getNombres();
		this.planNombre = sus.plan.getNombre();
		this.referenciaPlaceToPay = sus.referenciaPlaceToPay;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getCupon() {
		return cupon;
	}

	public void setCupon(String cupon) {
		this.cupon = cupon;
	}

	public BigDecimal getDescuento() {
		return descuento;
	}

	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public ValorEstadoPagoPlanEnum getEstado() {
		return estado;
	}

	public void setEstado(ValorEstadoPagoPlanEnum estado) {
		this.estado = estado;
	}

	public Timestamp getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Timestamp fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Timestamp getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Timestamp fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String getCodigoPago() {
		return codigoPago;
	}

	public void setCodigoPago(String codigoPago) {
		this.codigoPago = codigoPago;
	}

	public String getApellidos() {
		return this.usuario.getApellidos();
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNombres() {
		return this.usuario.getNombres();
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getPlanNombre() {
		return this.plan.getNombre();
	}

	public void setPlanNombre(String planNombre) {
		this.planNombre = planNombre;
	}

	public String getReferenciaPlaceToPay() {
		return referenciaPlaceToPay;
	}

	public void setReferenciaPlaceToPay(String referenciaPlaceToPay) {
		this.referenciaPlaceToPay = referenciaPlaceToPay;
	}

}