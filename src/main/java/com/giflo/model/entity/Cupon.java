package com.giflo.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.giflo.model.enums.ValorEstadoCuponEnum;

/**
 * The persistent class for the a_cupon database table.
 * 
 */
@Entity
@Table(name = "a_cupon")
public class Cupon implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2824188751803068257L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cu_id")
	private Integer id;

	@Column(name = "id_plan")
	private Integer idPlan;

	@Column(name = "cu_codigo")
	private String codigo;

	@Column(name = "cu_porciento")
	private BigDecimal porciento;

	@Column(name = "cu_fecha_inicio")
	private Timestamp fechaInicio;

	@Column(name = "cu_fecha_fin")
	private Timestamp fechaFin;

	@Column(name = "cu_mensaje")
	private String mensaje;

	@Column(name = "cu_estado")
	@Enumerated(EnumType.STRING)
	private ValorEstadoCuponEnum estado;

	public Cupon() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public BigDecimal getPorciento() {
		return porciento;
	}

	public void setPorciento(BigDecimal porciento) {
		this.porciento = porciento;
	}

	

	public Timestamp getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Timestamp fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Timestamp getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Timestamp fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ValorEstadoCuponEnum getEstado() {
		return estado;
	}

	public void setEstado(ValorEstadoCuponEnum estado) {
		this.estado = estado;
	}

	public Integer getIdPlan() {
		return idPlan;
	}

	public void setIdPlan(Integer idPlan) {
		this.idPlan = idPlan;
	}

}