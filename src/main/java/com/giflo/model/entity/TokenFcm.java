package com.giflo.model.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the s_token_fcm database table.
 * 
 */
@Entity
@Table(name = "s_token_fcm")
public class TokenFcm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ut_id")
	private Integer id;

	@Column(name = "ut_device")
	private String device;

	@Column(name = "ut_registro")
	private Timestamp registro;

	@Column(name = "ut_token")
	private String token;

	// bi-directional many-to-one association to SUsuario
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ut_id_usuario")
	private Usuario usuario;

	public TokenFcm() {
	}

	public TokenFcm(Integer id, String token) {
		super();
		this.id = id;
		this.token = token;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public Timestamp getRegistro() {
		return registro;
	}

	public void setRegistro(Timestamp registro) {
		this.registro = registro;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}