package com.giflo.model.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.giflo.model.enums.EstadoPedidoEnum;

import java.math.BigDecimal;

/**
 * The persistent class for the f_detalle_publicacion database table.
 * 
 */
@Entity
@Table(name = "a_detalle_pedido")
public class DetallePedido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "adp_id")
	private Integer id;

	@Column(name = "adp_estado")
	@Enumerated(EnumType.STRING)
	private EstadoPedidoEnum estado;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_pedido")
	private Pedido pedido;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_unidad_medida")
	private Catalogo unidadMedida;

	@Column(name = "adp_cantidad")
	private BigDecimal cantidad;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_largo_tallo")
	private Catalogo largoTallo;

	@Column(name = "adp_precio")
	private BigDecimal precio;

	@Transient
	private String valorlargoTallo;

	@Transient
	private Integer idLargoTallo;

	@Transient
	private String idUnidad;

	@Transient
	private String valorUnidad;

	public DetallePedido() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EstadoPedidoEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoPedidoEnum estado) {
		this.estado = estado;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public Catalogo getUnidadMedida() {
		return unidadMedida;
	}

	public void setUnidadMedida(Catalogo unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public BigDecimal getCantidad() {
		return cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public Catalogo getLargoTallo() {
		return largoTallo;
	}

	public void setLargoTallo(Catalogo largoTallo) {
		this.largoTallo = largoTallo;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public String getValorlargoTallo() {
		return valorlargoTallo;
	}

	public void setValorlargoTallo(String valorlargoTallo) {
		this.valorlargoTallo = valorlargoTallo;
	}

	public Integer getIdLargoTallo() {
		return idLargoTallo;
	}

	public void setIdLargoTallo(Integer idLargoTallo) {
		this.idLargoTallo = idLargoTallo;
	}

	public String getIdUnidad() {
		return idUnidad;
	}

	public void setIdUnidad(String idUnidad) {
		this.idUnidad = idUnidad;
	}

	public String getValorUnidad() {
		return valorUnidad;
	}

	public void setValorUnidad(String valorUnidad) {
		this.valorUnidad = valorUnidad;
	}

}