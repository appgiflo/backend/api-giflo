package com.giflo.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the s_parametro database table.
 * 
 */
@Entity
@Table(name = "s_parametro")
public class Parametro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "spa_id")
	private String id;

	@Column(name = "spa_activo")
	private String activo;

	@Column(name = "spa_descripcion")
	private String descripcion;

	@Column(name = "spa_valor")
	private String valor;

	public Parametro() {
	}

	public Parametro(String id, String valor) {
		super();
		this.id = id;
		this.valor = valor;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getActivo() {
		return this.activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}