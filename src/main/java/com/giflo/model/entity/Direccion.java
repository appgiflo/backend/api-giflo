package com.giflo.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.giflo.model.enums.ValorSiNoEnum;

/**
 * The persistent class for the a_direccion database table.
 * 
 */
@Entity
@Table(name = "a_direccion")
public class Direccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ad_id")
	private Integer id;

	@Column(name = "ad_activo")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum activo;

	@Column(name = "ad_calle_primaria")
	private String callePrimaria;

	@Column(name = "ad_calle_secundaria")
	private String calleSecundaria;

	@Column(name = "ad_latitud")
	private String latitud;

	@Column(name = "ad_longitud")
	private String longitud;

	@Column(name = "ad_numero")
	private String numero;

	@Column(name = "ad_telefono")
	private String telefono;

	@Column(name = "ad_celular")
	private String celular;

	@Column(name = "ad_referencia")
	private String referencia;

	private String ciudad;

	// uni-directional many-to-one association to Catalogo
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_tipo")
	private Catalogo tipo;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_pais")
	private Catalogo pais;

	// bi-directional many-to-one association to Empresa
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_empresa")
	private Empresa empresa;

	public Direccion() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

	public String getCallePrimaria() {
		return this.callePrimaria;
	}

	public void setCallePrimaria(String callePrimaria) {
		this.callePrimaria = callePrimaria;
	}

	public String getCalleSecundaria() {
		return this.calleSecundaria;
	}

	public void setCalleSecundaria(String calleSecundaria) {
		this.calleSecundaria = calleSecundaria;
	}

	public String getLatitud() {
		return this.latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return this.longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getReferencia() {
		return this.referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public Catalogo getTipo() {
		return this.tipo;
	}

	public void setTipo(Catalogo tipo) {
		this.tipo = tipo;
	}

	public Empresa getEmpresa() {
		return this.empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public Catalogo getPais() {
		return pais;
	}

	public void setPais(Catalogo pais) {
		this.pais = pais;
	}

}