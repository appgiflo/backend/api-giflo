package com.giflo.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.giflo.model.enums.ValorSiNoEnum;

/**
 * The persistent class for the a_empresa database table.
 * 
 */
@Entity
@Table(name = "a_empresa")
public class Empresa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ae_id")
	private Integer id;

	@Column(name = "ae_activo")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum activo;

	@Column(name = "ae_calificacion")
	private BigDecimal calificacion;

	@Column(name = "ae_cont_calificacion")
	private Integer contCalificacion;

	@Column(name = "ae_nombre")
	private String nombre;

	@Column(name = "ae_ruc")
	private String ruc;

	// bi-directional many-to-one association to Direccion
	@OneToMany(mappedBy = "empresa")
	private List<Direccion> direcciones;

	// bi-directional many-to-one association to EmpresaFoto
	@OneToMany(mappedBy = "empresa")
	private List<EmpresaFoto> fotos;

	// bi-directional many-to-one association to Flor
	@OneToMany(mappedBy = "empresa")
	private List<Flor> flores;

	@OneToMany(mappedBy = "empresa")
	private List<Usuario> usuarios;

	@OneToMany(mappedBy = "empresa")
	private List<Producto> servicios;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_tipo")
	private Catalogo tipo;

	@Column(name = "ae_sri_valido")
	private String sriValido;

	@Transient
	private String urlImagen;
	@Transient
	private String imagenId;
	@Transient
	private String callePrimaria;
	@Transient
	private String calleSecundaria;
	@Transient
	private String latitud;
	@Transient
	private String longitud;
	@Transient
	private String numero;
	@Transient
	private String telefono;
	@Transient
	private String celular;
	@Transient
	private String referencia;
	@Transient
	private String tipoEmpresa;
	@Transient
	private String tipoEmpresaCodigo;
	@Transient
	private Integer idUsuario;

	@Transient
	private Integer idTipoEmpresa;

	@Transient
	private String pais;

	@Transient
	private String paisCodigo;

	@Transient
	private String ciudad;

	public Empresa() {
	}

	public Empresa(Integer id, String nombre, String ruc, List<EmpresaFoto> fotos, List<Direccion> direcciones,
			BigDecimal calificacion, Integer contCalificacion, String tipoEmpresa, String sriValido,
			String tipoEmpresaCodigo) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.ruc = ruc;
		this.calificacion = calificacion;
		this.contCalificacion = contCalificacion;
		this.tipoEmpresa = tipoEmpresa;
		this.tipoEmpresaCodigo = tipoEmpresaCodigo;
		this.sriValido = sriValido;
		EmpresaFoto f = fotos.parallelStream().filter(fot -> ValorSiNoEnum.SI.equals(fot.getPrincipal())).findFirst()
				.orElse(null);
		if (f != null) {
			this.urlImagen = f.getNombre();
			this.imagenId = f.getFotoId();
		}
		Direccion d = direcciones.parallelStream().filter(dir -> ValorSiNoEnum.SI.equals(dir.getActivo())).findFirst()
				.orElse(null);
		if (d != null) {
			this.callePrimaria = d.getCallePrimaria();
			this.calleSecundaria = d.getCalleSecundaria();
			this.latitud = d.getLatitud();
			this.longitud = d.getLongitud();
			this.numero = d.getNumero();
			this.telefono = d.getTelefono();
			this.celular = d.getCelular();
			this.referencia = d.getReferencia();
			if (d.getPais() != null) {
				this.pais = d.getPais().getNombre();
				this.paisCodigo = d.getPais().getFotoNombre();
			}
			this.ciudad = d.getCiudad();
		}
	}

	public Empresa(Empresa e) {
		super();
		this.id = e.getId();
		this.calificacion = e.getCalificacion();
		this.nombre = e.getNombre();
		this.ruc = e.getRuc();
		this.tipoEmpresa = e.getTipo().getNombre();
		this.idTipoEmpresa = e.getTipo().getId();
		this.tipoEmpresaCodigo = e.getTipo().getCodigo();
		this.sriValido = e.getSriValido();
		EmpresaFoto f = e.getFotos().parallelStream().filter(fot -> ValorSiNoEnum.SI.equals(fot.getPrincipal()))
				.findFirst().orElse(null);
		if (f != null) {
			this.urlImagen = f.getNombre();
			this.imagenId = f.getFotoId();
		}
		Direccion d = e.getDirecciones().parallelStream().filter(dir -> ValorSiNoEnum.SI.equals(dir.getActivo()))
				.findFirst().orElse(null);
		if (d != null) {
			this.callePrimaria = d.getCallePrimaria();
			this.calleSecundaria = d.getCalleSecundaria();
			this.latitud = d.getLatitud();
			this.longitud = d.getLongitud();
			this.numero = d.getNumero();
			this.telefono = d.getTelefono();
			this.celular = d.getCelular();
			this.referencia = d.getReferencia();
			if (d.getPais() != null) {
				this.pais = d.getPais().getNombre();
				this.paisCodigo = d.getPais().getFotoNombre();
			}
			this.ciudad = d.getCiudad();
		}
	}

	public Empresa(Empresa e, List<Usuario> usuarios) {
		super();
		this.id = e.getId();
		this.nombre = e.getNombre();
		this.tipo = e.getTipo();
		this.tipoEmpresa = e.getTipo().getNombre();
		this.idTipoEmpresa = e.getTipo().getId();
		this.tipoEmpresaCodigo = e.getTipo().getCodigo();
		this.fotos = e.getFotos();
		this.sriValido = e.getSriValido();
		if (e.usuarios != null && e.usuarios.size() > 0) {
			Usuario u = e.usuarios.get(0);
			this.idUsuario = u.getId();
		}

	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

	public BigDecimal getCalificacion() {
		return this.calificacion;
	}

	public void setCalificacion(BigDecimal calificacion) {
		this.calificacion = calificacion;
	}

	public Integer getContCalificacion() {
		return contCalificacion;
	}

	public void setContCalificacion(Integer contCalificacion) {
		this.contCalificacion = contCalificacion;
	}

	public String getNombre() {
		return this.nombre != null ? this.nombre : "";
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRuc() {
		return this.ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public List<Direccion> getDirecciones() {
		return direcciones;
	}

	public void setDirecciones(List<Direccion> direcciones) {
		this.direcciones = direcciones;
	}

	public Direccion addADireccion(Direccion ADireccion) {
		getDirecciones().add(ADireccion);
		ADireccion.setEmpresa(this);

		return ADireccion;
	}

	public Direccion removeADireccion(Direccion ADireccion) {
		getDirecciones().remove(ADireccion);
		ADireccion.setEmpresa(null);

		return ADireccion;
	}

	public List<EmpresaFoto> getFotos() {
		return this.fotos;
	}

	public void setFotos(List<EmpresaFoto> fotos) {
		this.fotos = fotos;
	}

	public EmpresaFoto addFoto(EmpresaFoto foto) {
		getFotos().add(foto);
		foto.setEmpresa(this);

		return foto;
	}

	public EmpresaFoto removeFoto(EmpresaFoto foto) {
		getFotos().remove(foto);
		foto.setEmpresa(null);

		return foto;
	}

	public List<Flor> getFlores() {
		return this.flores;
	}

	public void setFlores(List<Flor> flores) {
		this.flores = flores;
	}

	public Flor addFlore(Flor flore) {
		getFlores().add(flore);
		flore.setEmpresa(this);
		return flore;
	}

	public Flor removeFlore(Flor flore) {
		getFlores().remove(flore);
		flore.setEmpresa(null);

		return flore;
	}

	public Catalogo getTipo() {
		return tipo;
	}

	public void setTipo(Catalogo tipo) {
		this.tipo = tipo;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public String getUrlImagen() {
		return urlImagen;
	}

	public void setUrlImagen(String urlImagen) {
		this.urlImagen = urlImagen;
	}

	public String getCallePrimaria() {
		return callePrimaria;
	}

	public void setCallePrimaria(String callePrimaria) {
		this.callePrimaria = callePrimaria;
	}

	public String getCalleSecundaria() {
		return calleSecundaria;
	}

	public void setCalleSecundaria(String calleSecundaria) {
		this.calleSecundaria = calleSecundaria;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getImagenId() {
		return imagenId;
	}

	public void setImagenId(String imagenId) {
		this.imagenId = imagenId;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Integer getIdTipoEmpresa() {
		return idTipoEmpresa;
	}

	public void setIdTipoEmpresa(Integer idTipoEmpresa) {
		this.idTipoEmpresa = idTipoEmpresa;
	}

	public Integer getIdUserFirst() {
		if (usuarios != null && usuarios.size() > 0) {
			Usuario u = usuarios.get(0);
			return u.getId();
		}
		return -1;
	}

	public String getTipoEmpresa() {
		return tipoEmpresa;
	}

	public void setTipoEmpresa(String tipoEmpresa) {
		this.tipoEmpresa = tipoEmpresa;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getPaisCodigo() {
		return paisCodigo;
	}

	public void setPaisCodigo(String paisCodigo) {
		this.paisCodigo = paisCodigo;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getSriValido() {
		return sriValido;
	}

	public void setSriValido(String sriValido) {
		this.sriValido = sriValido;
	}

	public List<Producto> getServicios() {
		return servicios;
	}

	public void setServicios(List<Producto> servicios) {
		this.servicios = servicios;
	}

	public String getTipoEmpresaCodigo() {
		return tipoEmpresaCodigo;
	}

	public void setTipoEmpresaCodigo(String tipoEmpresaCodigo) {
		this.tipoEmpresaCodigo = tipoEmpresaCodigo;
	}

}