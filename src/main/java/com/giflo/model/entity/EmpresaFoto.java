package com.giflo.model.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.giflo.model.enums.ValorEstadoEnum;
import com.giflo.model.enums.ValorSiNoEnum;

/**
 * The persistent class for the a_empresa_foto database table.
 * 
 */
@Entity
@Table(name = "a_empresa_foto")
public class EmpresaFoto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "aef_id")
	private Integer id;

	@Column(name = "aef_estado")
	@Enumerated(EnumType.STRING)
	private ValorEstadoEnum estado;

	@Column(name = "aef_nombre")
	private String nombre;

	@Column(name = "aef_public_id")
	private String fotoId;

	@Column(name = "aef_principal")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum principal;

	// bi-directional many-to-one association to Empresa
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_empresa")
	private Empresa empresa;

	@Column(name = "aef_fecha_actualizacion")
	private Timestamp fechaActualizacion;

	public EmpresaFoto() {
	}

	public EmpresaFoto(EmpresaFoto foto) {
		super();
		this.id = foto.id;
		this.nombre = foto.nombre;
		this.empresa=foto.getEmpresa();
		this.estado=foto.getEstado();
		this.fotoId=foto.getFotoId();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ValorEstadoEnum getEstado() {
		return estado;
	}

	public void setEstado(ValorEstadoEnum estado) {
		this.estado = estado;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ValorSiNoEnum getPrincipal() {
		return principal;
	}

	public void setPrincipal(ValorSiNoEnum principal) {
		this.principal = principal;
	}

	public Empresa getEmpresa() {
		return this.empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getFotoId() {
		return fotoId;
	}

	public void setFotoId(String fotoId) {
		this.fotoId = fotoId;
	}

	public Timestamp getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Timestamp fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

}