package com.giflo.model.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.giflo.model.enums.ValorSiNoEnum;

/**
 * The persistent class for the s_usuario database table.
 * 
 */
@Entity
@Table(name = "s_usuario")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "su_id")
	private Integer id;

	// uni-directional many-to-one association to Usuario
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_empresa")
	private Empresa empresa;

	@Column(name = "su_activo")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum activo;

	@Column(name = "su_contrasenia")
	private String contrasenia;

	@Column(name = "su_correo")
	private String correo;

	@Column(name = "su_fecha_login")
	private Timestamp fechaLogin;

	@Column(name = "su_fecha_registro")
	private Timestamp fechaRegistro;

	@Column(name = "su_intentos")
	private Integer intentos;

	@Column(name = "su_username")
	private String username;

	@Column(name = "su_apellidos")
	private String apellidos;

	@Column(name = "su_nombres")
	private String nombres;

	@Column(name = "su_recuperacion")
	private String recuperacion;

	@Column(name = "su_path_foto")
	private String foto;

	@Column(name = "su_path_foto_id")
	private String fotoId;

	@Column(name = "su_clave_validez")
	private Timestamp claveValidez;

	@Column(name = "su_fecha_actualizacion")
	private Timestamp fechaActualizacion;

	// bi-directional many-to-one association to UsuarioPerfil
	@OneToMany(mappedBy = "usuario")
	private List<UsuarioPerfil> usuarioPerfiles;

	@OneToMany(mappedBy = "usuario")
	private List<TokenFcm> tokenFcms;

	@Column(name = "su_es_admin")
	private String esAdmin;

	public Usuario() {
	}
	public Usuario(Usuario u) {
		this.id = u.getId();
		this.empresa = new Empresa(u.getEmpresa());
		this.correo = u.getCorreo();
		this.apellidos = u.getApellidos();
		this.nombres = u.getNombres();
	}

	public Usuario(Integer id, String nombres, String apellidos) {
		super();
		this.id = id;
		this.nombres = nombres;
		this.apellidos = apellidos;
	}

	public Usuario(Integer id, Empresa empresa, String correo, String apellidos, String nombres) {
		super();
		this.id = id;
		this.empresa = empresa;
		this.correo = correo;
		this.apellidos = apellidos;
		this.nombres = nombres;
	}

	public Usuario(Integer id, String apellidos, String nombres, List<TokenFcm> tokenFcms) {
		super();
		this.id = id;
		this.apellidos = apellidos;
		this.nombres = nombres;
		this.tokenFcms = tokenFcms.stream().map(t -> new TokenFcm(t.getId(), t.getToken()))
				.collect(Collectors.toList());
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

	public String getContrasenia() {
		return this.contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Timestamp getFechaLogin() {
		return this.fechaLogin;
	}

	public void setFechaLogin(Timestamp fechaLogin) {
		this.fechaLogin = fechaLogin;
	}

	public Timestamp getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Timestamp fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Integer getIntentos() {
		return this.intentos;
	}

	public void setIntentos(Integer intentos) {
		this.intentos = intentos;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getApellidos() {
		return apellidos != null ? apellidos : "";
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNombres() {
		return nombres != null ? nombres : "";
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public List<UsuarioPerfil> getUsuarioPerfiles() {
		return this.usuarioPerfiles;
	}

	public void setUsuarioPerfiles(List<UsuarioPerfil> usuarioPerfiles) {
		this.usuarioPerfiles = usuarioPerfiles;
	}

	public UsuarioPerfil addUsuarioPerfile(UsuarioPerfil usuarioPerfile) {
		getUsuarioPerfiles().add(usuarioPerfile);
		usuarioPerfile.setUsuario(this);

		return usuarioPerfile;
	}

	public UsuarioPerfil removeUsuarioPerfile(UsuarioPerfil usuarioPerfile) {
		getUsuarioPerfiles().remove(usuarioPerfile);
		usuarioPerfile.setUsuario(null);

		return usuarioPerfile;
	}

	public String getRecuperacion() {
		return recuperacion != null ? recuperacion : "NO";
	}

	public void setRecuperacion(String recuperacion) {
		this.recuperacion = recuperacion;
	}

	public Timestamp getClaveValidez() {
		return claveValidez;
	}

	public void setClaveValidez(Timestamp claveValidez) {
		this.claveValidez = claveValidez;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getFotoId() {
		return fotoId;
	}

	public void setFotoId(String fotoId) {
		this.fotoId = fotoId;
	}

	public Timestamp getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Timestamp fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public List<TokenFcm> getTokenFcms() {
		return tokenFcms;
	}

	public void setTokenFcms(List<TokenFcm> tokenFcms) {
		this.tokenFcms = tokenFcms;
	}

	public String getEsAdmin() {
		return esAdmin;
	}

	public void setEsAdmin(String esAdmin) {
		this.esAdmin = esAdmin;
	}

	public String NombresCompletos() {
		return this.getNombres().concat(" ").concat(this.getApellidos());
	}

}