package com.giflo.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the f_publicacion database table.
 * 
 */
@Entity
@Table(name = "f_publicacion")
public class Publicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "fp_id")
	private Integer id;
	
	@Column(name = "fp_descripcion")
	private String descripcion;
	
	@Column(name = "fp_unidad")
	private String unidad;
	
	@Column(name = "fp_estado")
	private String estado;

	@Column(name = "fp_fecha_publicacion")
	private Timestamp fechaPublicacion;

	@Temporal(TemporalType.DATE)
	@Column(name = "fp_inicio_vigencia")
	private Date inicioVigencia;

	@Temporal(TemporalType.DATE)
	@Column(name = "fp_fin_vigencia")
	private Date finVigencia;

	// bi-directional many-to-one association to DetallePublicacion
	@OneToMany(mappedBy = "publicacion")
	private List<DetallePublicacion> detalles;

	// uni-directional many-to-one association to Catalogo

	// uni-directional many-to-one association to Catalogo
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_tipo_publicacion")
	private Catalogo tipoPublicacion;

	// bi-directional many-to-one association to Empresa
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_flor")
	private Flor flor;

	@Transient
	private Integer idFlor;

	@Transient
	private Integer nombreFlor;

	@Transient
	private Integer idTipoPublicacion;

	@Transient
	private Integer idEmpresa;

	@Transient
	private BigDecimal calificacion;

	@Column(name = "fp_tiene_tallo")
	private String tieneTallo;

	@Column(name = "fp_tiene_boton")
	private String tieneBoton;

	public Publicacion() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Timestamp getFechaPublicacion() {
		return fechaPublicacion;
	}

	public void setFechaPublicacion(Timestamp fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	public List<DetallePublicacion> getDetalles() {
		return this.detalles;
	}

	public void setDetalles(List<DetallePublicacion> detalles) {
		this.detalles = detalles;
	}

	public DetallePublicacion addDetalle(DetallePublicacion detalle) {
		getDetalles().add(detalle);
		detalle.setPublicacion(this);

		return detalle;
	}

	public DetallePublicacion removeDetalle(DetallePublicacion detalle) {
		getDetalles().remove(detalle);
		detalle.setPublicacion(null);

		return detalle;
	}

	public Catalogo getTipoPublicacion() {
		return this.tipoPublicacion;
	}

	public void setTipoPublicacion(Catalogo tipoPublicacion) {
		this.tipoPublicacion = tipoPublicacion;
	}

	public Flor getFlor() {
		return flor;
	}

	public void setFlor(Flor flor) {
		this.flor = flor;
	}

	public Integer getIdFlor() {
		return idFlor;
	}

	public void setIdFlor(Integer idFlor) {
		this.idFlor = idFlor;
	}

	public Integer getNombreFlor() {
		return nombreFlor;
	}

	public void setNombreFlor(Integer nombreFlor) {
		this.nombreFlor = nombreFlor;
	}

	public Integer getIdTipoPublicacion() {
		return idTipoPublicacion;
	}

	public void setIdTipoPublicacion(Integer idTipoPublicacion) {
		this.idTipoPublicacion = idTipoPublicacion;
	}

	public BigDecimal getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(BigDecimal calificacion) {
		this.calificacion = calificacion;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getTieneTallo() {
		return tieneTallo;
	}

	public void setTieneTallo(String tieneTallo) {
		this.tieneTallo = tieneTallo;
	}

	public String getTieneBoton() {
		return tieneBoton;
	}

	public void setTieneBoton(String tieneBoton) {
		this.tieneBoton = tieneBoton;
	}

	public Date getInicioVigencia() {
		return inicioVigencia;
	}

	public void setInicioVigencia(Date inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	public Date getFinVigencia() {
		return finVigencia;
	}

	public void setFinVigencia(Date finVigencia) {
		this.finVigencia = finVigencia;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}
	

}