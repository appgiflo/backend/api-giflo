package com.giflo.model.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.giflo.model.dto.UsuarioDTO;
import com.giflo.model.enums.ValorSiNoEnum;

/**
 * The persistent class for the f_notificacion_push database table.
 * 
 */
@Entity
@Table(name = "f_contacto")
public class Contacto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2824188751803068257L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "fd_id")
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fd_usuario_origen")
	private Usuario origen;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fd_usuario_destino")
	private Usuario destino;

	@Column(name = "fd_estado")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum activo;

	@Column(name = "fd_fecha_registro")
	private Timestamp fechaRegistro;

	@Transient
	private String nombres;

	@Transient
	private String apellidos;

	@Transient
	private String empresaNombre;

	@Transient
	private Integer idEmpresa;

	@Transient
	private String logoEmpresa;

	@Transient
	private UsuarioDTO usuario;

	public Contacto() {

	}

	public Contacto(Contacto c) {
		id = c.getId();
		fechaRegistro = c.getFechaRegistro();
		nombres = c.getDestino().getNombres();
		apellidos = c.getDestino().getApellidos();
		empresaNombre = c.getDestino().getEmpresa().getNombre();
		idEmpresa = c.getDestino().getEmpresa().getId();
		activo = c.getActivo();
		for (EmpresaFoto f : c.getDestino().getEmpresa().getFotos()) {
			if (ValorSiNoEnum.SI.equals(f.getPrincipal())) {
				this.logoEmpresa = f.getNombre();
				break;
			}
		}
	}

	public Contacto(Contacto c, Integer idNuevo) {
		id = c.getId();
		fechaRegistro = c.getFechaRegistro();
		nombres = c.getDestino().getNombres();
		apellidos = c.getDestino().getApellidos();
		empresaNombre = c.getDestino().getEmpresa().getNombre();
		idEmpresa = c.getDestino().getEmpresa().getId();
		activo = c.getActivo();
		for (EmpresaFoto f : c.getDestino().getEmpresa().getFotos()) {
			if (ValorSiNoEnum.SI.equals(f.getPrincipal())) {
				this.logoEmpresa = f.getNombre();
				break;
			}
		}
		usuario = new UsuarioDTO(c.getDestino(), new Empresa(c.getDestino().getEmpresa()), new ArrayList<>(0),
				new ArrayList<>(0), 0, 0, 0, 0);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Usuario getOrigen() {
		return origen;
	}

	public void setOrigen(Usuario origen) {
		this.origen = origen;
	}

	public Usuario getDestino() {
		return destino;
	}

	public void setDestino(Usuario destino) {
		this.destino = destino;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

	public Timestamp getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Timestamp fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEmpresaNombre() {
		return empresaNombre;
	}

	public void setEmpresaNombre(String empresaNombre) {
		this.empresaNombre = empresaNombre;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getLogoEmpresa() {
		return logoEmpresa;
	}

	public void setLogoEmpresa(String logoEmpresa) {
		this.logoEmpresa = logoEmpresa;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

}