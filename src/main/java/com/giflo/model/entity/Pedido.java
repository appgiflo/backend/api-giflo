package com.giflo.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.giflo.model.enums.EstadoPedidoEnum;

/**
 * The persistent class for the f_publicacion database table.
 * 
 */
@Entity
@Table(name = "a_pedido")
public class Pedido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ap_id")
	private Integer id;

	@Column(name = "ap_observacion")
	private String observacion;

	@Column(name = "ap_estado")
	@Enumerated(EnumType.STRING)
	private EstadoPedidoEnum estado;

	@Column(name = "ap_total")
	private BigDecimal total;

	@Column(name = "ap_fecha_creacion")
	private Timestamp fechaCreacion;

	// bi-directional many-to-one association to DetallePublicacion
	@OneToMany(mappedBy = "pedido")
	private List<DetallePedido> detalles;

	// bi-directional many-to-one association to Empresa
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ap_id_cliente")
	private Usuario cliente;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ap_id_usuario")
	private Usuario usuario;

	@Transient
	private Integer idCliente;

	@Transient
	private Integer idUsuario;

	public Pedido() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public EstadoPedidoEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoPedidoEnum estado) {
		this.estado = estado;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Timestamp getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public List<DetallePedido> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<DetallePedido> detalles) {
		this.detalles = detalles;
	}

	public Usuario getCliente() {
		return cliente;
	}

	public void setCliente(Usuario cliente) {
		this.cliente = cliente;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

}