package com.giflo.model.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the a_catalogo database table.
 * 
 */
@Entity
@Table(name = "f_empleo")
public class Empleo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1012378770066622769L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "em_id")
	private Integer id;

	@Column(name = "em_fecha_registro")
	private Timestamp fechaRegistro;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;

	@Column(name = "em_descripcion")
	private String descripcion;

	@Column(name = "em_cargo")
	private String cargo;

	@Column(name = "em_beneficios")
	private String beneficios;

	@Column(name = "em_estado")
	private String estado;

	@Column(name = "em_jornada")
	private String jornada;

	@Transient
	private String nombres;

	@Transient
	private Integer idUsuario;

	@Transient
	private String empresa;

	@Transient
	private Integer idEmpresa;

	public Empleo() {
	}

	public Empleo(Empleo empleo) {
		super();
		this.id = empleo.id;
		this.fechaRegistro = empleo.fechaRegistro;
		this.descripcion = empleo.descripcion;
		this.cargo = empleo.cargo;
		this.beneficios = empleo.beneficios;
		this.estado = empleo.estado;
		this.jornada = empleo.jornada;
		if (empleo.getUsuario() != null) {
			this.nombres = empleo.getUsuario().NombresCompletos();
			this.idUsuario = empleo.getUsuario().getId();
			this.empresa = empleo.getUsuario().getEmpresa().getNombre();
			this.idEmpresa = empleo.getUsuario().getEmpresa().getId();
		}

	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Timestamp fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getBeneficios() {
		return beneficios;
	}

	public void setBeneficios(String beneficios) {
		this.beneficios = beneficios;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getJornada() {
		return jornada;
	}

	public void setJornada(String jornada) {
		this.jornada = jornada;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

}