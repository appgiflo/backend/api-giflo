package com.giflo.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.giflo.model.enums.ValorEstadoEnum;

/**
 * The persistent class for the a_variedad database table.
 * 
 */
@Entity
@Table(name = "a_producto")
public class Producto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "as_id")
	private Integer id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_empresa")
	private Empresa empresa;

	@Column(name = "as_nombre")
	private String nombre;
	@Column(name = "as_foto_id")
	private String fotoId;

	@Column(name = "as_estado")
	@Enumerated(EnumType.STRING)
	private ValorEstadoEnum estado;

	@Column(name = "as_descripcion")
	private String descripcion;

	@Column(name = "as_fecha_registro")
	private Timestamp fechaRegistro;

	@Column(name = "as_url_foto")
	private String urlFoto;

	@Column(name = "as_precio")
	private BigDecimal precio;

	@Column(name = "as_stock")
	private Integer stock;

	@Transient
	private Integer idEmpresa;
	
	@Transient
	private Integer idUsuario;
	@Transient
	private String nombreEmpresa;

	public Producto() {
		// TODO Auto-generated constructor stub
	}

	public Producto(Producto p) {
		id = p.getId();
		nombre = p.getNombre();
		estado = p.getEstado();
		descripcion = p.getDescripcion();
		urlFoto = p.getUrlFoto();
		precio = p.getPrecio();
		stock = p.getStock();
		idEmpresa = p.getEmpresa().getId();
		nombreEmpresa = p.getEmpresa().getNombre();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ValorEstadoEnum getEstado() {
		return estado;
	}

	public void setEstado(ValorEstadoEnum estado) {
		this.estado = estado;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Timestamp fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getUrlFoto() {
		return urlFoto;
	}

	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public String getFotoId() {
		return fotoId;
	}

	public void setFotoId(String fotoId) {
		this.fotoId = fotoId;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	

}