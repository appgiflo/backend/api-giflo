package com.giflo.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the f_notificacion_push database table.
 * 
 */
@Entity
@Table(name = "f_notificacion_push")
public class NotificacionPush implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2824188751803068257L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "np_id")
	private Integer id;

	@Column(name = "np_origen")
	private Integer origen;

	@Column(name = "np_destino")
	private Integer destino;

	public NotificacionPush() {
		
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getOrigen() {
		return origen;
	}

	public void setOrigen(Integer origen) {
		this.origen = origen;
	}

	public Integer getDestino() {
		return destino;
	}

	public void setDestino(Integer destino) {
		this.destino = destino;
	}

}