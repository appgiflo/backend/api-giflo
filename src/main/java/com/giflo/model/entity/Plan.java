package com.giflo.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.giflo.model.enums.GrupoSaldoEnum;
import com.giflo.model.enums.ValorEstadoPlanEnum;

/**
 * The persistent class for the a_plan database table.
 * 
 */
@Entity
@Table(name = "a_plan")
public class Plan implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2824188751803068257L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pl_id")
	private Integer id;

	@Column(name = "pl_nombre")
	private String nombre;

	@Column(name = "pl_valor")
	private BigDecimal valor;

	@Column(name = "pl_cantidad_contactos")
	private Integer cantidadContactos;

	@Column(name = "pl_color_primario")
	private String colorPrimario;

	@Column(name = "pl_color_secundario")
	private String colorSecundario;

	@Column(name = "pl_color_texto")
	private String colorTexto;

	@Column(name = "pl_estado")
	@Enumerated(EnumType.STRING)
	private ValorEstadoPlanEnum estado;

	@Column(name = "pl_orden")
	private Integer orden;

	@Column(name = "pl_grupo")
	@Enumerated(EnumType.STRING)
	private GrupoSaldoEnum grupo;

	public Plan() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Integer getCantidadContactos() {
		return cantidadContactos;
	}

	public void setCantidadContactos(Integer cantidadContactos) {
		this.cantidadContactos = cantidadContactos;
	}

	public String getColorPrimario() {
		return colorPrimario;
	}

	public void setColorPrimario(String colorPrimario) {
		this.colorPrimario = colorPrimario;
	}

	public String getColorSecundario() {
		return colorSecundario;
	}

	public void setColorSecundario(String colorSecundario) {
		this.colorSecundario = colorSecundario;
	}

	public String getColorTexto() {
		return colorTexto;
	}

	public void setColorTexto(String colorTexto) {
		this.colorTexto = colorTexto;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ValorEstadoPlanEnum getEstado() {
		return estado;
	}

	public void setEstado(ValorEstadoPlanEnum estado) {
		this.estado = estado;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public GrupoSaldoEnum getGrupo() {
		return grupo;
	}

	public void setGrupo(GrupoSaldoEnum grupo) {
		this.grupo = grupo;
	}

}