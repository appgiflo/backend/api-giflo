package com.giflo.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.giflo.model.enums.ValorEstadoEnum;

/**
 * The persistent class for the f_publicacion database table.
 * 
 */
@Entity
@Table(name = "f_disponibilidad")
public class Disponibilidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "fd_id")
	private Integer id;

	@Column(name = "fd_estado")
	@Enumerated(EnumType.STRING)
	private ValorEstadoEnum estado;

	@Temporal(TemporalType.DATE)
	@Column(name = "fd_fecha_actualizacion")
	private Date fechaActualizacion;

	// uni-directional many-to-one association to Catalogo
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_unidad_medida")
	private Catalogo unidadMedida;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_longitud")
	private Catalogo longitud;

	// bi-directional many-to-one association to Empresa
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_flor")
	private Flor flor;

	@Column(name = "fd_precio")
	private BigDecimal precio;

	@Column(name = "fd_cantidad")
	private BigDecimal cantidad;

	@Transient
	private String valorUnidadMedida;
	@Transient
	private String codigoUM;
	@Transient
	private Integer idUnidadMedida;

	@Transient
	private String valorLongitud;
	@Transient
	private Integer idLongitud;

	@Transient
	private Integer idFlor;
	@Transient
	private String variedad;

	@Transient
	private Integer idVariedad;

	@Transient
	private String empresaNombre;

	@Transient
	private Integer empresaId;

	@Transient
	private String paisNombre;

	@Transient
	private Integer paisId;

	public Disponibilidad() {
		// TODO Auto-generated constructor stub
	}

	public Disponibilidad(BigDecimal cantidad, String valorLongitud, Integer idLongitud, String variedad,
			Integer idVariedad, String paisNombre, Integer paisId) {
		super();
		this.cantidad = cantidad;
		this.valorLongitud = valorLongitud;
		this.idLongitud = idLongitud;
		this.variedad = variedad;
		this.idVariedad = idVariedad;
		this.paisNombre = paisNombre;
		this.paisId = paisId;
	}
	

	public Disponibilidad(BigDecimal cantidad, String variedad, Integer idVariedad, String paisNombre, Integer paisId,Boolean active) {
		super();
		this.cantidad = cantidad;
		this.variedad = variedad;
		this.idVariedad = idVariedad;
		this.paisNombre = paisNombre;
		this.paisId = paisId;
	}

	public Disponibilidad(BigDecimal cantidad, String variedad, Integer idVariedad) {
		super();
		this.cantidad = cantidad;
		this.variedad = variedad;
		this.idVariedad = idVariedad;
	}

	public Disponibilidad(BigDecimal cantidad, String variedad, Integer idVariedad, String valorLongitud,
			Integer idLongitud) {
		super();
		this.cantidad = cantidad;
		this.valorLongitud = valorLongitud;
		this.idLongitud = idLongitud;
		this.variedad = variedad;
		this.idVariedad = idVariedad;
	}

	public Disponibilidad(Disponibilidad d) {
		id = d.getId();
		estado = d.getEstado();
		fechaActualizacion = d.getFechaActualizacion();
		precio = d.getPrecio();
		cantidad = d.getCantidad();
		valorUnidadMedida = d.getUnidadMedida() != null ? d.getUnidadMedida().getNombre() : null;
		idUnidadMedida = d.getUnidadMedida() != null ? d.getUnidadMedida().getId() : null;
		codigoUM = d.getUnidadMedida() != null ? d.getUnidadMedida().getCodigo() : null;
		valorLongitud = d.getLongitud() != null ? d.getLongitud().getNombre() : null;
		idLongitud = d.getLongitud() != null ? d.getLongitud().getId() : null;
		idFlor = d.getFlor() != null ? d.getFlor().getId() : null;
		variedad = d.getFlor() != null ? d.getFlor().getVariedad().getNombre() : null;
		empresaNombre = d.getFlor().getEmpresa().getNombre();
		empresaId = d.getFlor().getEmpresa().getId();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ValorEstadoEnum getEstado() {
		return estado;
	}

	public void setEstado(ValorEstadoEnum estado) {
		this.estado = estado;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public Catalogo getUnidadMedida() {
		return unidadMedida;
	}

	public void setUnidadMedida(Catalogo unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public Flor getFlor() {
		return flor;
	}

	public void setFlor(Flor flor) {
		this.flor = flor;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public BigDecimal getCantidad() {
		return cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public String getValorUnidadMedida() {
		return valorUnidadMedida;
	}

	public void setValorUnidadMedida(String valorUnidadMedida) {
		this.valorUnidadMedida = valorUnidadMedida;
	}

	public Integer getIdUnidadMedida() {
		return idUnidadMedida;
	}

	public void setIdUnidadMedida(Integer idUnidadMedida) {
		this.idUnidadMedida = idUnidadMedida;
	}

	public Integer getIdFlor() {
		return idFlor;
	}

	public void setIdFlor(Integer idFlor) {
		this.idFlor = idFlor;
	}

	public String getVariedad() {
		return variedad;
	}

	public void setVariedad(String variedad) {
		this.variedad = variedad;
	}

	public Catalogo getLongitud() {
		return longitud;
	}

	public void setLongitud(Catalogo longitud) {
		this.longitud = longitud;
	}

	public String getValorLongitud() {
		return valorLongitud;
	}

	public void setValorLongitud(String valorLongitud) {
		this.valorLongitud = valorLongitud;
	}

	public Integer getIdLongitud() {
		return idLongitud;
	}

	public void setIdLongitud(Integer idLongitud) {
		this.idLongitud = idLongitud;
	}

	public String getCodigoUM() {
		return codigoUM;
	}

	public void setCodigoUM(String codigoUM) {
		this.codigoUM = codigoUM;
	}

	public Integer getIdVariedad() {
		return idVariedad;
	}

	public void setIdVariedad(Integer idVariedad) {
		this.idVariedad = idVariedad;
	}

	public String getEmpresaNombre() {
		return empresaNombre;
	}

	public void setEmpresaNombre(String empresaNombre) {
		this.empresaNombre = empresaNombre;
	}

	public Integer getEmpresaId() {
		return empresaId;
	}

	public void setEmpresaId(Integer empresaId) {
		this.empresaId = empresaId;
	}

	public String getPaisNombre() {
		return paisNombre;
	}

	public void setPaisNombre(String paisNombre) {
		this.paisNombre = paisNombre;
	}

	public Integer getPaisId() {
		return paisId;
	}

	public void setPaisId(Integer paisId) {
		this.paisId = paisId;
	}
	

}