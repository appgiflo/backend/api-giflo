package com.giflo.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the a_catalogo database table.
 * 
 */
@Entity
@Table(name = "s_device_token")
public class DeviceToken implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "dt_id")
	private Integer id;

	@Column(name = "dt_token")
	private String token;

	@Column(name = "dt_os")
	private String os;

	@Column(name = "dt_modelo")
	private String modelo;

	@Column(name = "dt_marca")
	private String marca;
	
	@Column(name = "dt_version_sistema")
	private String versionSistem;
	
	@Column(name = "dt_version_app")
	private String versionApp;

	public DeviceToken() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getVersionSistem() {
		return versionSistem;
	}

	public void setVersionSistem(String versionSistem) {
		this.versionSistem = versionSistem;
	}

	public String getVersionApp() {
		return versionApp;
	}

	public void setVersionApp(String versionApp) {
		this.versionApp = versionApp;
	}
	
	

}