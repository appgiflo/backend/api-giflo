package com.giflo.model.enums;

public enum TemplateEmailEnum {
	REGISTRO("REGISTRO"), ACTUALIZACION("ACTUALIZACION"), CAMBIO_CLAVE("CAMBIO_CLAVE"),ENVIO_OTP("ENVIO_OTP"),CAMBIO_PASS_EXITO("CAMBIO_PASS_EXITO");

	private String descripcion;

	private TemplateEmailEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
