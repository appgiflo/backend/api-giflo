package com.giflo.model.enums;

public enum ValorEstadoCuponEnum {
	ACTIVO("Activo"), INACTIVO("Inactivo"), FINALIZADO("Finalizado");

	private String descripcion;

	private ValorEstadoCuponEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
