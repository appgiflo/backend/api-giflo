package com.giflo.model.enums;

public enum ValorEstadoEnum {
	ACTIVO("Activo"), INACTIVO("Inactivo");

	private String descripcion;

	private ValorEstadoEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
