package com.giflo.model.enums;

public enum ValorEstadoOtpEnum {
	ENVIADO("Enviado"), VALIDADO("Validado"),APROBADO("Aprobado");

	private String descripcion;

	private ValorEstadoOtpEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
