package com.giflo.model.enums;

public enum ValorEstadoPlanEnum {
	ACTIVO("Activo"), INACTIVO("Inactivo");

	private String descripcion;

	private ValorEstadoPlanEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
