package com.giflo.model.enums;

public enum ValorEstadoPagoPlanEnum {
	PENDIENTE("Pendiente"), VIGENTE("Vigente"), CADUCADO("Caducado");

	private String descripcion;

	private ValorEstadoPagoPlanEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
