package com.giflo.model.enums;

public enum ValorSiNoEnum {
	SI("Si"), NO("No");

	private String descripcion;

	private ValorSiNoEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
