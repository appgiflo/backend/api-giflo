package com.giflo.model.enums;

public enum EstadoPedidoEnum {
	INGRESADO("INGRESADO"), RECEPCIONADO("RECEPCIONADO"),PROCESADO("PROCESADO"),PAGADO("PAGADO");

	private String descripcion;

	private EstadoPedidoEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
