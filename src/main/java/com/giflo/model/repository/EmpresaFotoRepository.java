package com.giflo.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.model.entity.EmpresaFoto;
import com.giflo.model.enums.ValorEstadoEnum;
import com.giflo.model.enums.ValorSiNoEnum;

@RepositoryRestResource(path = "empresaFoto")
public interface EmpresaFotoRepository extends PagingAndSortingRepository<EmpresaFoto, Integer> {

	@RestResource(path = "findOfEmpresa")
	@Query("select new EmpresaFoto(ef) from EmpresaFoto ef inner join ef.empresa e where e.id=:idEmpresa and ef.principal=:principal and ef.estado=:estado")
	List<EmpresaFoto> findOfEmpresa(@Param(value = "idEmpresa") Integer empresa,
			@Param(value = "principal") ValorSiNoEnum principal, @Param(value = "estado") ValorEstadoEnum estado);
}
