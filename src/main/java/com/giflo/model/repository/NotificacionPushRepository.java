package com.giflo.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.model.entity.NotificacionPush;

@RepositoryRestResource(path = "notifypush")
public interface NotificacionPushRepository extends PagingAndSortingRepository<NotificacionPush, Integer> {

	@RestResource(path = "by-origen")
	List<NotificacionPush> findByOrigen(@Param(value = "q") Integer origen);

	@RestResource(path = "by-destino")
	List<NotificacionPush> findByDestino(@Param(value = "q") Integer destino);	
	
	@RestResource(path = "by-origen-destino")
	NotificacionPush findByOrigenAndDestino(@Param(value = "p") Integer origen, @Param(value = "q") Integer destino);
	
	@Query( value  = "select n.np_destino from f_notificacion_push n where n.np_origen=:id",nativeQuery = true)
	List<Object[]> findUserNotify(@Param(value = "id") Integer idUser);
	
	@Query( value  = "select n.np_destino from f_notificacion_push n where n.np_destino!=:id",nativeQuery = true)
	List<Object[]> findUserAll(@Param(value = "id") Integer idUser);
	
}
