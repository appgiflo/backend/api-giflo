package com.giflo.model.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.giflo.model.entity.DeviceToken;

@RepositoryRestResource(path = "deviceToken")
public interface DeviceTokenRepository extends PagingAndSortingRepository<DeviceToken, Integer> {

	List<DeviceToken> findByToken(@Param(value = "token") String token);

}
