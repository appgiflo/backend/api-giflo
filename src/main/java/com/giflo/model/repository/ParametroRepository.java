package com.giflo.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.model.entity.Parametro;

@RepositoryRestResource(path = "parametro")
public interface ParametroRepository extends JpaRepository<Parametro, String> {

	@RestResource(path = "by-id")
	@Query("select new Parametro(e.id,e.valor) from Parametro e where e.id=:id and e.activo='SI'")
	Parametro buscarPorId(@Param(value = "id") String id);
}
