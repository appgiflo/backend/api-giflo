package com.giflo.model.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.model.dto.PublicacionDTO;
import com.giflo.model.entity.Publicacion;

@RepositoryRestResource(path = "publicacion")
public interface PublicacionRepository extends PagingAndSortingRepository<Publicacion, Integer> {

	@Query("select distinct p from Publicacion p inner join p.flor f inner join f.variedad v inner join v.color c inner join f.empresa emp where ( ((p.tipoPublicacion.id=:tipoPublicacion or :tipoPublicacion=-1) and (c.id=:idColor or :idColor=41)) or (upper(v.nombre) like upper(concat('%',:filtro,'%')) and :filtro!='')  or (upper(emp.nombre) like upper(concat('%',:filtro,'%'))  and :filtro!='')) and p.estado='ACTIVO' and f.disponible='SI' order by p.fechaPublicacion desc ")
	Page<Publicacion> findByColorAndPublicacion(@Param(value = "tipoPublicacion") Integer idTipoPublicacion,
			@Param(value = "idColor") Integer idColor, @Param(value = "filtro") String filtro, Pageable pageable);

	@Query("select distinct p from Publicacion p inner join p.flor f inner join f.variedad v inner join v.color c inner join f.empresa emp where ( ((p.tipoPublicacion.id=:tipoPublicacion or :tipoPublicacion=-1) and (c.id=:idColor or :idColor=41)) or (upper(v.nombre) like upper(concat('%',:filtro,'%')) and :filtro!='')  or (upper(emp.nombre) like upper(concat('%',:filtro,'%'))  and :filtro!='')) and p.estado='ACTIVO' and f.disponible='SI' and (p in (select dep.publicacion from DetallePublicacion dep where dep.largoTallo.id=:idTallo and dep.publicacion=p) or :idTallo=-1) order by p.fechaPublicacion desc ")
	Page<Publicacion> findByColorAndPublicacionAndTallo(@Param(value = "tipoPublicacion") Integer idTipoPublicacion,
			@Param(value = "idColor") Integer idColor, @Param(value = "filtro") String filtro, Pageable pageable,
			@Param(value = "idTallo") Integer idTallo);

	@Query("select distinct p from Publicacion p inner join p.flor f inner join f.variedad v inner join v.color c inner join f.empresa emp where ((upper(v.nombre) like upper(concat('%',:filtro,'%')))  or (upper(emp.nombre) like upper(concat('%',:filtro,'%')))) and p.estado='ACTIVO' and :fechaHoy between p.inicioVigencia and p.finVigencia and f.disponible='SI' order by p.fechaPublicacion desc ")
	Page<Publicacion> findByVariedadAndEmpresa(@Param(value = "filtro") String filtro,
			@Param(value = "fechaHoy") Date fechaHoy, Pageable pageable);

	@Query("select p from Publicacion p inner join p.flor f inner join f.empresa e inner join e.usuarios u  where u.id=:idUsuario order by p.fechaPublicacion desc ")
	List<Publicacion> findByUsuario(@Param(value = "idUsuario") Integer idUsuario);

	@Query("select p from Publicacion p inner join p.flor f inner join f.empresa e inner join e.usuarios u  where u.id=:idUsuario and :fechaHoy between p.inicioVigencia and p.finVigencia order by p.fechaPublicacion desc ")
	List<Publicacion> findByUsuarioVigente(@Param(value = "idUsuario") Integer idUsuario,
			@Param(value = "fechaHoy") Date fechaHoy);

	@Query("select p from Publicacion p where p.flor.empresa.id=:idEmpresa and p.id!=:idPublicacion and :fechaHoy between p.inicioVigencia and p.finVigencia order by p.fechaPublicacion desc ")
	List<Publicacion> findByEmpresaVigente(@Param(value = "idEmpresa") Integer idEmpresa,
			@Param(value = "idPublicacion") Integer idPublicacion, @Param(value = "fechaHoy") Date fechaHoy);

	@RestResource(path = "by-id")
	@Query("select new com.giflo.model.dto.PublicacionDTO(e) from Publicacion e where e.id=:id")
	PublicacionDTO buscarPorId(@Param(value = "id") Integer id);

}
