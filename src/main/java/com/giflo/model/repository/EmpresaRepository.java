package com.giflo.model.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.model.entity.Empresa;

@RepositoryRestResource(path = "empresa")
public interface EmpresaRepository extends PagingAndSortingRepository<Empresa, Integer> {
	
	@RestResource(path = "by-user")
	@Query("select e from Usuario u inner join u.empresa e where u.id=:id and e.activo=:estado")
	List<Empresa> findByUserAndActivo(@Param(value = "id") Integer id, @Param(value = "estado") String activo);

	@RestResource(path = "filter")
	List<Empresa> findByRucContaining(@Param(value = "q") String ruc);

	@RestResource(path = "by-id")
	@Query("select new Empresa(e) from Empresa e where e.id=:id")
	Empresa buscarPorId(@Param(value = "id") Integer id);

	@Query("select emp from Empresa emp inner join emp.usuarios u where ( emp.tipo.id=:tipoId or :tipoId=-1 ) and ( upper(emp.nombre) like upper(concat('%',:filtro,'%')) or upper(emp.tipo.nombre) like upper(concat('%',:filtro,'%'))) and u.esAdmin='NO'")
	Page<Empresa> findByTypeAndNombre(@Param(value = "tipoId") Integer  tipoId, @Param(value = "filtro") String filtro, Pageable pageable);
	
	@Query("select distinct emp from Empresa emp inner join emp.usuarios u inner join emp.direcciones dir where  u.esAdmin='NO' and dir.latitud!='0.0' and dir.longitud!='0.0'")
	List<Empresa> findByTypeAndNombreAndHasMap();

}
