package com.giflo.model.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.giflo.model.entity.DetallePublicacion;

@RepositoryRestResource(path = "detallepublicacion")
public interface DetallePublicacionRepository extends PagingAndSortingRepository<DetallePublicacion, Integer> {

	
}
