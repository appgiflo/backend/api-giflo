package com.giflo.model.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.model.entity.Empresa;
import com.giflo.model.entity.Resenia;
import com.giflo.model.entity.Usuario;

@RepositoryRestResource(path = "resenia")
public interface ReseniaRepository extends PagingAndSortingRepository<Resenia, Integer> {
	
	@RestResource(path = "by-empresa")
	List<Resenia> findByEmpresa(Empresa empresa);
	
	@RestResource(path = "by-empresa-user")
	Resenia findByEmpresaAndUsuario(Empresa empresa,  Usuario usuario);
	
	@Query("select r from Resenia r where r.empresa.id=:idEmpresa order by r.fechaRegistro desc ")
	Page<Resenia> findByEmpresaResenia(@Param(value = "idEmpresa") Integer idEmpresa, Pageable pageable);
	

}
