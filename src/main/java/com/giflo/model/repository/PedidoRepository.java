package com.giflo.model.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.giflo.model.entity.Pedido;

@RepositoryRestResource(path = "pedido")
public interface PedidoRepository extends PagingAndSortingRepository<Pedido, Integer> {

	

}
