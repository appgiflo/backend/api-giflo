package com.giflo.model.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.model.entity.Variedad;

@RepositoryRestResource(path = "variedad")
public interface VariedadRepository extends PagingAndSortingRepository<Variedad, Integer> {

	@Query("select new Variedad(f.id,f.indicaciones,f.nombre,f.nombreFoto,f.fotoPequenia) from Variedad f where f.activo='SI' order by f.nombre")
	List<Variedad> todas();
	
	
	@RestResource(path = "byLikeNombre", rel = "byLikeNombre")
	public Page<Variedad> findByActivoAndNombreContainingIgnoreCaseOrderByNombreDesc(@Param("activo") String activo,@Param("nombre") String nombre, Pageable p);
	
	@Query("select new Variedad(f.color,f.id,f.indicaciones,f.nombre,f.nombreFoto,f.fotoPequenia,f.activo) from Variedad f order by f.nombre")
	@RestResource(path = "filter", rel = "filter")
	Page<Variedad> filter(Pageable p);
	
	@Query("select new Variedad(f.color,f.id,f.indicaciones,f.nombre,f.nombreFoto,f.fotoPequenia,f.activo) from Variedad f where f.id=:idVariedad")
	@RestResource(path = "findByIdCustom", rel = "findByIdCustom")
	Variedad findByIdCustom(@Param("idVariedad") Integer idVariedad);
}
