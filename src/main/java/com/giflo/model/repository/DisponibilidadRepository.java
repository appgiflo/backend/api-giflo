package com.giflo.model.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.giflo.model.entity.Disponibilidad;

@RepositoryRestResource(path = "disponibilidad")
public interface DisponibilidadRepository extends PagingAndSortingRepository<Disponibilidad, Integer> {

	@Query("select new Disponibilidad(di) from Disponibilidad di where di.flor.empresa.id=:idEmpresa and di.fechaActualizacion=:fechaActualizacion")
	List<Disponibilidad> obtenerDisponibilidad(@Param(value = "idEmpresa") Integer idEmpresa,
			@Param(value = "fechaActualizacion") Date fechaActualizacion);

	@Query("select new Disponibilidad(di) from Disponibilidad di where  di.flor.id=:idFlor and di.longitud is null")
	List<Disponibilidad> obtenerDisponibilidadPorFlorAndTallo(@Param(value = "idFlor") Integer idFlor);

	@Query("select new Disponibilidad(di) from Disponibilidad di where  di.flor.id=:idFlor and di.longitud is not null order by di.flor.variedad.nombre, di.longitud.orden")
	List<Disponibilidad> obtenerDisponibilidadPorFlorAndBonches(@Param(value = "idFlor") Integer idFlor);

	@Query("select new Disponibilidad(di) from Disponibilidad di inner join di.flor flor inner join flor.empresa emp inner join emp.direcciones dir where  flor.variedad.id=:idVariedad and di.longitud is null and dir.id in (select dire.id from Direccion dire where dire.pais.id=:idPais) and di.cantidad>0  order by emp.nombre,flor.variedad.nombre ")
	List<Disponibilidad> obtenerDisponibilidadPorVariedad(@Param(value = "idVariedad") Integer idVariedad,
			@Param(value = "idPais") Integer idPais);

	@Query("select new Disponibilidad(di) from Disponibilidad di inner join di.flor flor inner join flor.empresa emp inner join emp.direcciones dir where  flor.variedad.id=:idVariedad and di.longitud is not null and di.longitud.id=:idLongitud and dir.id in (select dire.id from Direccion dire where dire.pais.id=:idPais) and di.cantidad>0 order by emp.nombre,flor.variedad.nombre ")
	List<Disponibilidad> obtenerDisponibilidadPorVariedadAndLongitud(@Param(value = "idVariedad") Integer idVariedad,
			@Param(value = "idPais") Integer idPais, @Param(value = "idLongitud") Integer idLongitud);

	@Query("select sum(di.cantidad),longitud.nombre,longitud.id,varie.nombre,varie.id,pais.nombre,pais.id from Disponibilidad di inner join di.flor flor inner join flor.variedad varie inner join di.longitud longitud inner join flor.empresa emp inner join emp.direcciones dir inner join dir.pais pais  where di.fechaActualizacion=:fechaActualizacion and di.longitud is not null group by pais.nombre,pais.id,varie.nombre,varie.id,longitud.nombre,longitud.id order by pais.nombre,varie.nombre,longitud.orden asc")
	List<Object[]> getDisponibilidadBonches(@Param(value = "fechaActualizacion") Date fechaActualizacion);

	@Query("select sum(di.cantidad),varie.nombre,varie.id,pais.nombre,pais.id from Disponibilidad di inner join di.flor flor inner join flor.variedad varie inner join flor.empresa emp inner join emp.direcciones dir inner join dir.pais pais  where di.fechaActualizacion=:fechaActualizacion and di.longitud is null group by pais.nombre,pais.id,varie.nombre,varie.id order by pais.nombre,varie.nombre")
	List<Object[]> getDisponibilidadVariedad(@Param(value = "fechaActualizacion") Date fechaActualizacion);

}
