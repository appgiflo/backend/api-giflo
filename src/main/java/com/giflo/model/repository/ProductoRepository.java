package com.giflo.model.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.giflo.model.entity.Producto;

@RepositoryRestResource(path = "producto")
public interface ProductoRepository extends PagingAndSortingRepository<Producto, Integer> {

	@Query("select p from Producto p inner join p.empresa emp where ((upper(p.nombre) like upper(concat('%',:filtro,'%')))  or (upper(emp.nombre) like upper(concat('%',:filtro,'%')))) and p.estado='ACTIVO'  order by p.nombre desc ")
	Page<Producto> findByPage(@Param(value = "filtro") String filtro, Pageable pageable);
	
	@Query("select p from Producto p inner join p.empresa em where p.empresa.id=:idEmpresa order by p.nombre")
	List<Producto> findByEmpresa(@Param(value = "idEmpresa")  Integer idEmpresa);
}
