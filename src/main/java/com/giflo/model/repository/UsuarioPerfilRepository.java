package com.giflo.model.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.giflo.model.entity.UsuarioPerfil;

@RepositoryRestResource(path = "usuarioperfil")
public interface UsuarioPerfilRepository extends PagingAndSortingRepository<UsuarioPerfil, Integer> {

	
}
