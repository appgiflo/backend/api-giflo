package com.giflo.model.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.giflo.model.entity.Cupon;

@RepositoryRestResource(path = "cupon")
public interface CuponRepository extends PagingAndSortingRepository<Cupon, Integer> {
	
	@Query("select c from Cupon c where c.codigo=:codCupon ")
	Cupon findByCodigo(@Param(value = "codCupon") String codCupon);
	
	@Query("select c from Cupon c where c.codigo=:codCupon and c.idPlan=:idPlan")
	Cupon findByCodigoAndPlan(@Param(value = "codCupon") String codCupon,  @Param(value = "idPlan") Integer idPlan);
	
}
