package com.giflo.model.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.giflo.model.entity.DetallePedido;

@RepositoryRestResource(path = "detallepedido")
public interface DetallePedidoRepository extends PagingAndSortingRepository<DetallePedido, Integer> {

	

}
