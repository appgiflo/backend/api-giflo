package com.giflo.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.giflo.model.entity.Otp;

@RepositoryRestResource(path = "otp")
public interface OtpRepository extends PagingAndSortingRepository<Otp, Integer> {

	@Query("select f from Otp f where f.contacto=:id")
	List<Otp> buscar(@Param(value = "id") String contacto);
	
}
