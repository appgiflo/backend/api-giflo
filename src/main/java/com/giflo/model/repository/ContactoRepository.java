package com.giflo.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.model.entity.Contacto;
import com.giflo.model.entity.Usuario;

@RepositoryRestResource(path = "contacto")
public interface ContactoRepository extends PagingAndSortingRepository<Contacto, Integer> {

	@RestResource(path = "by-origen-destino")
	Contacto findByOrigenAndDestino(@Param(value = "p") Usuario origen, @Param(value = "q") Usuario destino);

	@RestResource(path = "by-origen")
	@Query("select new Contacto(cat,1) from Contacto cat where cat.origen.id=:q and cat.activo='SI' order by cat.fechaRegistro desc")
	List<Contacto> buscarPorOrigen(@Param(value = "q") Integer idUsuarioOrigen);

	@RestResource(path = "byOrigenDestino")
	@Query("select count(cat.id) from Contacto cat where cat.origen.id=:p and cat.destino.id=:q and cat.activo='SI'")
	Integer buscarPorOrigen(@Param(value = "p") Integer idUsuarioOrigen, @Param(value = "q") Integer idUsuarioDestino);
}
