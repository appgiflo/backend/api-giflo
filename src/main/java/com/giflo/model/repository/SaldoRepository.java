package com.giflo.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.model.entity.Saldo;
import com.giflo.model.enums.GrupoSaldoEnum;

@RepositoryRestResource(path = "saldo")
public interface SaldoRepository extends PagingAndSortingRepository<Saldo, Integer> {

	@Query("select s from Saldo s where s.usuario.id=:usuario and s.grupo=:grupo")
	List<Saldo> findByUsuarioAndTipo(@Param(value = "usuario") Integer idUsuario, @Param(value = "grupo") GrupoSaldoEnum grupo);

	@RestResource(path = "obtenerSaldo")
	@Query("select s from Saldo s inner join s.usuario u where u.id=:p and s.grupo=:q")
	List<Saldo> buscarPorOrigen(@Param(value = "p") Integer idUsuario,@Param(value = "q") GrupoSaldoEnum grupo);
}
