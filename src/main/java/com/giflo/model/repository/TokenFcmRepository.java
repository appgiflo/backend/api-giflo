package com.giflo.model.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.model.entity.TokenFcm;
import com.giflo.model.entity.Usuario;

@RepositoryRestResource(path = "tokenfcm")
public interface TokenFcmRepository extends PagingAndSortingRepository<TokenFcm, Integer> {

	@RestResource(path = "by-user")
	List<TokenFcm> findByUsuario(@Param(value = "q") Usuario u);

	@RestResource(path = "by-token")
	List<TokenFcm> findByTokenAndUsuario(@Param(value = "q") String token, @Param(value = "user") Usuario user);
	
	@RestResource(path = "by-device")
	List<TokenFcm> findByUsuarioAndDevice(@Param(value = "user") Usuario user, @Param(value = "q") String device);
	
	List<TokenFcm> findByToken( @Param(value = "token") String token);
	
}
