package com.giflo.model.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.giflo.model.entity.Empleo;

@RepositoryRestResource(path = "empleo")
public interface EmpleoRepository extends PagingAndSortingRepository<Empleo, Integer> {

	@Query("select cat from Empleo cat inner join cat.usuario use inner join use.empresa emp where (upper(cat.descripcion) like upper(concat('%',:filtro,'%')) or upper(cat.cargo) like upper(concat('%',:filtro,'%')) or upper(cat.jornada) like upper(concat('%',:filtro,'%')) or upper(emp.nombre) like upper(concat('%',:filtro,'%'))) and cat.estado='ACTIVO' order by cat.fechaRegistro desc")
	Page<Empleo> filterActive(@Param("filtro") String filtro, Pageable pageable);

	@Query("select cat from Empleo cat inner join cat.usuario use inner join use.empresa emp where (upper(cat.descripcion) like upper(:filtro) or upper(cat.cargo) like upper(:filtro) or upper(cat.jornada) like upper(:filtro) or upper(emp.nombre) like upper(:filtro)) and cat.usuario.id=:idUsuario order by cat.fechaRegistro desc")
	Page<Empleo> filterByUsuario(@Param("filtro") String filtro, @Param("idUsuario") Integer idUsuario,
			Pageable pageable);

	@Query("select new Empleo(cat) from Empleo cat inner join cat.usuario us inner join us.empresa emp where cat.id=:idEmpleo")
	List<Empleo> filterById(@Param("idEmpleo") Integer idEmpleo);

}
