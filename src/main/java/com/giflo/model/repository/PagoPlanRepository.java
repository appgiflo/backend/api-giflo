package com.giflo.model.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.model.entity.PagoPlan;
import com.giflo.model.entity.Usuario;

@RepositoryRestResource(path = "pagoplan")
public interface PagoPlanRepository extends PagingAndSortingRepository<PagoPlan, Integer> {

	@RestResource(path = "by-usuario")
	List<PagoPlan> findByUsuario(Usuario usuario);

	@Query("select s from PagoPlan s where s.id=:idPagoPlan ")
	PagoPlan findByIdPagoPlan(@Param(value = "idPagoPlan") Integer idPagoPlan);

	@Query("select s from PagoPlan s where s.usuario.id=:idUsuario order by s.fechaRegistro desc ")
	List<PagoPlan> findByIdUsuario(@Param(value = "idUsuario") Integer idUsuario);

	@Query("select s from PagoPlan s where s.usuario.id=:idUsuario and s.estado=:estado order by s.fechaRegistro desc ")
	List<PagoPlan> findByUsuarioAndVigente(@Param(value = "idUsuario") Integer idUsuario,
			@Param(value = "estado") String estado);

	@Query("select s from PagoPlan s where s.usuario.id=:idUsuario and s.estado='VIGENTE'")
	List<PagoPlan> findVigenteByIdUsuario(@Param(value = "idUsuario") Integer idUsuario);

	@Query("select s from PagoPlan s where (upper(s.usuario.correo)=upper(:filter) or upper(s.usuario.nombres)=upper(:filter) or upper(s.usuario.apellidos)=upper(:filter) or :filter='-1') and ( s.codigoPago=:codigoPago or :codigoPago='-1') order by s.fechaRegistro desc ")
	Page<PagoPlan> findByVariedadAndEmpresa(@Param(value = "filter") String filtro,
			@Param(value = "codigoPago") String codigoPago, Pageable pageable);
	
	
	@Query("select s from PagoPlan s where s.usuario.id=:idUsuario and s.estado='PENDIENTE'")
	List<PagoPlan> findPendienteByIdUsuario(@Param(value = "idUsuario") Integer idUsuario);
	
	

}
