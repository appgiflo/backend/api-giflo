package com.giflo.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.model.entity.Catalogo;
import com.giflo.model.enums.ValorSiNoEnum;


@RepositoryRestResource(path = "catalogo")
public interface CatalogoRepository extends PagingAndSortingRepository<Catalogo, Integer> {

	@RestResource(path = "byByGroup")
	@Query("select new Catalogo(cat) from Catalogo cat where cat.grupo=:grupo and cat.activo='SI' order by cat.orden asc")
	List<Catalogo> findByGrupoAndActivoOrderByOrdenAsc(@Param("grupo") String grupo);
	
	
	Catalogo findByCodigoAndActivo(@Param("codigo") String codigo,@Param("Activo") ValorSiNoEnum activo);
	
	@RestResource(path = "by-codigo")
	@Query("select new Catalogo(cat) from Catalogo cat where cat.codigo=:codigo and cat.activo='SI'")
	Catalogo buscarPorCodigo(@Param(value = "codigo") String id);
	
}
