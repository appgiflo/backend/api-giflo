package com.giflo.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.model.entity.Perfil;
import com.giflo.model.enums.ValorSiNoEnum;

@RepositoryRestResource(path = "perfil")
public interface PerfilRepository extends PagingAndSortingRepository<Perfil, Integer> {

	@RestResource(path = "by-user")
	@Query("select new Perfil(per.id,per.activo,per.nombre) from Perfil per inner join per.usuarioPerfiles up where up.usuario.id=:id and up.activo=:estado and per.activo=:estado")
	List<Perfil> findByUserAndActivo(@Param(value = "id") Integer id, @Param(value = "estado") ValorSiNoEnum activo);
}
