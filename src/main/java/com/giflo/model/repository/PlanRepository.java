package com.giflo.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.model.entity.Plan;
import com.giflo.model.enums.GrupoSaldoEnum;

@RepositoryRestResource(path = "plan")
public interface PlanRepository extends PagingAndSortingRepository<Plan, Integer> {
	
	@Query("select p from Plan p where p.id=:idPlan ")
	Plan findByIdPlan(@Param(value = "idPlan") Integer idPlan);
	
	@RestResource(path = "by-activo")
	@Query("select p from Plan p where p.estado='ACTIVO' and p.grupo=:p order by p.orden")
	List<Plan> findAllActive(@Param(value = "p") GrupoSaldoEnum grupo);
	
}
