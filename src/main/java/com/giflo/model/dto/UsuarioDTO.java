package com.giflo.model.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import com.giflo.model.entity.Empresa;
import com.giflo.model.entity.Flor;
import com.giflo.model.entity.Perfil;
import com.giflo.model.entity.Usuario;

/**
 * The persistent class for the s_usuario database table.
 * 
 */

public class UsuarioDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String activo;
	private String contrasenia;
	private String correo;
	private String nombres;
	private String apellidos;
	private Integer intentos;
	private String isAnonymous;
	private List<Perfil> perfiles;
	private List<PublicacionDTO> publicaciones;
	private List<Flor> flores;
	private Empresa empresa;
	private String recuperacion;
	private String foto;
	private String fotoId;
	private Boolean suscrito;
	private Boolean planVigente;
	private String esAdmin;
	private Timestamp fechaLogin;
	private Timestamp fechaRegistro;
	private Integer contactosDisponibles;
	private Integer contactosRecarga;

	private Integer productosDisponibles;
	private Integer productosRecarga;
	private Boolean tieneDisponibilidad;

	public UsuarioDTO() {
	}

	public UsuarioDTO(Integer id, String activo, String contrasenia, String correo, Integer contactosDisponibles,
			Integer contactosRecarga, Integer productosDisponibles, Integer productosRecarga) {
		super();
		this.id = id;
		this.activo = activo;
		this.contrasenia = contrasenia;
		this.correo = correo;
		this.contactosDisponibles = contactosDisponibles;
		this.contactosRecarga = contactosRecarga;
		this.productosDisponibles = productosDisponibles;
		this.productosRecarga = productosRecarga;
	}

	public UsuarioDTO(Usuario user, Empresa empresa, List<Flor> flores, List<PublicacionDTO> publicaciones,
			Integer contactosDisponibles, Integer contactosRecarga, Integer productosDisponibles,
			Integer productosRecarga) {
		super();
		this.id = user.getId();
		this.correo = user.getCorreo();
		this.nombres = user.getNombres();
		this.apellidos = user.getApellidos();
		this.empresa = empresa;
		this.foto = user.getFoto();
		this.fotoId = user.getFotoId();
		this.flores = flores;
		this.publicaciones = publicaciones;
		this.fechaLogin = user.getFechaLogin();
		this.fechaRegistro = user.getFechaRegistro();
		this.contactosDisponibles = contactosDisponibles;
		this.contactosRecarga = contactosRecarga;
		this.productosDisponibles = productosDisponibles;
		this.productosRecarga = productosRecarga;

	}

	public Boolean getPlanVigente() {
		return planVigente;
	}

	public void setPlanVigente(Boolean planVigente) {
		this.planVigente = planVigente;
	}

	public Boolean getSuscrito() {
		return suscrito;
	}

	public void setSuscrito(Boolean suscrito) {
		this.suscrito = suscrito;
	}

	public String getIsAnonymous() {
		return isAnonymous;
	}

	public void setIsAnonymous(String isAnonymous) {
		this.isAnonymous = isAnonymous;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public List<Perfil> getPerfiles() {
		return perfiles;
	}

	public void setPerfiles(List<Perfil> perfiles) {
		this.perfiles = perfiles;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public Integer getIntentos() {
		return intentos;
	}

	public void setIntentos(Integer intentos) {
		this.intentos = intentos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getRecuperacion() {
		return recuperacion != null ? recuperacion : "NO";
	}

	public void setRecuperacion(String recuperacion) {
		this.recuperacion = recuperacion;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getFotoId() {
		return fotoId;
	}

	public void setFotoId(String fotoId) {
		this.fotoId = fotoId;
	}

	public List<PublicacionDTO> getPublicaciones() {
		return publicaciones;
	}

	public void setPublicaciones(List<PublicacionDTO> publicaciones) {
		this.publicaciones = publicaciones;
	}

	public List<Flor> getFlores() {
		return flores;
	}

	public void setFlores(List<Flor> flores) {
		this.flores = flores;
	}

	public String getEsAdmin() {
		return esAdmin;
	}

	public void setEsAdmin(String esAdmin) {
		this.esAdmin = esAdmin;
	}

	public Timestamp getFechaLogin() {
		return fechaLogin;
	}

	public void setFechaLogin(Timestamp fechaLogin) {
		this.fechaLogin = fechaLogin;
	}

	public Timestamp getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Timestamp fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Integer getContactosDisponibles() {
		return contactosDisponibles;
	}

	public void setContactosDisponibles(Integer contactosDisponibles) {
		this.contactosDisponibles = contactosDisponibles;
	}

	public Boolean getTieneDisponibilidad() {
		return tieneDisponibilidad;
	}

	public void setTieneDisponibilidad(Boolean tieneDisponibilidad) {
		this.tieneDisponibilidad = tieneDisponibilidad;
	}

	public Integer getContactosRecarga() {
		return contactosRecarga;
	}

	public void setContactosRecarga(Integer contactosRecarga) {
		this.contactosRecarga = contactosRecarga;
	}

	public Integer getProductosDisponibles() {
		return productosDisponibles;
	}

	public void setProductosDisponibles(Integer productosDisponibles) {
		this.productosDisponibles = productosDisponibles;
	}

	public Integer getProductosRecarga() {
		return productosRecarga;
	}

	public void setProductosRecarga(Integer productosRecarga) {
		this.productosRecarga = productosRecarga;
	}

}