package com.giflo.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.giflo.model.entity.Direccion;
import com.giflo.model.entity.EmpresaFoto;
import com.giflo.model.entity.Publicacion;
import com.giflo.model.enums.ValorSiNoEnum;

/**
 * The persistent class for the f_publicacion database table.
 * 
 */
public class PublicacionDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String descripcion;
	private String estado;
	private Timestamp fechaPublicacion;
	private String nombreVariedad;
	private String fotoVariedad;
	private Integer idEmpresa;
	private String nombreEmpresa;
	private String tipoEmpresa;
	private List<DetallePublicacionDTO> detalles;
	private Integer idTipoPublicacion;
	private String tipoPublicacion;
	private Integer idFlor;
	private String tieneTallo;
	private String tieneBoton;
	private String fotoEmpresa;
	private BigDecimal calificacion;
	private Integer calificacionCantidad;
	private Date inicioVigencia;
	private Date finVigencia;
	private String fotoPequenia;
	private String unidad;
	private String pais;

	public PublicacionDTO() {
	}

	public PublicacionDTO(Publicacion p) {

		this.setDescripcion(p.getDescripcion());
		this.setId(p.getId());
		this.setTieneBoton(p.getTieneBoton());
		this.setUnidad(p.getUnidad());
		this.setTieneTallo(p.getTieneTallo());
		this.setIdFlor(p.getFlor().getId());
		this.setInicioVigencia(p.getInicioVigencia());
		this.setFinVigencia(p.getFinVigencia());
		this.setIdTipoPublicacion(p.getTipoPublicacion().getId());
		this.setTipoPublicacion(p.getTipoPublicacion().getNombre());
		this.setIdEmpresa(p.getFlor().getEmpresa().getId());
		this.setTipoEmpresa(p.getFlor().getEmpresa().getTipo().getNombre());
		this.setCalificacion(p.getFlor().getEmpresa().getCalificacion());
		this.setCalificacionCantidad(p.getFlor().getEmpresa().getContCalificacion());
		this.setEstado(p.getEstado());
		this.setFechaPublicacion(p.getFechaPublicacion());
		this.setNombreVariedad(p.getFlor().getVariedad().getNombre());
		this.setNombreEmpresa(p.getFlor().getEmpresa().getNombre());
		this.setPais(p.getFlor().getEmpresa().getPais());
		this.setFotoVariedad(p.getFlor().getVariedad().getNombreFoto());
		this.setFotoPequenia(p.getFlor().getVariedad().getFotoPequenia());
		this.setDetalles(p.getDetalles().stream().map(d -> new DetallePublicacionDTO(d)).collect(Collectors.toList()));
		for (EmpresaFoto f : p.getFlor().getEmpresa().getFotos()) {
			if (ValorSiNoEnum.SI.equals(f.getPrincipal())) {
				this.fotoEmpresa = f.getNombre();
			}
		}
		if(p.getFlor().getEmpresa().getDirecciones()!=null) {
			Direccion dir = p.getFlor().getEmpresa().getDirecciones().parallelStream()
					.filter(d -> ValorSiNoEnum.SI.equals(d.getActivo())).findFirst().orElse(null);
			if (dir != null && dir.getPais() != null) {
				setPais(dir.getPais().getNombre());
			}	
		}
		
	}

	public BigDecimal getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(BigDecimal calificacion) {
		this.calificacion = calificacion;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Timestamp getFechaPublicacion() {
		return this.fechaPublicacion;
	}

	public void setFechaPublicacion(Timestamp fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	public List<DetallePublicacionDTO> getDetalles() {
		return this.detalles;
	}

	public void setDetalles(List<DetallePublicacionDTO> detalles) {
		this.detalles = detalles;
	}

	public DetallePublicacionDTO addDetalle(DetallePublicacionDTO detalle) {
		getDetalles().add(detalle);
		return detalle;
	}

	public DetallePublicacionDTO removeDetalle(DetallePublicacionDTO detalle) {
		getDetalles().remove(detalle);
		return detalle;
	}

	public String getNombreVariedad() {
		return nombreVariedad;
	}

	public void setNombreVariedad(String nombreVariedad) {
		this.nombreVariedad = nombreVariedad;
	}

	public String getFotoVariedad() {
		return fotoVariedad;
	}

	public void setFotoVariedad(String fotoVariedad) {
		this.fotoVariedad = fotoVariedad;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Integer getIdTipoPublicacion() {
		return idTipoPublicacion;
	}

	public void setIdTipoPublicacion(Integer idTipoPublicacion) {
		this.idTipoPublicacion = idTipoPublicacion;
	}

	public Integer getIdFlor() {
		return idFlor;
	}

	public void setIdFlor(Integer idFlor) {
		this.idFlor = idFlor;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public String getTieneTallo() {
		return tieneTallo;
	}

	public void setTieneTallo(String tieneTallo) {
		this.tieneTallo = tieneTallo;
	}

	public String getTieneBoton() {
		return tieneBoton;
	}

	public void setTieneBoton(String tieneBoton) {
		this.tieneBoton = tieneBoton;
	}

	public String getFotoEmpresa() {
		return fotoEmpresa;
	}

	public void setFotoEmpresa(String fotoEmpresa) {
		this.fotoEmpresa = fotoEmpresa;
	}

	public String getTipoPublicacion() {
		return tipoPublicacion;
	}

	public void setTipoPublicacion(String tipoPublicacion) {
		this.tipoPublicacion = tipoPublicacion;
	}

	public Date getInicioVigencia() {
		return inicioVigencia;
	}

	public void setInicioVigencia(Date inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	public Date getFinVigencia() {
		return finVigencia;
	}

	public void setFinVigencia(Date finVigencia) {
		this.finVigencia = finVigencia;
	}

	public String getFotoPequenia() {
		return fotoPequenia;
	}

	public void setFotoPequenia(String fotoPequenia) {
		this.fotoPequenia = fotoPequenia;
	}

	public Integer getCalificacionCantidad() {
		return calificacionCantidad;
	}

	public void setCalificacionCantidad(Integer calificacionCantidad) {
		this.calificacionCantidad = calificacionCantidad;
	}

	public String getTipoEmpresa() {
		return tipoEmpresa;
	}

	public void setTipoEmpresa(String tipoEmpresa) {
		this.tipoEmpresa = tipoEmpresa;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

}