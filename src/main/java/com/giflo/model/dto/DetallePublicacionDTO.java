package com.giflo.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.giflo.model.entity.DetallePublicacion;

/**
 * The persistent class for the f_detalle_publicacion database table.
 * 
 */

public class DetallePublicacionDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private BigDecimal cantidad;
	private String estado;
	private BigDecimal precio;
	private String largoTallo;
	private String tamanioBoton;
	private Integer idLargoTallo;
	private Integer idTamanioBoton;

	public DetallePublicacionDTO() {
		// TODO Auto-generated constructor stub
	}
	public DetallePublicacionDTO(DetallePublicacion d) {
		this.setId(d.getId());
		this.setCantidad(d.getCantidad());
		this.setEstado(d.getEstado());
		this.setPrecio(d.getPrecio());
		this.setLargoTallo(d.getLargoTallo() != null ? d.getLargoTallo().getNombre() : "");
		this.setTamanioBoton(d.getTamanioBoton() != null ? d.getTamanioBoton().getNombre() : "");
		this.setIdLargoTallo(d.getLargoTallo() != null ? d.getLargoTallo().getId() : 0);
		this.setIdTamanioBoton(d.getTamanioBoton() != null ? d.getTamanioBoton().getId() : 0);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public String getLargoTallo() {
		return largoTallo;
	}

	public void setLargoTallo(String largoTallo) {
		this.largoTallo = largoTallo;
	}

	public String getTamanioBoton() {
		return tamanioBoton;
	}

	public void setTamanioBoton(String tamanioBoton) {
		this.tamanioBoton = tamanioBoton;
	}

	public Integer getIdLargoTallo() {
		return idLargoTallo;
	}

	public void setIdLargoTallo(Integer idLargoTallo) {
		this.idLargoTallo = idLargoTallo;
	}

	public Integer getIdTamanioBoton() {
		return idTamanioBoton;
	}

	public void setIdTamanioBoton(Integer idTamanioBoton) {
		this.idTamanioBoton = idTamanioBoton;
	}

}