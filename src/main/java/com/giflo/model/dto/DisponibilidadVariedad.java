package com.giflo.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class DisponibilidadVariedad implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String variedad;
	private Integer idVariedad;
	private BigDecimal precio;
	private Long cantidad;
	private List<DisponibilidadLongitud> longitudes;

	public DisponibilidadVariedad() {
	}

	public DisponibilidadVariedad(String variedad, Integer idVariedad, BigDecimal precio, Long cantidad,
			List<DisponibilidadLongitud> longitudes) {
		super();
		this.variedad = variedad;
		this.idVariedad = idVariedad;
		this.precio = precio;
		this.cantidad = cantidad;
		this.longitudes = longitudes;
	}

	public List<DisponibilidadLongitud> getLongitudes() {
		return longitudes;
	}

	public void setLongitudes(List<DisponibilidadLongitud> longitudes) {
		this.longitudes = longitudes;
	}

	public String getVariedad() {
		return variedad;
	}

	public void setVariedad(String variedad) {
		this.variedad = variedad;
	}

	public Integer getIdVariedad() {
		return idVariedad;
	}

	public void setIdVariedad(Integer idVariedad) {
		this.idVariedad = idVariedad;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}
	

}
