package com.giflo.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.giflo.model.entity.Resenia;

/**
 * 
 * 
 */

public class ReseniaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String comentario;
	private BigDecimal rate;
	private Date fechaRegistro;
	
	private BigDecimal calificacion;
	private Integer calificacionContador;
	
	private String foto;
	private String nombreUsuario;
	
	private Integer idEmpresa;
	private Integer idUsuario;
	
	public ReseniaDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public ReseniaDTO(Resenia r) {
		super();
		this.setId(r.getId());
		this.setComentario(r.getComentario());
		this.setRate(r.getRate());
		this.setFechaRegistro(r.getFechaRegistro());
		this.setFoto(r.getUsuario().getFoto());
		this.setNombreUsuario(r.getUsuario().getNombres()+" "+r.getUsuario().getApellidos());
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public BigDecimal getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(BigDecimal calificacion) {
		this.calificacion = calificacion;
	}

	public Integer getCalificacionContador() {
		return calificacionContador;
	}

	public void setCalificacionContador(Integer calificacionContador) {
		this.calificacionContador = calificacionContador;
	}
		
}