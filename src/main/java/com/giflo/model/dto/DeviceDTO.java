package com.giflo.model.dto;

import java.io.Serializable;

/**
 * 
 */

public class DeviceDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String os;
	private String versionSistema;
	private String modelo;
	private String marca;
	private String sdk;
	private String firebaseToken;
	private String versionApp;

	public DeviceDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getVersionSistema() {
		return versionSistema;
	}

	public void setVersionSistema(String versionSistema) {
		this.versionSistema = versionSistema;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getSdk() {
		return sdk;
	}

	public void setSdk(String sdk) {
		this.sdk = sdk;
	}

	public String getFirebaseToken() {
		return firebaseToken!=null?firebaseToken:"";
	}

	public void setFirebaseToken(String firebaseToken) {
		this.firebaseToken = firebaseToken;
	}

	public String getVersionApp() {
		return versionApp;
	}

	public void setVersionApp(String versionApp) {
		this.versionApp = versionApp;
	}
	

}