package com.giflo.model.dto;

import java.io.Serializable;

/**
 * 
 */

public class MessageDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String title;
	private String urlImage;
	private String urlImagePub;
	private String body;
	private String url;
	private String labelBoton;
	private String tipoNotificacion;

	public MessageDTO() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrlImage() {
		return urlImage!=null?urlImage:"";
	}

	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}

	public String getBody() {
		return body!=null?body:"";
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getUrlImagePub() {
		return urlImagePub != null ? urlImagePub : "";
	}

	public void setUrlImagePub(String urlImagePub) {
		this.urlImagePub = urlImagePub;
	}

	public String getUrl() {
		return url != null ? url : "";
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLabelBoton() {
		return labelBoton != null ? labelBoton : "";
	}

	public void setLabelBoton(String labelBoton) {
		this.labelBoton = labelBoton;
	}

	public String getTipoNotificacion() {
		return tipoNotificacion;
	}

	public void setTipoNotificacion(String tipoNotificacion) {
		this.tipoNotificacion = tipoNotificacion;
	}
	

}