package com.giflo.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import com.giflo.model.entity.Cupon;
import com.giflo.model.entity.PagoPlan;
import com.giflo.model.enums.ValorEstadoPagoPlanEnum;

/**
 * 
 */

public class PagoPlanDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer idPlan;
	private Integer idUsuario;
	private Timestamp fechaRegistro;
	private Timestamp fechaPago;
	private String codigoPago;
	private String documento;
	private String cupon;
	private BigDecimal descuento;
	private BigDecimal subtotal;
	private BigDecimal iva;
	private BigDecimal total;
	private ValorEstadoPagoPlanEnum estado;

	private String plan;
	private BigDecimal valorPlan;
	private Integer cantidadContactos;
	private BigDecimal porcentajeCupon;
	private String mensaje;
	private Integer idCupon;
	private String referenciaPlaceToPay;

	public PagoPlanDTO() {
		// TODO Auto-generated constructor stub
	}

	public PagoPlanDTO(PagoPlan s) {
		this.id = s.getId();
		this.idPlan = s.getPlan().getId();
		this.idUsuario = s.getUsuario().getId();
		this.fechaRegistro = s.getFechaRegistro();
		this.fechaPago = s.getFechaPago();
		this.codigoPago = s.getCodigoPago();
		this.documento = s.getDocumento();
		this.cupon = s.getCupon();
		this.descuento = s.getDescuento();
		this.subtotal = s.getSubtotal();
		this.iva = s.getIva();
		this.total = s.getTotal();
		this.estado = s.getEstado();
		this.plan = s.getPlan().getNombre();
		this.valorPlan = s.getPlan().getValor();
		this.cantidadContactos = s.getPlan().getCantidadContactos();
		this.referenciaPlaceToPay = s.getReferenciaPlaceToPay();
	}

	public PagoPlanDTO(PagoPlan s, Cupon c) {
		this.id = s.getId();
		this.idPlan = s.getPlan().getId();
		this.idUsuario = s.getUsuario().getId();
		this.fechaRegistro = s.getFechaRegistro();
		this.fechaPago = s.getFechaPago();
		this.codigoPago = s.getCodigoPago();
		this.documento = s.getDocumento();
		this.cupon = s.getCupon();
		this.descuento = s.getDescuento();
		this.subtotal = s.getSubtotal();
		this.iva = s.getIva();
		this.total = s.getTotal();
		this.estado = s.getEstado();
		this.plan = s.getPlan().getNombre();
		this.valorPlan = s.getPlan().getValor();
		this.cantidadContactos = s.getPlan().getCantidadContactos();
		this.porcentajeCupon = c.getPorciento();
		this.referenciaPlaceToPay = s.getReferenciaPlaceToPay();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdPlan() {
		return idPlan;
	}

	public void setIdPlan(Integer idPlan) {
		this.idPlan = idPlan;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Timestamp getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Timestamp fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Timestamp getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Timestamp fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String getCodigoPago() {
		return codigoPago;
	}

	public void setCodigoPago(String codigoPago) {
		this.codigoPago = codigoPago;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getCupon() {
		return cupon;
	}

	public void setCupon(String cupon) {
		this.cupon = cupon;
	}

	public BigDecimal getDescuento() {
		return descuento;
	}

	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public ValorEstadoPagoPlanEnum getEstado() {
		return estado;
	}

	public void setEstado(ValorEstadoPagoPlanEnum estado) {
		this.estado = estado;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public BigDecimal getValorPlan() {
		return valorPlan;
	}

	public void setValorPlan(BigDecimal valorPlan) {
		this.valorPlan = valorPlan;
	}

	public Integer getCantidadContactos() {
		return cantidadContactos;
	}

	public void setCantidadContactos(Integer cantidadContactos) {
		this.cantidadContactos = cantidadContactos;
	}

	public BigDecimal getPorcentajeCupon() {
		return porcentajeCupon;
	}

	public void setPorcentajeCupon(BigDecimal porcentajeCupon) {
		this.porcentajeCupon = porcentajeCupon;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Integer getIdCupon() {
		return idCupon;
	}

	public void setIdCupon(Integer idCupon) {
		this.idCupon = idCupon;
	}

	public String getReferenciaPlaceToPay() {
		return referenciaPlaceToPay;
	}

	public void setReferenciaPlaceToPay(String referenciaPlaceToPay) {
		this.referenciaPlaceToPay = referenciaPlaceToPay;
	}

}