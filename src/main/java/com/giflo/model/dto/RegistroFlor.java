package com.giflo.model.dto;

import java.io.Serializable;

public class RegistroFlor implements Serializable {

	/**
		 * 
		 */
	private static final long serialVersionUID = 444123076078482774L;
	private Integer idEmpresa;
	private Integer idVariedad;

	public RegistroFlor() {
		// TODO Auto-generated constructor stub
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Integer getIdVariedad() {
		return idVariedad;
	}

	public void setIdVariedad(Integer idVariedad) {
		this.idVariedad = idVariedad;
	}

}
