package com.giflo.model.dto;

import java.io.Serializable;

public class RespuestaOtpDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -388471974655477305L;
	private Integer validez;
	private String fechaValidez;
	private String mensaje;
	private Boolean valido;

	public RespuestaOtpDTO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getValidez() {
		return validez;
	}

	public void setValidez(Integer validez) {
		this.validez = validez;
	}

	public String getFechaValidez() {
		return fechaValidez;
	}

	public void setFechaValidez(String fechaValidez) {
		this.fechaValidez = fechaValidez;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Boolean getValido() {
		return valido;
	}

	public void setValido(Boolean valido) {
		this.valido = valido;
	}

}
