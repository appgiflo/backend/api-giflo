package com.giflo.model.dto;

import java.io.Serializable;

public class MenuDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Integer idTipoPublicacion;
	String nombre;

	public MenuDTO() {
		// TODO Auto-generated constructor stub
	}

	public MenuDTO(String nombre) {
		super();
		this.nombre = nombre;
	}

	public Integer getIdTipoPublicacion() {
		return idTipoPublicacion;
	}

	public void setIdTipoPublicacion(Integer idTipoPublicacion) {
		this.idTipoPublicacion = idTipoPublicacion;
	}
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
