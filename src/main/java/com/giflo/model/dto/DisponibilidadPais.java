package com.giflo.model.dto;

import java.io.Serializable;
import java.util.List;

public class DisponibilidadPais implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3255928400662563604L;

	private String pais;
	private Integer idPais;
	private List<DisponibilidadVariedad> variedades;

	public DisponibilidadPais() {
		// TODO Auto-generated constructor stub
	}

	public DisponibilidadPais(String pais, Integer idPais, List<DisponibilidadVariedad> variedades) {
		super();
		this.pais = pais;
		this.idPais = idPais;
		this.variedades = variedades;
	}

	public List<DisponibilidadVariedad> getVariedades() {
		return variedades;
	}

	public void setVariedades(List<DisponibilidadVariedad> variedades) {
		this.variedades = variedades;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public Integer getIdPais() {
		return idPais;
	}

	public void setIdPais(Integer idPais) {
		this.idPais = idPais;
	}

}
