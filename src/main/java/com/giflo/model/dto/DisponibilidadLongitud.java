package com.giflo.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class DisponibilidadLongitud implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3255928400662563604L;

	
	private String valorLongitud;
	private Integer idLongitud;
	private BigDecimal precio;
	private BigDecimal cantidad;
	
	public DisponibilidadLongitud() {
		// TODO Auto-generated constructor stub
	}
	

	public DisponibilidadLongitud(String valorLongitud, Integer idLongitud, BigDecimal precio, BigDecimal cantidad) {
		super();
		this.valorLongitud = valorLongitud;
		this.idLongitud = idLongitud;
		this.precio = precio;
		this.cantidad = cantidad;
	}


	public String getValorLongitud() {
		return valorLongitud;
	}

	public void setValorLongitud(String valorLongitud) {
		this.valorLongitud = valorLongitud;
	}

	public Integer getIdLongitud() {
		return idLongitud;
	}

	public void setIdLongitud(Integer idLongitud) {
		this.idLongitud = idLongitud;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public BigDecimal getCantidad() {
		return cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
	
	
}
