package com.giflo.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 */

public class CuponDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer idCupon;
	private String cupon;
	private String mensaje;
	private BigDecimal valorPlan;
	private BigDecimal descuento;
	private BigDecimal subtotal;
	private BigDecimal iva;
	private BigDecimal total;
	private BigDecimal porcentaje;

	public CuponDTO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getIdCupon() {
		return idCupon;
	}

	public void setIdCupon(Integer idCupon) {
		this.idCupon = idCupon;
	}

	public String getCupon() {
		return cupon;
	}

	public void setCupon(String cupon) {
		this.cupon = cupon;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public BigDecimal getValorPlan() {
		return valorPlan;
	}

	public void setValorPlan(BigDecimal valorPlan) {
		this.valorPlan = valorPlan;
	}

	public BigDecimal getDescuento() {
		return descuento;
	}

	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(BigDecimal porcentaje) {
		this.porcentaje = porcentaje;
	}

}