package com.giflo.view.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.function.Consumer;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.giflo.model.dto.MessageDTO;
import com.giflo.model.entity.Catalogo;
import com.giflo.model.entity.DeviceToken;
import com.giflo.model.entity.Empresa;
import com.giflo.model.entity.Flor;
import com.giflo.model.entity.Parametro;
import com.giflo.model.entity.Publicacion;
import com.giflo.model.entity.TokenFcm;
import com.giflo.model.entity.Usuario;
import com.giflo.model.entity.Variedad;
import com.giflo.model.repository.DeviceTokenRepository;
import com.giflo.model.repository.ParametroRepository;
import com.giflo.model.repository.UsuarioRepository;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.SendResponse;

@Service
public class FirebaseMessageServiceImpl implements IFirebaseMessageService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private DeviceTokenRepository deviceTokenRepository;

	@Autowired
	private ParametroRepository parametroRepository;

	private Logger log = LoggerFactory.getLogger(FirebaseMessageServiceImpl.class);

	
	private Integer cantidadMaxMensajes = 500;

	public FirebaseMessageServiceImpl() {
	}

	@PostConstruct
	void init() {
		try {
			
			Parametro paramCantidad = parametroRepository.buscarPorId("FIREBASE_CANTIDAD_MENSAJES");
			if (paramCantidad != null)
				cantidadMaxMensajes = Integer.valueOf(paramCantidad.getValor());
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	@Override
	public void sendPushPublicacion(Publicacion p, List<Usuario> usuarios) {
		try {
			if (usuarios.size() > 0) {
				Flor flor = p.getFlor();
				Variedad variedad = flor.getVariedad();
				Empresa empresa = flor.getEmpresa();
				Catalogo tp = p.getTipoPublicacion();
				HashMap<Integer, List<Message>> mapToken = new HashMap<>();
				Integer indice = 0;
				Integer contador = 0;
				List<Message> lista = new ArrayList<>(0);
				for (Usuario u : usuarios) {
					for (TokenFcm fcm : u.getTokenFcms()) {
						if (contador < cantidadMaxMensajes) {

							HashMap<String, String> map = new HashMap<>();
							map.put("idPublicacion", p.getId().toString());
							map.put("tipoPublicacion", p.getTipoPublicacion().getCodigo());
							map.put("tipoPush", "PUB");
							Message m = Message.builder().putAllData(map).setToken(fcm.getToken())
									.setNotification(Notification.builder()
											.setTitle(String.format("!Hola %1s, %2s creo una nueva publicación¡",
													u.getNombres(), empresa.getNombre()))
											.setBody(String.format("Variedad %1s, %2s ",
													variedad.getNombre(), tp.getNombre()))
											.setImage(variedad.getFotoPequenia()).build())
									.build();
							lista.add(m);
							contador++;
						} else {
							mapToken.put(indice, lista);
							contador = 0;
							lista = new ArrayList<>(0);
							indice++;
						}
					}
				}
				if (contador != 0 && contador < cantidadMaxMensajes) {
					mapToken.put(indice, lista);
				}
				Set<Integer> keys = mapToken.keySet();
				for (Integer key : keys) {
					List<Message> messages = mapToken.get(key);
					BatchResponse response = FirebaseMessaging.getInstance().sendAll(messages);// sendMulticast(message);
					if (response.getFailureCount() > 0) {
						log.warn(String.format("No se envio los mensaje a %1S dispositivos, Exitos: %2s",
								response.getFailureCount(), response.getSuccessCount()));
						if (response.getResponses() != null)
							response.getResponses().stream().forEach(new Consumer<SendResponse>() {
								@Override
								public void accept(SendResponse t) {
									log.warn(String.format(" ID Mesnaje %1s, Motivo %2s , Exitoso %3s",
											t.getMessageId(),
											t.getException() != null ? t.getException().getMessage() : "Desconocido",
											t.isSuccessful()));

								}
							});

					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public MessageDTO sendPushMessage(MessageDTO dto, Integer idUsuario) throws FirebaseMessagingException {
		try {
			Iterable<Usuario> usuarios = usuarioRepository.findAll();
			Iterable<DeviceToken> deviceToken = deviceTokenRepository.findAll();
			List<Message> lista = new ArrayList<>(0);
			HashMap<Integer, List<Message>> mapToken = new HashMap<>();
			Integer indice = 0;
			Integer contador = 0;
			for (Usuario u : usuarios) {
				if (idUsuario != null && idUsuario.equals(u.getId())) {
					break;
				}
				for (TokenFcm fcm : u.getTokenFcms()) {
					if (contador < cantidadMaxMensajes) {
						HashMap<String, String> map = new HashMap<>();
						map.put("tipoPush", dto.getTipoNotificacion());
						map.put("urlImagePub", dto.getUrlImagePub());
						map.put("url", dto.getUrl());
						map.put("labelBoton", dto.getLabelBoton());
						Message m = Message.builder().putAllData(map).setToken(fcm.getToken())
								.setNotification(Notification.builder()
										.setTitle(String.format("!Hola %1s, %2s", u.getNombres(), dto.getTitle()))
										.setBody(dto.getBody()).setImage(dto.getUrlImage()).build())
								.build();
						lista.add(m);
						contador++;
					} else {
						mapToken.put(indice, lista);
						contador = 0;
						lista = new ArrayList<>(0);
						indice++;
					}
				}

			}
			for (DeviceToken device : deviceToken) {
				if (contador < cantidadMaxMensajes) {
					HashMap<String, String> map = new HashMap<>();
					map.put("tipoPush", dto.getTipoNotificacion());
					map.put("urlImagePub", dto.getUrlImagePub());
					map.put("url", dto.getUrl());
					map.put("labelBoton", dto.getLabelBoton());
					Message m = Message.builder().putAllData(map).setToken(device.getToken())
							.setNotification(Notification.builder()
									.setTitle(String.format("!Hola %1s, %2s", device.getModelo(), dto.getTitle()))
									.setBody(dto.getBody()).setImage(dto.getUrlImage()).build())
							.build();
					lista.add(m);
					contador++;
				} else {
					mapToken.put(indice, lista);
					contador = 0;
					lista = new ArrayList<>(0);
					indice++;
				}
			}
			if (contador != 0 && contador < cantidadMaxMensajes) {
				mapToken.put(indice, lista);
			}
			Set<Integer> keys = mapToken.keySet();
			for (Integer key : keys) {
				List<Message> messages = mapToken.get(key);
				BatchResponse response = FirebaseMessaging.getInstance().sendAll(messages);// sendMulticast(message);
				if (response.getFailureCount() > 0) {
					log.warn(String.format("No se envio los mensaje a %1S dispositivos, Exitos: %2s",
							response.getFailureCount(), response.getSuccessCount()));
					if (response.getResponses() != null)
						response.getResponses().stream().forEach(new Consumer<SendResponse>() {
							@Override
							public void accept(SendResponse t) {
								log.warn(String.format(" ID Mesnaje %1s, Motivo %2s , Exitoso %3s", t.getMessageId(),
										t.getException() != null ? t.getException().getMessage() : "Desconocido",
										t.isSuccessful()));

							}
						});

				}
			}
			dto.setTitle("Publicado Exitosamente");
		} catch (Exception e) {
			dto.setTitle("Ocurriío un error en la notificación");
		}
		return dto;
	}

	@Override
	public MessageDTO sendPushMessageToUser(MessageDTO dto, Usuario u) throws FirebaseMessagingException {
		List<Message> lista = new ArrayList<>(0);
		HashMap<Integer, List<Message>> mapToken = new HashMap<>();
		Integer indice = 0;
		Integer contador = 0;
		for (TokenFcm fcm : u.getTokenFcms()) {
			if (contador < cantidadMaxMensajes) {
				HashMap<String, String> map = new HashMap<>();
				map.put("tipoPush", dto.getTipoNotificacion());
				map.put("urlImagePub", dto.getUrlImagePub());
				map.put("url", dto.getUrl());
				map.put("labelBoton", dto.getLabelBoton());
				Message m = Message.builder().putAllData(map).setToken(fcm.getToken())
						.setNotification(Notification.builder().setTitle(String.format("%2s", dto.getTitle()))
								.setBody(dto.getBody()).setImage(dto.getUrlImage()).build())
						.build();
				lista.add(m);
				contador++;
			} else {
				mapToken.put(indice, lista);
				contador = 0;
				lista = new ArrayList<>(0);
				indice++;
			}
		}
		if (contador != 0 && contador < cantidadMaxMensajes) {
			mapToken.put(indice, lista);
		}
		Set<Integer> keys = mapToken.keySet();
		for (Integer key : keys) {
			List<Message> messages = mapToken.get(key);
			BatchResponse response = FirebaseMessaging.getInstance().sendAll(messages);// sendMulticast(message);
			if (response.getFailureCount() > 0) {
				log.warn(String.format("No se envio los mensaje a %1S dispositivos, Exitos: %2s",
						response.getFailureCount(), response.getSuccessCount()));
				if (response.getResponses() != null)
					response.getResponses().stream().forEach(new Consumer<SendResponse>() {
						@Override
						public void accept(SendResponse t) {
							log.warn(String.format(" ID Mesnaje %1s, Motivo %2s , Exitoso %3s", t.getMessageId(),
									t.getException() != null ? t.getException().getMessage() : "Desconocido",
									t.isSuccessful()));

						}
					});
			}
		}
		dto.setTitle("Publicado Exitosamente");
		return dto;

	}

}
