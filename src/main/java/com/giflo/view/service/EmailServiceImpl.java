package com.giflo.view.service;

import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class EmailServiceImpl implements EmailService {


	@Autowired
	private Environment env;

	@Bean
	public JavaMailSender getJavaMailSender() {
		String user = env.getProperty("config.email.user");
		String pass = env.getProperty("config.email.password");
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost("smtp.gmail.com");
		mailSender.setPort(587);

		mailSender.setUsername(user);
		mailSender.setPassword(pass);

		Properties props = mailSender.getJavaMailProperties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.debug", "true");
		props.put("mail.mime.charset", "utf8");
		mailSender.setDefaultEncoding("UTF-8");
		return mailSender;
	}

	public void sendSimpleMessage(String to, String subject, String text) {
		try {
			JavaMailSender jms=getJavaMailSender();
			MimeMessage msg = jms.createMimeMessage();
			
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);
			// helper.setFrom("giflo@gmail.com");
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setText(text, true);
			
			// hard coded a file path
			// FileSystemResource file = new FileSystemResource(new
			// File("path/android.png"));
			// helper.addAttachment("my_photo.png", new ClassPathResource("android.png"));
			jms.send(msg);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
