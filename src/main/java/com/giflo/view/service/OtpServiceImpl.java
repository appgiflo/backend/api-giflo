package com.giflo.view.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.giflo.model.dto.RespuestaOtpDTO;
import com.giflo.model.entity.Otp;
import com.giflo.model.enums.TemplateEmailEnum;
import com.giflo.model.enums.ValorEstadoOtpEnum;
import com.giflo.model.repository.OtpRepository;
import com.giflo.util.AES;
import com.giflo.util.GifloTemplateEmail;

@Service
public class OtpServiceImpl implements IOtpService {

	@Autowired
	private Environment env;
	@Autowired
	private EmailService emailService;

	@Autowired
	private OtpRepository otpRepository;

	private static final SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	@Override
	public RespuestaOtpDTO enviarOTP(String contacto) throws Exception {
		String correo = (new AES()).decrypt(contacto);
		RespuestaOtpDTO respuesta = new RespuestaOtpDTO();
		List<Otp> lista = otpRepository.buscar(correo);
		String tiempoValidesSegundos = env.getProperty("config.otp.valid");
		Otp otp;
		respuesta.setValido(true);
		if (lista.isEmpty()) {
			otp = new Otp();
			otp.setActivo(ValorEstadoOtpEnum.ENVIADO);
			otp.setContacto(correo);
			otp.setFechaRegistro(GregorianCalendar.getInstance().getTime());
		} else {
			otp = lista.get(0);
			// Validamos fecha de expiracion
			Timestamp current = new Timestamp(GregorianCalendar.getInstance().getTimeInMillis());
			Timestamp fechaExpiracion = otp.getValidez();
			if (current.before(fechaExpiracion)) {
				respuesta.setFechaValidez(sdf3.format(otp.getValidez()));
				respuesta.setValidez(Integer.valueOf(tiempoValidesSegundos));
				respuesta.setMensaje(
						String.format("Ya se te envió un código, puedes solicitar un nuevo código luego de %1s.",
								sdf3.format(fechaExpiracion)));
				respuesta.setValido(false);
				return respuesta;
			}
		}

		// Enviamos el otp

		String codigo = generateCommonLangPassword();
		Calendar c = GregorianCalendar.getInstance();
		c.add(Calendar.SECOND, Integer.valueOf(tiempoValidesSegundos));
		Timestamp timestamp = new Timestamp(c.getTimeInMillis());
		otp.setCodigo((new AES()).encrypt(codigo));
		otp.setValidez(timestamp);

		HashMap<String, String> map = new HashMap<>();
		map.put("contacto", correo);
		map.put("clave", codigo);
		map.put("tiempo", sdf3.format(timestamp));
		emailService.sendSimpleMessage(correo, "Giflo Código",
				GifloTemplateEmail.buildEmail(map, TemplateEmailEnum.ENVIO_OTP));
		otpRepository.save(otp);

		respuesta.setFechaValidez(sdf3.format(timestamp));
		respuesta.setValidez(Integer.valueOf(tiempoValidesSegundos));

		respuesta.setMensaje(
				String.format("Se envío un código al correo %1s, válida por %2s segundos. Por favor revisa tu correo.",
						AES.maskEmail(correo), tiempoValidesSegundos));
		return respuesta;
	}

	@Override
	public RespuestaOtpDTO validarOTP(String contacto, String codigo) throws Exception {
		RespuestaOtpDTO respuesta = new RespuestaOtpDTO();
		List<Otp> lista = otpRepository.buscar((new AES()).decrypt(contacto));
		Otp otp = lista.get(0);
		// Validamos el codigo OTP
		String codeBase = (new AES()).decrypt(otp.getCodigo());
		String codeClient = (new AES()).decrypt(codigo);
		// Validamos el estado
		if (otp.getActivo().equals(ValorEstadoOtpEnum.APROBADO) && codeBase.equals(codeClient)) {
			respuesta.setValido(true);
			respuesta.setMensaje("El código ya fue validado correctamente.");
			return respuesta;
		}
		// Validamos fecha de expiracion
		Timestamp current = new Timestamp(GregorianCalendar.getInstance().getTimeInMillis());
		Timestamp fechaExpiracion = otp.getValidez();
		if (current.after(fechaExpiracion)) {
			respuesta.setValido(false);
			respuesta.setMensaje(String.format("El código de verificación a expirado %1s, solicite un nuevo código.",
					sdf3.format(fechaExpiracion)));
			return respuesta;
		}
		if (codeBase.equals(codeClient)) {
			respuesta.setValido(true);
			respuesta.setMensaje("Código validado exitosamente.");
			otp.setActivo(ValorEstadoOtpEnum.APROBADO);
			otpRepository.save(otp);
		} else {
			respuesta.setValido(false);
			respuesta.setMensaje(
					"El código ingresado es incorrecto, revisa que tu código sea el mismo que se te envío a tu correo.");
		}
		return respuesta;
	}

	public String generateCommonLangPassword() {
		String numbers = RandomStringUtils.randomNumeric(4);
		List<Character> pwdChars = numbers.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
		Collections.shuffle(pwdChars);
		String password = pwdChars.stream().collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
				.toString();
		return password;
	}

}
