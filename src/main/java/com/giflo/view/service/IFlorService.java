package com.giflo.view.service;

import com.giflo.model.entity.Flor;

public interface IFlorService {

	/**
	 * Busca un usuario por tipo de identificacion y cedula
	 * 
	 * @param type
	 * @param identification
	 * @return
	 * @throws Exception
	 */
	Flor crear(final Integer idEmpresa,final Integer idVariedad) throws Exception;
	/**
	 * 
	 * @param flor
	 * @param id
	 * @return
	 * @throws Exception
	 */
	Flor actualziar(Flor flor,Integer id) throws Exception;
}
