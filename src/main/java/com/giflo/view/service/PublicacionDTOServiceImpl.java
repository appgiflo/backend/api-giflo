package com.giflo.view.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.giflo.model.dto.PublicacionDTO;
import com.giflo.model.entity.Catalogo;
import com.giflo.model.entity.DetallePublicacion;
import com.giflo.model.entity.Flor;
import com.giflo.model.entity.Publicacion;
import com.giflo.model.entity.Usuario;
import com.giflo.model.repository.CatalogoRepository;
import com.giflo.model.repository.DetallePublicacionRepository;
import com.giflo.model.repository.FlorRepository;
import com.giflo.model.repository.PublicacionRepository;

@Service
public class PublicacionDTOServiceImpl implements IPublicacionDTOService {

	@Autowired
	PublicacionRepository publicacionRepository;

	@Autowired
	DetallePublicacionRepository detallePublicacionRepository;

	@Autowired
	FlorRepository florRepository;

	@Autowired
	private CatalogoRepository catalogoRepository;

	@Autowired
	private IFirebaseMessageService firebaseMessageService;

	@Autowired
	private IUsuarioService iUsuarioService;

	@Override
	public Map<String, Object> findByFlorAndPublicacion(Integer idTipoPublicacion, Integer idColor, String search,
			Integer page, Integer size) {
		Pageable paging = PageRequest.of(page, size);
		search = search != null ? search.trim() : "";
		Page<Publicacion> padeRet = publicacionRepository.findByColorAndPublicacion(idTipoPublicacion, idColor,
				search != null ? search.trim() : "", paging);
		List<PublicacionDTO> lista = padeRet.getContent().stream().map(p -> new PublicacionDTO(p))
				.collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		response.put("data", lista);
		response.put("page", padeRet.getNumber());
		response.put("totalPages", padeRet.getTotalPages());
		response.put("count", padeRet.getTotalElements());
		return response;
	}

	@Override
	public Map<String, Object> findByFlorAndPublicacionAndTallo(Integer idTipoPublicacion, Integer idColor,
			String search, Integer page, Integer size, Integer idTallo) {
		Pageable paging = PageRequest.of(page, size);
		search = search != null ? search.trim() : "";
		Page<Publicacion> padeRet = publicacionRepository.findByColorAndPublicacionAndTallo(idTipoPublicacion, idColor,
				search != null ? search.trim() : "", paging, idTallo);
		List<PublicacionDTO> lista = padeRet.getContent().stream().map(p -> new PublicacionDTO(p))
				.collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		response.put("data", lista);
		response.put("page", padeRet.getNumber());
		response.put("totalPages", padeRet.getTotalPages());
		response.put("count", padeRet.getTotalElements());
		return response;
	}

	@Override
	public Map<String, Object> findByFilter(String search, Integer page, Integer size) {
		Pageable paging = PageRequest.of(page, size);
		search = search != null ? search.trim() : "";
		Page<Publicacion> padeRet = publicacionRepository.findByVariedadAndEmpresa(search,
				Calendar.getInstance().getTime(), paging);
		List<PublicacionDTO> lista = padeRet.getContent().stream().map(p -> new PublicacionDTO(p))
				.collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		response.put("data", lista);
		response.put("page", padeRet.getNumber());
		response.put("totalPages", padeRet.getTotalPages());
		response.put("count", padeRet.getTotalElements());
		return response;
	}

	@Override
	public List<PublicacionDTO> findAllByUser(Integer idUsuario) {
		return publicacionRepository.findByUsuario(idUsuario).stream().map(p -> new PublicacionDTO(p))
				.collect(Collectors.toList());
	}

	@Override
	public List<PublicacionDTO> findByUsuarioVigente(Integer idUsuario) {
		return publicacionRepository.findByUsuarioVigente(idUsuario, Calendar.getInstance().getTime()).stream()
				.map(p -> new PublicacionDTO(p)).collect(Collectors.toList());
	}

	@Override
	public List<PublicacionDTO> findByEmpresaVigente(Integer idEmpresa, Integer idPublicacion) {
		return publicacionRepository.findByEmpresaVigente(idEmpresa, idPublicacion, Calendar.getInstance().getTime())
				.stream().map(p -> new PublicacionDTO(p)).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public List<PublicacionDTO> crear(List<Publicacion> ps) throws Exception {
		return ps.parallelStream().map(p -> {
			try {
				if (p.getId() == -1) {
					Flor flor = florRepository.buscarPorId(p.getIdFlor());
					Catalogo tipoPub = catalogoRepository.findById(p.getIdTipoPublicacion()).orElse(null);
					Publicacion nuevo = new Publicacion();
					nuevo.setDescripcion(p.getDescripcion());
					nuevo.setFlor(flor);
					nuevo.setEstado("ACTIVO");
					nuevo.setUnidad(p.getUnidad());
					nuevo.setTieneTallo(p.getTieneTallo());
					nuevo.setFechaPublicacion(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
					nuevo.setTipoPublicacion(tipoPub);
					nuevo.setInicioVigencia(p.getInicioVigencia());
					nuevo.setFinVigencia(p.getFinVigencia());
					publicacionRepository.save(nuevo);

					List<DetallePublicacion> detalles = new ArrayList<>(0);
					for (DetallePublicacion dt : p.getDetalles()) {
						DetallePublicacion nuevoDet = new DetallePublicacion();
						nuevoDet.setCantidad(dt.getCantidad());
						nuevoDet.setPrecio(dt.getPrecio());
						nuevoDet.setEstado("ACTIVO");
						nuevoDet.setLargoTallo("SI".equalsIgnoreCase(p.getTieneTallo())
								? catalogoRepository.findById(dt.getIdLargoTallo()).orElse(null)
								: null);
						nuevoDet.setTamanioBoton("SI".equalsIgnoreCase(p.getTieneBoton())
								? catalogoRepository.findById(dt.getIdTamanioBoton()).orElse(null)
								: null);
						nuevoDet.setPublicacion(nuevo);
						detallePublicacionRepository.save(nuevoDet);
						detalles.add(nuevoDet);
					}
					nuevo.setDetalles(detalles);
					List<Usuario> usuarioToken = iUsuarioService.findTokens(flor.getEmpresa().getIdUsuario());
					firebaseMessageService.sendPushPublicacion(nuevo, usuarioToken);
					return new PublicacionDTO(nuevo);
				} else {
					Publicacion nuevo = publicacionRepository.findById(p.getId()).orElse(null);
					Catalogo tipoPub = catalogoRepository.findById(p.getIdTipoPublicacion()).orElse(null);
					if (nuevo != null) {
						Flor flor = florRepository.findById(p.getIdFlor()).orElse(null);
						nuevo.setDescripcion(p.getDescripcion());
						nuevo.setFlor(flor);
						nuevo.setEstado("ACTIVO");
						nuevo.setTipoPublicacion(tipoPub);
						nuevo.setUnidad(p.getUnidad());
						nuevo.setFechaPublicacion(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
						nuevo.setInicioVigencia(p.getInicioVigencia());
						nuevo.setFinVigencia(p.getFinVigencia());
						publicacionRepository.save(nuevo);
						detallePublicacionRepository.deleteAll(nuevo.getDetalles());
						for (DetallePublicacion dt : p.getDetalles()) {
							DetallePublicacion nuevoDet = new DetallePublicacion();
							nuevoDet.setCantidad(dt.getCantidad());
							nuevoDet.setPrecio(dt.getPrecio());
							nuevoDet.setEstado("ACTIVO");
							nuevoDet.setLargoTallo("SI".equalsIgnoreCase(p.getTieneTallo())
									? catalogoRepository.findById(dt.getIdLargoTallo()).orElse(null)
									: null);
							nuevoDet.setTamanioBoton("SI".equalsIgnoreCase(p.getTieneBoton())
									? catalogoRepository.findById(dt.getIdTamanioBoton()).orElse(null)
									: null);
							nuevoDet.setPublicacion(nuevo);
							detallePublicacionRepository.save(nuevoDet);
						}
						nuevo = publicacionRepository.findById(p.getId()).orElse(null);
						firebaseMessageService.sendPushPublicacion(nuevo,
								iUsuarioService.findTokens(flor.getEmpresa().getUsuarios().get(0).getId()));
						return new PublicacionDTO(nuevo);
					}
					return null;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}).collect(Collectors.toList());

	}

	@Override
	public PublicacionDTO enviarNotificacionPush(Integer idPublicacion) throws Exception {
		Publicacion registrado = publicacionRepository.findById(idPublicacion).orElse(null);
		if (registrado != null) {
			firebaseMessageService.sendPushPublicacion(registrado,
					iUsuarioService.findTokens(registrado.getFlor().getEmpresa().getUsuarios().get(0).getId()));
			return new PublicacionDTO(registrado);
		}
		return new PublicacionDTO();

	}

	@Override
	public Boolean activarInactivar(Integer id, PublicacionDTO p) {
		Publicacion upd = publicacionRepository.findById(id).orElse(null);
		if (upd != null) {
			upd.setEstado(p.getEstado());
			publicacionRepository.save(upd);
			return true;
		}
		return false;

	}

	@Override
	public Boolean eliminarPublicacion(Integer id) {
		Publicacion p = publicacionRepository.findById(id).orElse(null);
		p.getDetalles().stream().forEach(d -> detallePublicacionRepository.delete(d));
		publicacionRepository.delete(p);
		return true;
	}

}
