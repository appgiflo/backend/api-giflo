package com.giflo.view.service;

public interface IContactoService {
	
	/**
	 * Crear regsitro notificacion
	 * @param id origen
	 * @param id destino
	 * @return
	 * @throws Exception 
	 */
	Boolean crear(Integer origen, Integer destino) throws Exception;

	/**
	 * Eliminar registro notificacion
	 * @param id origen
	 * @param id destino
	 * @return
	 * @throws Exception
	 */
	
	Boolean eliminar(Integer origen, Integer destino) throws Exception;


}
