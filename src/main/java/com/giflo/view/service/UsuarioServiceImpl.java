package com.giflo.view.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.giflo.model.dto.DeviceDTO;
import com.giflo.model.dto.PublicacionDTO;
import com.giflo.model.dto.RegistroDTO;
import com.giflo.model.dto.UsuarioDTO;
import com.giflo.model.entity.DeviceToken;
import com.giflo.model.entity.Direccion;
import com.giflo.model.entity.Empresa;
import com.giflo.model.entity.EmpresaFoto;
import com.giflo.model.entity.Flor;
import com.giflo.model.entity.PagoPlan;
import com.giflo.model.entity.Parametro;
import com.giflo.model.entity.Perfil;
import com.giflo.model.entity.Plan;
import com.giflo.model.entity.Producto;
import com.giflo.model.entity.Saldo;
import com.giflo.model.entity.TokenFcm;
import com.giflo.model.entity.Usuario;
import com.giflo.model.entity.UsuarioPerfil;
import com.giflo.model.entity.Variedad;
import com.giflo.model.enums.GrupoSaldoEnum;
import com.giflo.model.enums.TemplateEmailEnum;
import com.giflo.model.enums.ValorEstadoEnum;
import com.giflo.model.enums.ValorSiNoEnum;
import com.giflo.model.repository.CatalogoRepository;
import com.giflo.model.repository.DeviceTokenRepository;
import com.giflo.model.repository.DireccionRepository;
import com.giflo.model.repository.EmpresaFotoRepository;
import com.giflo.model.repository.EmpresaRepository;
import com.giflo.model.repository.FlorRepository;
import com.giflo.model.repository.NotificacionPushRepository;
import com.giflo.model.repository.PagoPlanRepository;
import com.giflo.model.repository.ParametroRepository;
import com.giflo.model.repository.PerfilRepository;
import com.giflo.model.repository.PlanRepository;
import com.giflo.model.repository.ProductoRepository;
import com.giflo.model.repository.PublicacionRepository;
import com.giflo.model.repository.TokenFcmRepository;
import com.giflo.model.repository.UsuarioPerfilRepository;
import com.giflo.model.repository.UsuarioRepository;
import com.giflo.util.AES;
import com.giflo.util.GifloTemplateEmail;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired
	private NotificacionPushRepository notificacionPushRepository;
	@Autowired
	private PlanRepository planRepository;
	@Autowired
	private PerfilRepository perfilRepository;
	@Autowired
	private EmpresaRepository empresaRepository;
	@Autowired
	private EmpresaFotoRepository empresaFotoRepository;
	@Autowired
	private DireccionRepository direccionRepository;
	@Autowired
	private UsuarioPerfilRepository usuarioPerfilRepository;
	@Autowired
	private CatalogoRepository catalogoRepository;
	@Autowired
	private Environment env;
	@Autowired
	private EmailService emailService;
	@Autowired
	private TokenFcmRepository fcmRepository;
	@Autowired
	private FlorRepository florRepository;
	@Autowired
	private PublicacionRepository publicacionRepository;
	@Autowired
	private IQualityRuleService iQualityRuleService;
	@Autowired
	private PagoPlanRepository pagoPlanRepository;
	@Autowired
	private ParametroRepository parametroRepository;
	@Autowired
	private DeviceTokenRepository deviceTokenRepository;
	@Autowired
	private ProductoRepository productoRepository;
	@Autowired
	private IDisponibilidadService disponibilidadService;
	@Autowired
	private ISaldoService iSaldoService;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Override
	public UsuarioDTO findByUsername(String username) {
		Usuario user = usuarioRepository.findByUsername(username);
		UsuarioDTO usuarioDTO = null;
		if (user != null) {
			String anonymousUser = env.getProperty("config.security.oauth.anonymous");
			Saldo saldoContacto = iSaldoService.obtener(user.getId(), GrupoSaldoEnum.CONTACTO);
			Saldo saldoProducto = iSaldoService.obtener(user.getId(), GrupoSaldoEnum.PRODUCTO);
			usuarioDTO = new UsuarioDTO(user.getId(), user.getActivo().name(), user.getContrasenia(), user.getCorreo(),
					saldoContacto.getDisponible(), saldoContacto.getRecarga(), saldoProducto.getDisponible(),
					saldoProducto.getRecarga());
			usuarioDTO.setPerfiles(perfilRepository.findByUserAndActivo(user.getId(), ValorSiNoEnum.SI));
			usuarioDTO.setNombres(user.getNombres());
			usuarioDTO.setApellidos(user.getApellidos());
			usuarioDTO.setIntentos(user.getIntentos());
			usuarioDTO.setIsAnonymous(anonymousUser.equalsIgnoreCase(username) ? "S" : "N");
			usuarioDTO.setId(user.getId());
			usuarioDTO.setCorreo(user.getCorreo());
			usuarioDTO.setActivo(user.getActivo().name());
			usuarioDTO.setRecuperacion(user.getRecuperacion());
			usuarioDTO.setEsAdmin(user.getEsAdmin());
			usuarioDTO.setTieneDisponibilidad(disponibilidadService.disponibilidadHoy(user.getEmpresa().getId()));
			List<PagoPlan> sus = pagoPlanRepository.findVigenteByIdUsuario(user.getId());
			usuarioDTO.setPlanVigente(!sus.isEmpty());
		}
		return usuarioDTO;
	}

	@Override
	public String validToken(String device) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		DeviceDTO deviceObj = objectMapper.readValue(device, DeviceDTO.class);
		if (!deviceObj.getFirebaseToken().isEmpty()) {
			List<DeviceToken> tokens = deviceTokenRepository.findByToken(deviceObj.getFirebaseToken());
			List<?> tokenuser = fcmRepository.findByToken(deviceObj.getFirebaseToken());
			if (tokens.isEmpty() && tokenuser.isEmpty()) {
				DeviceToken dt = new DeviceToken();
				dt.setMarca(deviceObj.getMarca());
				dt.setModelo(deviceObj.getModelo());
				dt.setOs(deviceObj.getOs());
				dt.setToken(deviceObj.getFirebaseToken());
				dt.setVersionSistem(deviceObj.getVersionSistema());
				dt.setVersionApp(deviceObj.getVersionApp());
				deviceTokenRepository.save(dt);
			}
		}
		return "OK";

	}

	public void deleteTokenDevice(String token) throws Exception {
		List<DeviceToken> tokens = deviceTokenRepository.findByToken(token);
		if (!tokens.isEmpty()) {
			tokens.stream().forEach(to -> deviceTokenRepository.delete(to));
		}
	}

	@Transactional
	@Override
	public String register(RegistroDTO register) throws Exception {
		AES aes = new AES();
		String correo = aes.decrypt(register.getCorreoElectronico());
		String ruc = aes.decrypt(register.getRuc());
		String celular = aes.decrypt(register.getCelular());
		String username = aes.decrypt(register.getCedula());
		String password = aes.decrypt(register.getContrasenia());
		if (!empresaRepository.findByRucContaining(ruc).isEmpty())
			return "Ahora ya puedes crear tus publicaciones y ponerte en contacto con otras empresas.";
		String validate = iQualityRuleService.validateRuc(ruc);
		Parametro idPerfil = parametroRepository.buscarPorId("USUARIO_GIFLO_PERFIL");
		Parametro links = parametroRepository.buscarPorId("VIDEO_LINKS");
		if (idPerfil == null)
			throw new Exception("No esta definido el código del perfil");
		if (validate == null) {
			Empresa emp = new Empresa();
			emp.setActivo(ValorSiNoEnum.SI);
			emp.setCalificacion(BigDecimal.valueOf(0));
			emp.setContCalificacion(Integer.valueOf(0));
			emp.setNombre(register.getNombreEmpresa());
			emp.setRuc(ruc);
			emp.setTipo(catalogoRepository.findById(register.getIdTipo()).orElse(null));
			empresaRepository.save(emp);

			Usuario user = new Usuario();
			user.setActivo(ValorSiNoEnum.SI);
			user.setEsAdmin("NO");
			user.setCorreo(correo);
			user.setFechaRegistro(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
			user.setEmpresa(emp);
			user.setUsername(username);
			user.setNombres(register.getNombres());
			user.setApellidos(register.getApellidos());
			user.setFoto(register.getFoto());
			user.setContrasenia(passwordEncoder.encode(password));
			user.setRecuperacion("NO");
			user.setActivo(ValorSiNoEnum.SI);
			user.setFechaActualizacion(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
			usuarioRepository.save(user);

			Direccion dir = new Direccion();
			dir.setActivo(ValorSiNoEnum.SI);
			dir.setCallePrimaria("N/A");
			dir.setCalleSecundaria("N/A");
			dir.setEmpresa(emp);
			dir.setTipo(catalogoRepository.findById(1).orElse(null));
			dir.setReferencia("N/A");
			dir.setCelular(celular);
			dir.setCiudad(register.getCiudad());
			dir.setLongitud(register.getLongitud());
			dir.setLatitud(register.getLatitud());
			if (register.getIdPais() != null) {
				dir.setPais(catalogoRepository.findById(register.getIdPais()).orElse(null));
			}
			direccionRepository.save(dir);

			Perfil perfil = perfilRepository.findById(Integer.valueOf(idPerfil.getValor())).orElse(null);
			UsuarioPerfil up = new UsuarioPerfil();
			up.setActivo(ValorSiNoEnum.SI);
			up.setPerfil(perfil);
			up.setUsuario(user);
			usuarioPerfilRepository.save(up);
			HashMap<String, String> map = new HashMap<>();
			map.put("usuario", user.getNombres().concat(" ").concat(user.getApellidos()));
			map.put("links", links != null ? links.getValor() : "");
			for (Variedad v : register.getVariedades()) {
				Flor f = new Flor();
				f.setDisponible(ValorSiNoEnum.SI);
				f.setEmpresa(emp);
				f.setVariedad(v);
				florRepository.save(f);
			}
			for (Producto v : register.getProductos()) {
				v.setId(null);
				v.setEmpresa(emp);
				v.setFechaRegistro(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
				v.setEstado(ValorEstadoEnum.ACTIVO);
				productoRepository.save(v);
			}
			Parametro idPlanContactos = parametroRepository.buscarPorId("SUS_PLAN_REGISTRO");
			Plan planContactos=planRepository.findById(Integer.valueOf(idPlanContactos.getValor())).orElse(null);
			iSaldoService.guardar(user.getId(), planContactos.getGrupo(), planContactos.getCantidadContactos());
			
			emailService.sendSimpleMessage(correo, "Registro",
					GifloTemplateEmail.buildEmail(map, TemplateEmailEnum.REGISTRO));
			return String.format("Ahora ya puedes crear tus publicaciones y ponerte en contacto con otras empresas.");
		} else {
			throw new Exception(String.format("%1s : %2s.", ruc, validate));
		}

	}

	@Transactional
	@Override
	public String actualizar(RegistroDTO register) throws Exception {
		AES aes = new AES();
		String correo = aes.decrypt(register.getCorreoElectronico());
		String celular = aes.decrypt(register.getCelular());
		String telefono = aes.decrypt(register.getTelefono());
		String ruc = aes.decrypt(register.getRuc());
		Empresa emp = empresaRepository.findById(register.getIdEmpresa()).orElse(null);
		emp.setNombre(register.getNombreEmpresa());
		emp.setRuc(ruc);
		Usuario user = usuarioRepository.findById(register.getIdUsuario()).orElse(null);
		if (emp == null || user == null) {
			throw new Exception("La empresa o el usuario no existe");
		}
		user.setCorreo(correo);
		user.setNombres(register.getNombres());
		user.setApellidos(register.getApellidos());
		user.setFoto(register.getFoto());
		user.setFechaActualizacion(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
		user.setFotoId(register.getFotoId());
		usuarioRepository.save(user);

		if (user.getTokenFcms() != null) {
			List<TokenFcm> tokensList = fcmRepository.findByTokenAndUsuario(register.getToken(), user);
			if (tokensList.isEmpty()) {
				TokenFcm fcm = new TokenFcm();
				fcm.setUsuario(user);
				fcm.setDevice(register.getDevice());
				fcm.setToken(register.getToken());
				fcm.setRegistro(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
				fcmRepository.save(fcm);
			}

		} else {
			TokenFcm fcm = new TokenFcm();
			fcm.setUsuario(user);
			fcm.setDevice(register.getDevice());
			fcm.setToken(register.getToken());
			fcm.setRegistro(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
			fcmRepository.save(fcm);

		}
		deleteTokenDevice(register.getToken());
		if (register.getFotoNombre() != null && register.getFotoNombre() != "") {
			EmpresaFoto foto = new EmpresaFoto();
			List<EmpresaFoto> list = empresaFotoRepository.findOfEmpresa(register.getIdEmpresa(), ValorSiNoEnum.SI,
					ValorEstadoEnum.ACTIVO);
			if (!list.isEmpty()) {
				for (EmpresaFoto empresaFoto : list) {
					empresaFoto.setPrincipal(ValorSiNoEnum.NO);
					empresaFoto.setFechaActualizacion(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
					empresaFotoRepository.save(empresaFoto);
				}
			}
			foto.setEmpresa(emp);
			foto.setNombre(register.getFotoNombre());
			foto.setFotoId(register.getFotoNombreId());
			foto.setPrincipal(ValorSiNoEnum.SI);
			foto.setEstado(ValorEstadoEnum.ACTIVO);
			foto.setFechaActualizacion(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
			empresaFotoRepository.save(foto);
		}

		List<Direccion> dirs = direccionRepository.findByEmpresaAndActivo(emp, ValorSiNoEnum.SI);
		Direccion dir = new Direccion();
		dir.setEmpresa(emp);
		if (!dirs.isEmpty())
			dir = dirs.get(0);
		dir.setActivo(ValorSiNoEnum.SI);
		dir.setCallePrimaria(register.getCallePrincipal());
		dir.setCalleSecundaria(register.getCalleSecundaria());
		dir.setTipo(catalogoRepository.findById(1).orElse(null));
		dir.setReferencia(register.getReferencia());
		dir.setCelular(celular);
		dir.setTelefono(telefono);
		dir.setNumero(register.getNumero());
		dir.setLongitud(register.getLongitud());
		dir.setLatitud(register.getLatitud());
		dir.setCiudad(register.getCiudad());
		if (register.getIdPais() != null) {
			dir.setPais(catalogoRepository.findById(register.getIdPais()).orElse(null));
		}
		direccionRepository.save(dir);

		HashMap<String, String> map = new HashMap<>();
		map.put("usuario", user.getNombres().concat(" ").concat(user.getApellidos()));

		return "Se actualizo correctamente.";
	}

	private static final SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	@Transactional
	@Override
	public String recuperarContrasenia(String usernameEncrypt) throws Exception {
		AES aes = new AES();
		String minuts = env.getProperty("config.password.valid");
		String username = aes.decrypt(usernameEncrypt);
		List<Usuario> usuarios = usuarioRepository.byUsername(username);
		if (!usuarios.isEmpty()) {
			if (usuarios.size() == 1) {
				Usuario user = usuarios.get(0);
				if ("SI".equalsIgnoreCase(user.getRecuperacion())) {
					Timestamp current = new Timestamp(GregorianCalendar.getInstance().getTimeInMillis());
					Timestamp validUntil = user.getClaveValidez();
					if (current.before(validUntil)) {
						throw new Exception(
								String.format("Ya existe un petición previa, puedes intentar neuvamente luedo de %1s",
										sdf3.format(validUntil)));
					}
				}
				String tempPass = generateCommonLangPassword();
				Calendar c = GregorianCalendar.getInstance();
				c.add(Calendar.MINUTE, Integer.valueOf(minuts));
				Timestamp timestamp = new Timestamp(c.getTimeInMillis());
				user.setContrasenia(passwordEncoder.encode(tempPass));
				user.setRecuperacion("SI");
				user.setClaveValidez(timestamp);
				user.setActivo(ValorSiNoEnum.SI);
				HashMap<String, String> map = new HashMap<>();
				map.put("usuario", user.getNombres().concat(" ").concat(user.getApellidos()));
				map.put("clave", tempPass);
				map.put("tiempo", sdf3.format(timestamp));
				emailService.sendSimpleMessage(user.getCorreo(), "Recuperación Contraseña",
						GifloTemplateEmail.buildEmail(map, TemplateEmailEnum.CAMBIO_CLAVE));
				return String.format(
						"Recuperación exitosa, se le envió a su correo %1s con la contraseña temporal, valida por %2s minutos ",
						AES.maskEmail(user.getCorreo()), minuts);
			} else {
				throw new Exception(
						String.format("Existe más de un usuario con la misma identificación %1s", username));
			}

		} else {
			throw new Exception(String.format("El usuario con la identificación %1s no existe", username));
		}
	}

	@Transactional
	@Override
	public String cambiarContrasenia(String usernameEncrypt, String passwordeEncrypt) throws Exception {
		AES aes = new AES();
		String username = aes.decrypt(usernameEncrypt);
		String passwoprd = aes.decrypt(passwordeEncrypt);
		List<Usuario> usuarios = usuarioRepository.byUserRecuperacion(username);
		if (!usuarios.isEmpty()) {
			if (usuarios.size() == 1) {
				Usuario user = usuarios.get(0);
				Timestamp current = new Timestamp(GregorianCalendar.getInstance().getTimeInMillis());
				Timestamp validUntil = user.getClaveValidez();
				if (current.before(validUntil)) {
					user.setContrasenia(passwordEncoder.encode(passwoprd));
					user.setRecuperacion("NO");
					user.setActivo(ValorSiNoEnum.SI);
					user.setIntentos(0);
					HashMap<String, String> map = new HashMap<>();
					map.put("usuario", user.NombresCompletos());
					emailService.sendSimpleMessage(user.getCorreo(), "Confirmación de cambio de Contraseña",
							GifloTemplateEmail.buildEmail(map, TemplateEmailEnum.CAMBIO_PASS_EXITO));
					return "Actualizado correctamente!.";
				} else {
					throw new Exception(
							String.format("La clave ya ha caducado, fecha actual %1s", sdf3.format(current)));
				}
			} else {
				throw new Exception(
						String.format("Existe más de un usuario con la misma identificación %1s", username));
			}
		} else {
			throw new Exception(
					String.format("El usuario con la identificación %1s no existe o esta inactivo", username));
		}
	}

	public String generateCommonLangPassword() {
		String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
		String lowerCaseLetters = RandomStringUtils.random(1, 97, 122, true, true);
		String numbers = RandomStringUtils.randomNumeric(2);
		String totalChars = RandomStringUtils.randomAlphanumeric(1);
		String combinedChars = upperCaseLetters.concat(lowerCaseLetters).concat(numbers).concat(totalChars);
		List<Character> pwdChars = combinedChars.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
		Collections.shuffle(pwdChars);
		String password = pwdChars.stream().collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
				.toString();
		return password;
	}

	@Override
	public UsuarioDTO findById(Integer idUsuario) {
		try {
			Usuario us = usuarioRepository.byIdCustom(idUsuario);
			List<Flor> flores = florRepository.findPorUsuario(idUsuario);
			List<PublicacionDTO> publicaciones = publicacionRepository.findByUsuario(idUsuario).stream()
					.map(p -> new PublicacionDTO(p)).collect(Collectors.toList());
			Empresa empresa = new Empresa(us.getEmpresa().getId(), us.getEmpresa().getNombre(),
					us.getEmpresa().getRuc(), us.getEmpresa().getFotos(), us.getEmpresa().getDirecciones(),
					us.getEmpresa().getCalificacion(), us.getEmpresa().getContCalificacion(),
					us.getEmpresa().getTipo().getNombre(), us.getEmpresa().getSriValido(),
					us.getEmpresa().getTipo().getCodigo());
			List<EmpresaFoto> list = empresaFotoRepository.findOfEmpresa(us.getEmpresa().getId(), ValorSiNoEnum.SI,
					ValorEstadoEnum.ACTIVO);
			if (!list.isEmpty()) {
				for (EmpresaFoto empresaFoto : list) {
					if (ValorSiNoEnum.SI.equals(empresaFoto.getPrincipal())) {
						empresa.setUrlImagen(list.get(0).getNombre());
						break;
					}
				}
			}
			Saldo saldoContacto = iSaldoService.obtener(us.getId(), GrupoSaldoEnum.CONTACTO);
			Saldo saldoProducto = iSaldoService.obtener(us.getId(), GrupoSaldoEnum.PRODUCTO);
			UsuarioDTO dto = new UsuarioDTO(us, empresa, flores, publicaciones, saldoContacto.getDisponible(),
					saldoContacto.getRecarga(), saldoProducto.getDisponible(), saldoProducto.getRecarga());
			List<PagoPlan> sus = pagoPlanRepository.findVigenteByIdUsuario(us.getId());
			dto.setPlanVigente(!sus.isEmpty());
			return dto;
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public UsuarioDTO findByIdResume(Integer idUsuario) {
		try {
			Usuario us = usuarioRepository.byIdCustom(idUsuario);

			Empresa empresa = new Empresa(us.getEmpresa().getId(), us.getEmpresa().getNombre(),
					us.getEmpresa().getRuc(), us.getEmpresa().getFotos(), us.getEmpresa().getDirecciones(),
					us.getEmpresa().getCalificacion(), us.getEmpresa().getContCalificacion(),
					us.getEmpresa().getTipo().getNombre(), us.getEmpresa().getSriValido(),
					us.getEmpresa().getTipo().getCodigo());
			List<EmpresaFoto> list = empresaFotoRepository.findOfEmpresa(us.getEmpresa().getId(), ValorSiNoEnum.SI,
					ValorEstadoEnum.ACTIVO);
			if (!list.isEmpty()) {
				for (EmpresaFoto empresaFoto : list) {
					if (ValorSiNoEnum.SI.equals(empresaFoto.getPrincipal())) {
						empresa.setUrlImagen(list.get(0).getNombre());
						break;
					}
				}
			}
			Saldo saldoContacto = iSaldoService.obtener(us.getId(), GrupoSaldoEnum.CONTACTO);
			Saldo saldoProducto = iSaldoService.obtener(us.getId(), GrupoSaldoEnum.PRODUCTO);
			UsuarioDTO dto = new UsuarioDTO(us, empresa, new ArrayList<>(), new ArrayList<>(),
					saldoContacto.getDisponible(), saldoContacto.getRecarga(), saldoProducto.getDisponible(),
					saldoProducto.getRecarga());
			return dto;
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public UsuarioDTO findByIdCustom(Integer id, String token, String device) throws Exception {
		Usuario us = usuarioRepository.byIdCustom(id);
		if (token != null && !token.trim().isEmpty()) {
			if (us.getTokenFcms() != null) {
				List<TokenFcm> tokensList = fcmRepository.findByUsuarioAndDevice(us, device);
				if (tokensList.isEmpty()) {
					TokenFcm fcm = new TokenFcm();
					fcm.setUsuario(us);
					fcm.setDevice(device);
					fcm.setToken(token);
					fcm.setRegistro(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
					fcmRepository.save(fcm);
				} else {
					TokenFcm fcm = tokensList.get(0);
					fcm.setToken(token);
					fcm.setRegistro(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
					fcmRepository.save(fcm);
				}

			} else {
				TokenFcm fcm = new TokenFcm();
				fcm.setUsuario(us);
				fcm.setDevice(device);
				fcm.setToken(token);
				fcm.setRegistro(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
				fcmRepository.save(fcm);
			}
			deleteTokenDevice(token);
		}
		List<Flor> flores = florRepository.findPorUsuario(id);
		List<PublicacionDTO> publicaciones = publicacionRepository.findByUsuario(id).stream()
				.map(p -> new PublicacionDTO(p)).collect(Collectors.toList());
		Empresa empresa = new Empresa(us.getEmpresa().getId(), us.getEmpresa().getNombre(), us.getEmpresa().getRuc(),
				us.getEmpresa().getFotos(), us.getEmpresa().getDirecciones(), us.getEmpresa().getCalificacion(),
				us.getEmpresa().getContCalificacion(), us.getEmpresa().getTipo().getNombre(),
				us.getEmpresa().getSriValido(), us.getEmpresa().getTipo().getCodigo());
		List<EmpresaFoto> list = empresaFotoRepository.findOfEmpresa(us.getEmpresa().getId(), ValorSiNoEnum.SI,
				ValorEstadoEnum.ACTIVO);
		if (!list.isEmpty()) {
			for (EmpresaFoto empresaFoto : list) {
				if (ValorSiNoEnum.SI.equals(empresaFoto.getPrincipal())) {
					empresa.setUrlImagen(list.get(0).getNombre());
					break;
				}
			}
		}
		Saldo saldoContacto = iSaldoService.obtener(us.getId(), GrupoSaldoEnum.CONTACTO);
		Saldo saldoProducto = iSaldoService.obtener(us.getId(), GrupoSaldoEnum.PRODUCTO);
		UsuarioDTO dto = new UsuarioDTO(us, empresa, flores, publicaciones, saldoContacto.getDisponible(),
				saldoContacto.getRecarga(),saldoProducto.getDisponible(),
				saldoProducto.getRecarga());
		List<PagoPlan> sus = pagoPlanRepository.findVigenteByIdUsuario(us.getId());
		dto.setPlanVigente(!sus.isEmpty());

		return dto;
	}

	@Override
	public List<Usuario> findTokens(Integer idUser) throws Exception {
		List<Usuario> tokens = new ArrayList<Usuario>(0);
		try {
			List<Object[]> users = notificacionPushRepository.findUserNotify(idUser);
			for (Object[] ids : users) {
				Integer id = (Integer) ids[0];
				Usuario u = usuarioRepository.findById(id).orElse(null);
				u = new Usuario(u.getId(), u.getApellidos(), u.getNombres(), u.getTokenFcms());
				tokens.add(u);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tokens;
	}

	@Override
	public Boolean removeDevice(Integer idUser, String device) throws Exception {
		boolean respuesta = false;
		Usuario us = usuarioRepository.findById(idUser).orElse(null);
		if (us != null) {
			List<TokenFcm> tokensList = fcmRepository.findByUsuarioAndDevice(us, device);
			if (!tokensList.isEmpty()) {
				TokenFcm fcm = tokensList.get(0);
				fcmRepository.delete(fcm);
				respuesta = true;
			}
			validToken(device);
		}
		return respuesta;
	}

	@Override
	public UsuarioDTO findByIdEmpresa(Integer idEmpresa, Integer idUser) throws Exception {
		Usuario us = usuarioRepository.findByEmpresa(idEmpresa);
		UsuarioDTO dto = new UsuarioDTO();
		Boolean suscrito = false;
		if (us != null) {
			List<Flor> flores = florRepository.findPorUsuario(us.getId());
			List<PublicacionDTO> pub = publicacionRepository.findByUsuario(us.getId()).stream()
					.map(p -> new PublicacionDTO(p)).collect(Collectors.toList());
			Empresa empresa = new Empresa(us.getEmpresa().getId(), us.getEmpresa().getNombre(),
					us.getEmpresa().getRuc(), us.getEmpresa().getFotos(), us.getEmpresa().getDirecciones(),
					us.getEmpresa().getCalificacion(), us.getEmpresa().getContCalificacion(),
					us.getEmpresa().getTipo().getNombre(), us.getEmpresa().getSriValido(),
					us.getEmpresa().getTipo().getCodigo());
			List<EmpresaFoto> list = empresaFotoRepository.findOfEmpresa(us.getEmpresa().getId(), ValorSiNoEnum.SI,
					ValorEstadoEnum.ACTIVO);
			if (notificacionPushRepository.findByOrigenAndDestino(us.getId(), idUser) != null) {
				suscrito = true;
			}
			if (!list.isEmpty()) {
				for (EmpresaFoto empresaFoto : list) {
					if (ValorSiNoEnum.SI.equals(empresaFoto.getPrincipal())) {
						empresa.setUrlImagen(list.get(0).getNombre());
						break;
					}
				}
			}
			Saldo saldoContacto = iSaldoService.obtener(us.getId(), GrupoSaldoEnum.CONTACTO);
			Saldo saldoProducto = iSaldoService.obtener(us.getId(), GrupoSaldoEnum.PRODUCTO);
			dto = new UsuarioDTO(us, empresa, flores, pub, saldoContacto.getDisponible(), saldoContacto.getRecarga(),saldoProducto.getDisponible(), saldoProducto.getRecarga());
			dto.setSuscrito(suscrito);
			List<PagoPlan> sus = pagoPlanRepository.findVigenteByIdUsuario(us.getId());
			dto.setPlanVigente(!sus.isEmpty());
		}
		return dto;
	}

	@Transactional
	@Override
	public String updateLocation(Integer idUsuario, String longitud, String latitud) throws Exception {
		Usuario us = usuarioRepository.findById(idUsuario).orElse(null);
		if (us != null) {
			List<Direccion> dirs = direccionRepository.findByEmpresaAndActivo(us.getEmpresa(), ValorSiNoEnum.SI);
			if (!dirs.isEmpty()) {
				Direccion dir = dirs.get(0);
				;
				dir.setLongitud(longitud);
				dir.setLatitud(latitud);
				direccionRepository.save(dir);
			}

		}
		return "OK";
	}

	@Override
	public List<UsuarioDTO> search(String filtro, Integer idUser) throws Exception {
		filtro = filtro.isEmpty() ? filtro : "%".concat(filtro).concat("%");
		List<Usuario> users = usuarioRepository.search(filtro, idUser);
		return users.stream().map((u) -> {
			UsuarioDTO user = findById(u.getId());
			return user;
		}).collect(Collectors.toList());
	}

	@Override
	public List<UsuarioDTO> searchActive(String filtro) throws Exception {
		filtro = filtro.isEmpty() ? filtro : "%".concat(filtro).concat("%");
		List<Usuario> users = usuarioRepository.searchActive(filtro);
		return users.stream().map((u) -> {
			UsuarioDTO user = findByIdResume(u.getId());
			return user;
		}).collect(Collectors.toList());
	}

}
