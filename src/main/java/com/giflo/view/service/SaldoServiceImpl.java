package com.giflo.view.service;

import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.giflo.model.entity.Saldo;
import com.giflo.model.enums.GrupoSaldoEnum;
import com.giflo.model.repository.SaldoRepository;
import com.giflo.model.repository.UsuarioRepository;

@Service
public class SaldoServiceImpl implements ISaldoService {

	@Autowired
	private SaldoRepository saldoRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public void guardar(Integer idUsuario, GrupoSaldoEnum codigoTipo, Integer recarga) {
		List<Saldo> list = saldoRepository.findByUsuarioAndTipo(idUsuario, codigoTipo);
		Saldo saldo = new Saldo();
		if (list.isEmpty()) {
			saldo.setDisponible(recarga);
			saldo.setRecarga(recarga);
			saldo.setFechaRegistro(new Timestamp(GregorianCalendar.getInstance().getTime().getTime()));
			saldo.setFechaRecarga(new Timestamp(GregorianCalendar.getInstance().getTime().getTime()));
			saldo.setGrupo(codigoTipo);
			saldo.setUsuario(usuarioRepository.findById(idUsuario).orElse(null));
		} else {
			saldo = list.get(0);
			saldo.setDisponible(saldo.getDisponible() + recarga);
			saldo.setRecarga(saldo.getRecarga() + recarga);
			saldo.setFechaRecarga(new Timestamp(GregorianCalendar.getInstance().getTime().getTime()));
		}
		saldoRepository.save(saldo);
	}

	@Override
	public void usar(Integer idUsuario, GrupoSaldoEnum codigoTipo, Integer recarga) {
		List<Saldo> list = saldoRepository.findByUsuarioAndTipo(idUsuario, codigoTipo);
		Saldo saldo = new Saldo();
		if (!list.isEmpty()) {
			saldo = list.get(0);
			saldo.setDisponible(saldo.getDisponible() - recarga);
			saldoRepository.save(saldo);
		}

	}

	@Override
	public Saldo obtener(Integer idUsuario, GrupoSaldoEnum codigoTipo) {
		List<Saldo> list = saldoRepository.findByUsuarioAndTipo(idUsuario, codigoTipo);
		Saldo saldo = new Saldo(0, 0);
		if (!list.isEmpty()) {
			saldo = list.get(0);
		}
		return saldo;

	}

	@Override
	public void descartar(Integer idUsuario, GrupoSaldoEnum codigoTipo, Integer recarga) {
		List<Saldo> list = saldoRepository.findByUsuarioAndTipo(idUsuario, codigoTipo);
		Saldo saldo = new Saldo();
		if (!list.isEmpty()) {
			saldo = list.get(0);
			saldo.setDisponible(saldo.getDisponible() - recarga);
			saldo.setRecarga(saldo.getRecarga() - recarga);
			saldoRepository.save(saldo);
		}
	}

}
