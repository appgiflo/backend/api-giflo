package com.giflo.view.service;

import java.util.List;

import com.giflo.model.dto.MessageDTO;
import com.giflo.model.entity.Publicacion;
import com.giflo.model.entity.Usuario;
import com.google.firebase.messaging.FirebaseMessagingException;

public interface IFirebaseMessageService {

	/**
	 * Crea una notificación push
	 * 
	 * @param p
	 * @param token
	 */
	public void sendPushPublicacion(Publicacion p, List<Usuario> tokens);

	/**
	 * Envia una notificacion a todos los usuarios
	 * @param dto
	 * @throws FirebaseMessagingException 
	 */
	MessageDTO sendPushMessage(MessageDTO dto,Integer idUsuario) throws FirebaseMessagingException;

	/**
	 * Envia una notificación a un usuario en especifico
	 * @param dto
	 * @param u
	 * @return
	 * @throws FirebaseMessagingException
	 */
	MessageDTO sendPushMessageToUser(MessageDTO dto, Usuario u) throws FirebaseMessagingException;

}
