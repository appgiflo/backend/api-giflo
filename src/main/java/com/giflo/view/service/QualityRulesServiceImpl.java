package com.giflo.view.service;

import org.springframework.stereotype.Component;

@Component
public class QualityRulesServiceImpl implements IQualityRuleService {

	public String validateRuc(String ea_ruc) {
		    //Validamos si ha ingresado 13 digitos
		    int codigoRegion = 0;
		    int codigoTipo = 0;
		    int digitoVerificador = 0;
		    int digitoResultante = 0;
		    String digitos[];
		    int producto = -1;
		    int sucursal = 0;
		    if (ea_ruc.length() == 13) {
			      digitos = ea_ruc.split("");
			      codigoRegion =  Integer.parseInt(ea_ruc.substring(0, 2));
			      codigoTipo = Integer.parseInt(digitos[2]);
			      sucursal = Integer.parseInt(ea_ruc.substring(10, 13));
			      if (codigoTipo == 9) {
				        digitoVerificador = Integer.parseInt(digitos[9]);
				        int coeficientes[] = {4, 3, 2, 7, 6, 5, 4, 3, 2};
				        producto = multiplica(digitos, coeficientes, codigoTipo);
				        digitoResultante = producto % 11;
				        digitoResultante = digitoResultante > 0 ? (11 - digitoResultante) : digitoResultante;
			      } else if (codigoTipo == 6) {
				        digitoVerificador = Integer.parseInt(digitos[8]);
				        int coeficientes[] = {3, 2, 7, 6, 5, 4, 3, 2};
				        producto = multiplica(digitos, coeficientes, codigoTipo);
				        digitoResultante = producto % 11;
				        digitoResultante = digitoResultante > 0 ? (11 - digitoResultante) : digitoResultante;
			      } else {
				        digitoVerificador = Integer.parseInt(digitos[9]);
				        int coeficientes[] = {2, 1, 2, 1, 2, 1, 2, 1, 2};
				        producto = multiplica(digitos, coeficientes, codigoTipo);
				        digitoResultante = producto % 10;
				        digitoResultante = digitoResultante > 0 ? (10 - digitoResultante) : digitoResultante;
			      }
			      if (sucursal <= 0)
			    	  return "Sucursal Invalida";
			      else if ((codigoRegion <= 0 || codigoRegion > 22) && codigoRegion != 30)
			    	  return "Ruc Inválido";
			      else if (digitoResultante != digitoVerificador) {
			    	  return "Ruc Inválido";
			      }
			      return null;
			      
		    } else if (ea_ruc.length() > 0) {
		    	return null;
		    } else {
		    	return null;
		    }
	}
	
	int multiplica(String digitos[], int coeficientes[], int codigoTipo) {
		  int sum = 0;
		  for (int i = 0; i < coeficientes.length; i++) {
			  int producto = Integer.parseInt(digitos[i]) * coeficientes[i];
			  if (producto >= 10 && codigoTipo < 6) {
				  producto -= 9;
			  }
			  sum += producto;
		  }
		  return sum;
	}
		
}
