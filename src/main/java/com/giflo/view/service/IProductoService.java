package com.giflo.view.service;

import java.util.List;
import java.util.Map;

import com.giflo.model.entity.Producto;

public interface IProductoService {

	/**
	 * 
	 * @param p
	 * @return
	 */
	List<Producto> guardar(Producto p);

	
	/**
	 * 
	 * @param idEmpresa
	 * @return
	 */
	List<Producto> buscarPorEmpresa(Integer idEmpresa);

	/**
	 * 
	 * @param search
	 * @param page
	 * @param size
	 * @return
	 */
	Map<String, Object> buscarPorFiltro(String search, Integer page, Integer size);

}
