package com.giflo.view.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.giflo.model.entity.Empresa;
import com.giflo.model.entity.Flor;
import com.giflo.model.entity.Variedad;
import com.giflo.model.enums.ValorSiNoEnum;
import com.giflo.model.repository.EmpresaRepository;
import com.giflo.model.repository.FlorRepository;
import com.giflo.model.repository.VariedadRepository;

@Service
public class FlorServiceImpl implements IFlorService {

	@Autowired
	private EmpresaRepository empresaRepository;

	@Autowired
	private VariedadRepository variedadRepository;

	@Autowired
	private FlorRepository florRepository;

	@Override
	public Flor crear(final Integer idEmpresa, final Integer idVariedad) throws Exception {
		Empresa empresa = empresaRepository.findById(idEmpresa).orElse(null);
		Variedad variedad = variedadRepository.findById(idVariedad).orElse(null);
		if (empresa != null && variedad != null) {
			Flor flor = null;
			List<Flor> list = florRepository.validExist(idEmpresa, idVariedad);
			if (list.isEmpty()) {
				flor = new Flor();

			} else {
				flor = list.get(0);
				if (ValorSiNoEnum.SI.equals(flor.getDisponible()))
					throw new Exception("Ya tienes agregado esta variedad");
			}
			flor.setEmpresa(empresa);
			flor.setVariedad(variedad);
			flor.setDisponible(ValorSiNoEnum.SI);
			florRepository.save(flor);
			Flor flordDto = new Flor();
			flordDto.setFoto(variedad.getNombreFoto());
			flordDto.setFotoPequenia(variedad.getFotoPequenia());
			flordDto.setNombre(variedad.getNombre());
			flordDto.setIndicaciones(variedad.getIndicaciones());
			flordDto.setDisponible(flor.getDisponible());
			flordDto.setId(flor.getId());
			return flordDto;

		}
		return null;

	}

	@Override
	public Flor actualziar(Flor flor, Integer id) throws Exception {
		Flor florDb = florRepository.findById(id).orElse(null);
		if (florDb != null) {
			florDb.setDisponible(flor.getDisponible());
			florDb = florRepository.save(florDb);
			return flor;
		}
		return null;
	}

}
