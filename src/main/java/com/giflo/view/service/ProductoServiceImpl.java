package com.giflo.view.service;

import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.giflo.model.entity.Producto;
import com.giflo.model.enums.GrupoSaldoEnum;
import com.giflo.model.repository.EmpresaRepository;
import com.giflo.model.repository.ProductoRepository;

@Service
public class ProductoServiceImpl implements IProductoService {

	@Autowired
	private ProductoRepository productoRepository;

	@Autowired
	private EmpresaRepository empreRepository;
	
	@Autowired
	private ISaldoService iSaldoService;

	@Override
	public List<Producto> guardar(Producto p) {
		if (p.getId().equals(-1)) {
			p.setId(null);
			p.setFechaRegistro(new Timestamp(GregorianCalendar.getInstance().getTime().getTime()));
		}
		p.setEmpresa(empreRepository.findById(p.getIdEmpresa()).orElse(null));
		productoRepository.save(p);
		iSaldoService.usar(p.getIdUsuario(), GrupoSaldoEnum.PRODUCTO, 1);
		return buscarPorEmpresa(p.getIdEmpresa());
	}

	@Override
	public List<Producto> buscarPorEmpresa(Integer idEmpresa) {
		List<Producto> list=productoRepository.findByEmpresa(idEmpresa).stream().map(p -> new Producto(p))
				.collect(Collectors.toList());
		return list;
	}

	@Override
	public Map<String, Object> buscarPorFiltro(String search, Integer page, Integer size) {
		Pageable paging = PageRequest.of(page, size);
		search = search != null ? search.trim() : "";
		Page<Producto> padeRet = productoRepository.findByPage(search != null ? search.trim() : "", paging);
		List<Producto> lista = padeRet.getContent().stream().map(p -> new Producto(p)).collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		response.put("data", lista);
		response.put("page", padeRet.getNumber());
		response.put("totalPages", padeRet.getTotalPages());
		response.put("count", padeRet.getTotalElements());
		return response;
	}

}
