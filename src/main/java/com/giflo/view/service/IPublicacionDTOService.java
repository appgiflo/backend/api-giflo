package com.giflo.view.service;

import java.util.List;
import java.util.Map;

import com.giflo.model.dto.PublicacionDTO;
import com.giflo.model.entity.Publicacion;

public interface IPublicacionDTOService {

	/**
	 * Obtiene las publicaciones de una empresa
	 * 
	 * @param idEmpresa
	 * @return
	 */
	List<PublicacionDTO> findByEmpresaVigente(Integer idEmpresa, Integer idPublicacion);

	/**
	 * Crea una neuva publicacion
	 * 
	 * @param p
	 * @return
	 * @throws Exception
	 */
	List<PublicacionDTO> crear(List<Publicacion> p) throws Exception;

	/**
	 * 
	 * @param id
	 * @param p
	 * @return
	 */
	Boolean activarInactivar(Integer id, PublicacionDTO p);

	Map<String, Object> findByFlorAndPublicacion(Integer idTipoPublicacion, Integer idColor, String search,
			Integer page, Integer size);

	/**
	 * 
	 * @param idUsuario
	 * @return
	 */
	List<PublicacionDTO> findByUsuarioVigente(Integer idUsuario);

	/**
	 * Busca publicaciones por paginación
	 * 
	 * @param search
	 * @param page
	 * @param size
	 * @return
	 */
	Map<String, Object> findByFilter(String search, Integer page, Integer size);

	/**
	 * Obtiene todas las publicaciones de un usuario
	 * 
	 * @param idUsuario
	 * @return
	 */
	List<PublicacionDTO> findAllByUser(Integer idUsuario);

	/**
	 * Elimina una publicación
	 * @param id
	 * @return
	 */
	Boolean eliminarPublicacion(Integer id);

	/**
	 * Busca por color, tipoPublicacion, variedad y largo tallo
	 * @param idTipoPublicacion
	 * @param idColor
	 * @param search
	 * @param page
	 * @param size
	 * @param idTallo
	 * @return
	 */
	Map<String, Object> findByFlorAndPublicacionAndTallo(Integer idTipoPublicacion, Integer idColor, String search,
			Integer page, Integer size, Integer idTallo);

	/**
	 * 
	 * @param idPublicacion
	 * @return
	 * @throws Exception
	 */
	PublicacionDTO enviarNotificacionPush(Integer idPublicacion) throws Exception;
}
