package com.giflo.view.service;

import java.util.Map;

import com.giflo.model.dto.ReseniaDTO;

public interface IReseniaService {
	
	/**
	 * 
	 * @param Registro de resenia
	 * @return
	 * @throws Exception
	 */
	ReseniaDTO registerReview(ReseniaDTO register) throws Exception;
	
	/**
	 * 
	 * @param Eliminar resenia
	 * @return
	 * @throws Exception
	 */
	ReseniaDTO deleteReview(Integer idResenia) throws Exception;
	
	/**
	 * 
	 * @param Recuperamos datos de resenia
	 * @return
	 * @throws Exception
	 */
	ReseniaDTO recoverReview(Integer idEmpresa, Integer idUsuario) throws Exception;
	
	/**
	 * 
	 * @param Paginamos opiniones y calificaciones
	 * @return map
	 * @throws Exception
	 */
	Map<String, Object> findByEmpresa(Integer idEmpresa, Integer page, Integer size);
	
}
