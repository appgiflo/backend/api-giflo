package com.giflo.view.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.giflo.model.entity.Catalogo;
import com.giflo.model.entity.Disponibilidad;
import com.giflo.model.enums.CatalogoEnum;
import com.giflo.model.enums.ValorEstadoEnum;
import com.giflo.model.repository.CatalogoRepository;
import com.giflo.model.repository.DisponibilidadRepository;
import com.giflo.model.repository.FlorRepository;

@Service
public class DisponibilidadServiceImpl implements IDisponibilidadService {

	@Autowired
	private FlorRepository florRepository;

	@Autowired
	DisponibilidadRepository disponibilidadRepository;

	@Autowired
	CatalogoRepository catalogoRepository;

	@Override
	public List<Disponibilidad> guardar(List<Disponibilidad> listado) {
		List<Disponibilidad> list = listado.stream().map(d -> {
			d.setFlor(florRepository.findById(d.getIdFlor()).orElse(null));
			d.setUnidadMedida(catalogoRepository.findById(d.getIdUnidadMedida()).orElse(null));
			if (d.getIdLongitud() != null && d.getIdLongitud() != -1)
				d.setLongitud(catalogoRepository.findById(d.getIdLongitud()).orElse(null));
			d.setEstado(ValorEstadoEnum.ACTIVO);
			d.setFechaActualizacion(GregorianCalendar.getInstance().getTime());
			disponibilidadRepository.save(d);
			return new Disponibilidad(d);
		}).collect(Collectors.toList());
		return list;
	}

	@Override
	public List<Disponibilidad> obtenerDisponiblidadDetallePorTallos(Integer idVariedad, Integer idPais) {
		return disponibilidadRepository.obtenerDisponibilidadPorVariedad(idVariedad, idPais);
	}

	@Override
	public List<Disponibilidad> obtenerDisponiblidadDetallePorBonches(Integer idVariedad, Integer idPais,
			Integer idLongitud) {
		return disponibilidadRepository.obtenerDisponibilidadPorVariedadAndLongitud(idVariedad, idPais, idLongitud);
	}

	@Override
	public List<Disponibilidad> getDisponibilidadPorBonches() {
		return disponibilidadRepository.getDisponibilidadBonches(GregorianCalendar.getInstance().getTime()).stream()
				.map(ob -> {
					BigDecimal cantidad = (BigDecimal) ob[0];
					String longitudNombre = String.valueOf(ob[1]);
					Integer longitudId = Integer.valueOf(ob[2].toString());
					String variedadNombre = String.valueOf(ob[3]);
					Integer variedadId = Integer.valueOf(ob[4].toString());
					String paisNombre = String.valueOf(ob[5]);
					Integer paisId = Integer.valueOf(ob[6].toString());
					return new Disponibilidad(cantidad, longitudNombre, longitudId, variedadNombre, variedadId,
							paisNombre, paisId);
				}).collect(Collectors.toList());

	}

	@Override
	public List<Disponibilidad> getDisponibilidadPorTallos() {
		return disponibilidadRepository.getDisponibilidadVariedad(GregorianCalendar.getInstance().getTime()).stream()
				.map(ob -> {
					BigDecimal cantidad = (BigDecimal) ob[0];
					String variedadNombre = String.valueOf(ob[1]);
					Integer variedadId = Integer.valueOf(ob[2].toString());
					String paisNombre = String.valueOf(ob[3]);
					Integer paisId = Integer.valueOf(ob[4].toString());
					return new Disponibilidad(cantidad, variedadNombre, variedadId, paisNombre, paisId, false);
				}).collect(Collectors.toList());

	}

	@Override
	public List<Disponibilidad> obtener(Integer idEmpresa, CatalogoEnum codigoUnidad) {
		final List<Disponibilidad> lista = new ArrayList<>();
		List<Catalogo> longitudes = catalogoRepository
				.findByGrupoAndActivoOrderByOrdenAsc(CatalogoEnum.LARGO_TALLO.name());
		florRepository.obtenerFloresPorEmpresa(idEmpresa).stream().forEach(f -> {
			if (CatalogoEnum.UNIDAD_TALLOS.equals(codigoUnidad)) {
				List<Disponibilidad> list = disponibilidadRepository.obtenerDisponibilidadPorFlorAndTallo(f.getId());
				if (list.isEmpty()) {
					Disponibilidad d = new Disponibilidad();
					d.setIdFlor(f.getId());
					d.setVariedad(f.getVariedad().getNombre());
					d.setCantidad(BigDecimal.valueOf(0));
					d.setPrecio(BigDecimal.valueOf(0));
					lista.add(d);
				} else
					list.stream().forEach(disp -> {
						lista.add(disp);
					});
			} else {
				List<Disponibilidad> list = disponibilidadRepository.obtenerDisponibilidadPorFlorAndBonches(f.getId());
				if (list.isEmpty()) {
					longitudes.stream().forEach(l -> {
						Disponibilidad d = new Disponibilidad();
						d.setIdFlor(f.getId());
						d.setVariedad(f.getVariedad().getNombre());
						d.setCantidad(BigDecimal.valueOf(0));
						d.setPrecio(BigDecimal.valueOf(0));
						d.setIdLongitud(l.getId());
						d.setValorLongitud(l.getNombre());
						lista.add(d);
					});

				} else {
					list.stream().forEach(disp -> {
						lista.add(disp);
					});
				}
			}

		});
		return lista;
	}

	@Override
	public Boolean disponibilidadHoy(Integer idEmpresa) {
		List<?> list = disponibilidadRepository.obtenerDisponibilidad(idEmpresa,
				GregorianCalendar.getInstance().getTime());
		return !list.isEmpty();
	}

}
