package com.giflo.view.service;

import java.util.Map;

import com.giflo.model.entity.Empleo;

public interface IEmpleoService {

	/**
	 * Guarda los datos de un empleo
	 * @param empleo
	 * @return
	 * @throws Exception 
	 */
	public Empleo save(Empleo empleo) throws Exception;
	/**
	 * Cambia el estado de un empleo
	 * @param id
	 * @param estado
	 * @return
	 * @throws Exception 
	 */
	public Empleo cambiarEstado(Integer id, String estado) throws Exception;
	/**
	 * 
	 * @param filtro
	 * @param page
	 * @param size
	 * @return
	 */
	Map<String, Object> filterActive(String filtro, Integer page, Integer size);
	
	/**
	 * 
	 * @param filtro
	 * @param idUsuario
	 * @param page
	 * @param size
	 * @return
	 */
	Map<String, Object> filterByUsuario(String filtro, Integer idUsuario, Integer page, Integer size);
	/**
	 * Envia una notificación push
	 * @param id
	 * @return
	 * @throws Exception
	 */
	Empleo enviarNotificacion(Integer id) throws Exception;

}
