package com.giflo.view.service;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import com.giflo.model.dto.CuponDTO;
import com.giflo.model.dto.PagoPlanDTO;

public interface IPagoPlanService {

	/**
	 * 
	 * @param Registro
	 * @return
	 * @throws Exception
	 */
	PagoPlanDTO registrarPago(PagoPlanDTO register) throws Exception;

	/**
	 * 
	 * @param Recuperacion de datos
	 * @return
	 * @throws Exception
	 */
	List<PagoPlanDTO> obtenerPago(Integer idUsuario) throws Exception;

	/**
	 * 
	 * @param Cupon aplicado
	 * @return
	 * @throws Exception
	 */
	CuponDTO couponApplied(Integer idPlan, String codigoCupon) throws Exception;

	/**
	 * 
	 * @param r
	 * @return
	 * @throws Exception
	 */
	PagoPlanDTO stateSubscription(@RequestBody PagoPlanDTO r) throws Exception;

	/**
	 * 
	 * @param idPagoPlan
	 * @return
	 * @throws Exception
	 */
	Boolean eliminar(Integer idPagoPlan) throws Exception;

}
