package com.giflo.view.service;

import java.util.List;
import java.util.Map;

import com.giflo.model.entity.Empresa;

public interface IEmpresaService {

	Map<String, Object> findByTipoAndName(Integer idTipoEmpresa, String search, String orderBy, Integer page,
			Integer size);

	List<Empresa> findByTipoAndNameMap();

}
