package com.giflo.view.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.giflo.model.dto.MessageDTO;
import com.giflo.model.dto.ReseniaDTO;
import com.giflo.model.entity.Empresa;
import com.giflo.model.entity.Parametro;
import com.giflo.model.entity.Resenia;
import com.giflo.model.entity.Usuario;
import com.giflo.model.repository.EmpresaRepository;
import com.giflo.model.repository.ParametroRepository;
import com.giflo.model.repository.ReseniaRepository;
import com.giflo.model.repository.UsuarioRepository;

@Service
public class ReseniaServiceImpl implements IReseniaService {

	@Autowired
	private ReseniaRepository reseniaRepository;

	@Autowired
	EmpresaRepository empresaRepository;

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	private IFirebaseMessageService firebaseMessageService;

	@Autowired
	private ParametroRepository parametroRepository;

	@Transactional
	@Override
	public ReseniaDTO registerReview(ReseniaDTO register) throws Exception {
		Empresa emp = empresaRepository.findById(register.getIdEmpresa()).orElse(null);
		Usuario user = usuarioRepository.findById(register.getIdUsuario()).orElse(null);
		if (emp != null && user != null) {
			Resenia res = reseniaRepository.findByEmpresaAndUsuario(emp, user);
			if (res != null) {
				res.setComentario(register.getComentario());
				res.setRate(register.getRate());
				res.setFechaRegistro(register.getFechaRegistro());
				res.setEmpresa(emp);
				res.setUsuario(user);
				reseniaRepository.save(res);
				return averageRate(emp);
			} else {
				Resenia review = new Resenia();
				review.setComentario(register.getComentario());
				review.setRate(register.getRate());
				review.setFechaRegistro(register.getFechaRegistro());
				review.setEmpresa(emp);
				review.setUsuario(user);
				reseniaRepository.save(review);
				Parametro paramUrlImagen = parametroRepository.buscarPorId("FIREABSE_PUSH_RESENIA_IMAGEN");
				if (paramUrlImagen != null && emp != null && emp.getUsuarios() != null
						&& !emp.getUsuarios().isEmpty()) {
					Usuario userComentario = emp.getUsuarios().get(0);
					MessageDTO messajeEmpleo = new MessageDTO();
					messajeEmpleo.setTipoNotificacion("RESENIA");
					messajeEmpleo.setUrlImage(paramUrlImagen.getValor());
					messajeEmpleo.setTitle(user.getEmpresa().getNombre().concat(" te hizo un comentario"));
					messajeEmpleo.setBody(register.getComentario());
					firebaseMessageService.sendPushMessageToUser(messajeEmpleo, userComentario);
				}
				return averageRate(emp);
			}
		}
		return new ReseniaDTO();
	}

	@Transactional
	@Override
	public ReseniaDTO recoverReview(Integer idEmpresa, Integer idUsuario) throws Exception {
		Empresa emp = empresaRepository.findById(idEmpresa).orElse(null);
		Usuario user = usuarioRepository.findById(idUsuario).orElse(null);
		ReseniaDTO reseniaDTO = new ReseniaDTO();
		if (emp != null && user != null) {
			Resenia res = reseniaRepository.findByEmpresaAndUsuario(emp, user);
			if (res != null) {
				reseniaDTO.setId(res.getId());
				reseniaDTO.setComentario(res.getComentario());
				reseniaDTO.setRate(res.getRate());
				reseniaDTO.setFechaRegistro(res.getFechaRegistro());
				reseniaDTO.setIdEmpresa(idEmpresa);
				reseniaDTO.setIdUsuario(idUsuario);
				return reseniaDTO;
			}
		}
		return reseniaDTO;
	}

	@Transactional
	@Override
	public ReseniaDTO deleteReview(Integer idResenia) throws Exception {
		Resenia res = reseniaRepository.findById(idResenia).orElse(null);
		if (res != null) {
			Empresa emp = res.getEmpresa();
			reseniaRepository.delete(res);
			return averageRate(emp);
		}
		return new ReseniaDTO();
	}

	@Override
	public Map<String, Object> findByEmpresa(Integer idEmpresa, Integer page, Integer size) {
		Pageable paging = PageRequest.of(page, size);
		Page<Resenia> padeRet = reseniaRepository.findByEmpresaResenia(idEmpresa, paging);
		List<ReseniaDTO> lista = padeRet.getContent().stream().map(r -> new ReseniaDTO(r)).collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		response.put("data", lista);
		response.put("page", padeRet.getNumber());
		response.put("totalPages", padeRet.getTotalPages());
		response.put("count", padeRet.getTotalElements());
		return response;
	}

	public ReseniaDTO averageRate(Empresa emp) {
		List<Resenia> lista = reseniaRepository.findByEmpresa(emp);
		ReseniaDTO resenia = new ReseniaDTO();
		Double suma = 0.0;
		Integer contador = lista.size();
		for (Resenia r : lista) {
			suma += r.getRate().doubleValue();
		}
		if (contador == 0) {
			emp.setContCalificacion(contador);
			emp.setCalificacion(new BigDecimal(suma));
			empresaRepository.save(emp);
			resenia.setCalificacion(new BigDecimal(suma));
			resenia.setCalificacionContador(contador);
			return resenia;
		} else {
			Double average = suma / contador;
			BigDecimal bAverage = new BigDecimal(average);
			bAverage = bAverage.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			emp.setContCalificacion(contador);
			emp.setCalificacion(bAverage);
			empresaRepository.save(emp);
			resenia.setCalificacion(bAverage);
			resenia.setCalificacionContador(contador);
			return resenia;
		}
	}

}
