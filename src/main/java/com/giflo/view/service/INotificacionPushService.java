package com.giflo.view.service;

import java.util.List;

import com.giflo.model.dto.UsuarioDTO;

public interface INotificacionPushService {
	
	/**
	 * Crear regsitro notificacion
	 * @param id origen
	 * @param id destino
	 * @return
	 * @throws Exception 
	 */
	Boolean crear(Integer origen, Integer destino) throws Exception;

	/**
	 * Eliminar registro notificacion
	 * @param id origen
	 * @param id destino
	 * @return
	 * @throws Exception
	 */
	
	Boolean eliminar(Integer origen, Integer destino) throws Exception;

	/**
	 * Obtiene los usuarios donde estoy suscrito
	 * @param destino
	 * @return
	 * @throws Exception
	 */
	List<UsuarioDTO> findByDestino(Integer destino) throws Exception;

	/**
	 * Obtiene los usuarios que se me suscribieron
	 * @param origen
	 * @return
	 * @throws Exception
	 */
	List<UsuarioDTO> findByOrigen(Integer origen) throws Exception;

}
