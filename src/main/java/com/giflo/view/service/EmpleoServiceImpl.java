package com.giflo.view.service;

import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.giflo.model.dto.MessageDTO;
import com.giflo.model.entity.Empleo;
import com.giflo.model.entity.Parametro;
import com.giflo.model.entity.Usuario;
import com.giflo.model.repository.EmpleoRepository;
import com.giflo.model.repository.ParametroRepository;
import com.giflo.model.repository.UsuarioRepository;

@Service
public class EmpleoServiceImpl implements IEmpleoService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private EmpleoRepository empleoRepository;

	@Autowired
	private IFirebaseMessageService firebaseMessageService;

	@Autowired
	private ParametroRepository parametroRepository;

	@Override
	public Empleo enviarNotificacion(Integer id) throws Exception {
		Empleo em = empleoRepository.filterById(id).get(0);
		if (em != null) {
			Usuario user = usuarioRepository.findById(em.getIdUsuario()).orElse(null);
			Parametro paramUrlImagen = parametroRepository.buscarPorId("FIREABSE_PUSH_EMPLEO_IMAGEN");
			if (paramUrlImagen != null && user != null) {
				MessageDTO messajeEmpleo = new MessageDTO();
				messajeEmpleo.setTipoNotificacion("EMPLEO");
				messajeEmpleo.setUrlImage(paramUrlImagen.getValor());
				messajeEmpleo.setTitle("Publicaron un nuevo empleo que podría interesarte.");
				messajeEmpleo
						.setBody(user.getEmpresa().getNombre().concat(" ").concat("busca un ").concat(em.getCargo()));
				firebaseMessageService.sendPushMessage(messajeEmpleo, em.getIdUsuario());
			}

		}
		return em;
	}

	@Override
	public Empleo save(Empleo empleo) throws Exception {
		Empleo em = new Empleo();
		Usuario user = usuarioRepository.findById(empleo.getIdUsuario()).orElse(null);
		if (empleo.getId() != null && empleo.getId() != -1) {
			em = empleoRepository.findById(empleo.getId()).orElse(null);
			em.setDescripcion(empleo.getDescripcion());
			em.setBeneficios(empleo.getBeneficios());
			em.setCargo(empleo.getCargo());
			em.setJornada(empleo.getJornada());
			empleoRepository.save(em);

		} else {
			if (user != null) {
				em.setDescripcion(empleo.getDescripcion());
				em.setBeneficios(empleo.getBeneficios());
				em.setCargo(empleo.getCargo());
				em.setEstado("ACTIVO");
				em.setJornada(empleo.getJornada());
				em.setUsuario(user);
				em.setFechaRegistro(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
				empleoRepository.save(em);
				Parametro paramUrlImagen = parametroRepository.buscarPorId("FIREABSE_PUSH_EMPLEO_IMAGEN");
				if (paramUrlImagen != null) {
					MessageDTO messajeEmpleo = new MessageDTO();
					messajeEmpleo.setTipoNotificacion("EMPLEO");
					messajeEmpleo.setUrlImage(paramUrlImagen.getValor());
					messajeEmpleo.setTitle("Publicaron un nuevo empleo que podría interesarte.");
					messajeEmpleo.setBody(
							user.getEmpresa().getNombre().concat(" ").concat("busca un ").concat(empleo.getCargo()));
					firebaseMessageService.sendPushMessage(messajeEmpleo, empleo.getIdUsuario());
				}

			}
		}

		return new Empleo(em);
	}

	@Override
	public Empleo cambiarEstado(Integer id, String estado) throws Exception {
		Empleo em = empleoRepository.findById(id).orElse(null);
		if (em != null) {
			em.setEstado(estado);
			empleoRepository.save(em);
		}
		return new Empleo(em);
	}

	@Override
	public Map<String, Object> filterActive(String filtro, Integer page, Integer size) {
		Pageable paging = PageRequest.of(page, size);
		filtro = filtro != null ? filtro.trim() : "";
		filtro = filtro.isEmpty() ? "%%" : filtro;
		Page<Empleo> padeRet = empleoRepository.filterActive(filtro, paging);
		List<Empleo> lista = padeRet.getContent().stream().map(p -> new Empleo(p)).collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		response.put("data", lista);
		response.put("page", padeRet.getNumber());
		response.put("totalPages", padeRet.getTotalPages());
		response.put("count", padeRet.getTotalElements());
		return response;
	}

	@Override
	public Map<String, Object> filterByUsuario(String filtro, Integer idUsuario, Integer page, Integer size) {
		Pageable paging = PageRequest.of(page, size);
		filtro = filtro != null ? filtro.trim() : "";
		filtro = filtro.isEmpty() ? "%%" : filtro;
		Page<Empleo> padeRet = empleoRepository.filterByUsuario(filtro, idUsuario, paging);
		List<Empleo> lista = padeRet.getContent().stream().map(p -> new Empleo(p)).collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		response.put("data", lista);
		response.put("page", padeRet.getNumber());
		response.put("totalPages", padeRet.getTotalPages());
		response.put("count", padeRet.getTotalElements());
		return response;
	}

}
