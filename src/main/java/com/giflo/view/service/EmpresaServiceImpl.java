package com.giflo.view.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.giflo.model.entity.Empresa;
import com.giflo.model.repository.EmpresaRepository;

@Service
public class EmpresaServiceImpl implements IEmpresaService {

	@Autowired
	EmpresaRepository empresaRepository;

	@Override
	public Map<String, Object> findByTipoAndName(Integer tipoEmpresa, String search, String orderBy, Integer page,
			Integer size) {
		Pageable paging = PageRequest.of(page, size,
				Sort.by("name".equalsIgnoreCase(orderBy) ? Sort.Direction.ASC : Sort.Direction.DESC,
						"name".equalsIgnoreCase(orderBy) ? "nombre" : "calificacion"));
		search = search != null ? search.trim() : "";
		Page<Empresa> padeRet = empresaRepository.findByTypeAndNombre(tipoEmpresa, search, paging);
		List<Empresa> lista = padeRet.getContent().stream().map(em -> new Empresa(em)).collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		response.put("data", lista);
		response.put("page", padeRet.getNumber());
		response.put("totalPages", padeRet.getTotalPages());
		response.put("count", padeRet.getTotalElements());
		return response;
	}

	@Override
	public List<Empresa> findByTipoAndNameMap() {
		return empresaRepository.findByTypeAndNombreAndHasMap().stream().map(em -> new Empresa(em))
				.collect(Collectors.toList());
	}

}
