package com.giflo.view.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.giflo.model.dto.UsuarioDTO;
import com.giflo.model.entity.NotificacionPush;
import com.giflo.model.entity.Usuario;
import com.giflo.model.repository.NotificacionPushRepository;
import com.giflo.model.repository.UsuarioRepository;

@Service
public class NotificacionPushServiceImpl implements INotificacionPushService {

	@Autowired
	private NotificacionPushRepository notificacionPushRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private IUsuarioService iUsuarioService;

	// Registrar o-d notificacion
	@Override
	public Boolean crear(Integer origen, Integer destino) throws Exception {
		Boolean result = false;
		Usuario us_origen = usuarioRepository.findById(origen).orElse(null);
		Usuario us_destino = usuarioRepository.findById(destino).orElse(null);
		if (us_origen != null && us_destino != null) {
			if (notificacionPushRepository.findByOrigenAndDestino(origen, destino) == null) {
				NotificacionPush np = new NotificacionPush();
				np.setOrigen(origen);
				np.setDestino(destino);
				notificacionPushRepository.save(np);
				result = true;
			}
		}
		return result;
	}

	// Eliminar o-d notificacion
	@Override
	public Boolean eliminar(Integer origen, Integer destino) throws Exception {
		Boolean result = false;
		NotificacionPush np = notificacionPushRepository.findByOrigenAndDestino(origen, destino);
		if (np != null) {
			notificacionPushRepository.delete(np);
			result = true;
		}
		return result;
	}

	@Override
	public List<UsuarioDTO> findByDestino(Integer destino) throws Exception {
		List<NotificacionPush> np = notificacionPushRepository.findByDestino(destino);
		List<UsuarioDTO> users = np.stream().map((u) -> {
			UsuarioDTO user = iUsuarioService.findById(u.getOrigen());
			return user;
		}).collect(Collectors.toList());
		return users;
	}

	@Override
	public List<UsuarioDTO> findByOrigen(Integer origen) throws Exception {
		List<NotificacionPush> np = notificacionPushRepository.findByOrigen(origen);
		List<UsuarioDTO> users = np.stream().map((u) -> {
			UsuarioDTO user = iUsuarioService.findById(u.getDestino());
			return user;
		}).collect(Collectors.toList());
		return users;
	}

}
