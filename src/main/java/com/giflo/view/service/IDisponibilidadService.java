package com.giflo.view.service;

import java.util.List;

import com.giflo.model.entity.Disponibilidad;
import com.giflo.model.enums.CatalogoEnum;

public interface IDisponibilidadService {

	List<Disponibilidad> guardar(List<Disponibilidad> listado);

	List<Disponibilidad> obtener(Integer idEmpresa, CatalogoEnum codigoUnidad);

	Boolean disponibilidadHoy(Integer idEmpresa);

	/**
	 * 
	 * @param idVariedad
	 * @param idPais
	 * @return
	 */
	List<Disponibilidad> obtenerDisponiblidadDetallePorTallos(Integer idVariedad, Integer idPais);

	/**
	 * 
	 * @param idVariedad
	 * @param idPais
	 * @param idLongitud
	 * @return
	 */
	List<Disponibilidad> obtenerDisponiblidadDetallePorBonches(Integer idVariedad, Integer idPais, Integer idLongitud);

	/**
	 * 
	 * @return
	 */
	List<Disponibilidad> getDisponibilidadPorBonches();

	/**
	 * 
	 * @return
	 */
	List<Disponibilidad> getDisponibilidadPorTallos();
}
