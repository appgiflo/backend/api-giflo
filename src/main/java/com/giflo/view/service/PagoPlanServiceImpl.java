package com.giflo.view.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.giflo.model.dto.CuponDTO;
import com.giflo.model.dto.PagoPlanDTO;
import com.giflo.model.entity.Cupon;
import com.giflo.model.entity.PagoPlan;
import com.giflo.model.entity.Parametro;
import com.giflo.model.entity.Plan;
import com.giflo.model.entity.Usuario;
import com.giflo.model.enums.ValorEstadoPagoPlanEnum;
import com.giflo.model.repository.CatalogoRepository;
import com.giflo.model.repository.CuponRepository;
import com.giflo.model.repository.PagoPlanRepository;
import com.giflo.model.repository.ParametroRepository;
import com.giflo.model.repository.PlanRepository;
import com.giflo.model.repository.UsuarioRepository;

@Service
public class PagoPlanServiceImpl implements IPagoPlanService {

	@Autowired
	private PagoPlanRepository pagoPlanRepository;

	@Autowired
	PlanRepository planRepository;

	@Autowired
	CatalogoRepository catalogoRepository;

	@Autowired
	CuponRepository cuponRepository;

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	private ParametroRepository parametroRepository;
	
	@Autowired
	private ISaldoService iSaldoService;

	@Override
	public PagoPlanDTO registrarPago(PagoPlanDTO register) throws Exception {

		List<PagoPlan> pendientes = pagoPlanRepository.findPendienteByIdUsuario(register.getIdUsuario());
		PagoPlanDTO ret = new PagoPlanDTO();
		if (pendientes.isEmpty() || ValorEstadoPagoPlanEnum.VIGENTE.equals(register.getEstado())) {

			Plan pla = planRepository.findById(register.getIdPlan()).orElse(null);
			Usuario user = usuarioRepository.findById(register.getIdUsuario()).orElse(null);
			Cupon cupon = null;
			BigDecimal descuento = BigDecimal.valueOf(0.0);
			BigDecimal subtotal = BigDecimal.valueOf(0.0);
			BigDecimal iva = BigDecimal.valueOf(0.0);
			BigDecimal total = BigDecimal.valueOf(0.0);
			try {
				cupon = cuponRepository.findByCodigoAndPlan(register.getCupon(), register.getIdPlan());
			} catch (Exception e) {
				cupon = null;
			}
			Parametro ivaParam = null;
			try {
				ivaParam = parametroRepository.buscarPorId("SUS_IVA");
			} catch (Exception e) {
				ivaParam = null;
			}
			if (pla != null && user != null) {
				String code = generateCodeSuscription().toUpperCase();
				PagoPlan pan = new PagoPlan();
				pan.setPlan(pla);
				pan.setUsuario(user);
				pan.setCupon(register.getCupon());
				pan.setEstado(
						register.getEstado() != null ? register.getEstado() : ValorEstadoPagoPlanEnum.PENDIENTE);
				total = pla.getValor();
				subtotal = total;
				if (cupon != null) {
					descuento = total.multiply(cupon.getPorciento().divide(BigDecimal.valueOf(100)));
					subtotal = total.subtract(descuento);
				}
				if (ivaParam != null) {
					iva = subtotal.multiply(BigDecimal.valueOf(Double.parseDouble(ivaParam.getValor())));
				}
				total = subtotal.add(iva);
				pan.setCodigoPago(code);
				pan.setDescuento(descuento);
				pan.setSubtotal(subtotal);
				pan.setIva(iva);
				pan.setTotal(total);
				pan.setFechaRegistro(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
				pan.setFechaPago(register.getFechaPago());
				if (ValorEstadoPagoPlanEnum.VIGENTE.equals(pan.getEstado())) {
					iSaldoService.guardar(user.getId(), pan.getPlan().getGrupo(), pan.getPlan().getCantidadContactos());
					pan.setReferenciaPlaceToPay(register.getReferenciaPlaceToPay());
					pan.setFechaPago(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
					usuarioRepository.save(user);
				}
				pagoPlanRepository.save(pan);
				ret.setCodigoPago(code);
			}
		} else {
			throw new Exception("Existe una suscripción aún PENDIENTE de pago.");
		}

		return ret;
	}

	@Transactional
	@Override
	public List<PagoPlanDTO> obtenerPago(Integer idUsuario) throws Exception {
		List<PagoPlan> lista = pagoPlanRepository.findByIdUsuario(idUsuario);
		List<PagoPlanDTO> pagoPlanLista = new ArrayList<>(0);
		if (lista != null) {
			for (PagoPlan pagoPlan : lista) {
				Cupon cupon = cuponRepository.findByCodigo(pagoPlan.getCupon());
				PagoPlanDTO obj;
				if (cupon != null) {
					obj = new PagoPlanDTO(pagoPlan, cupon);
				} else {
					obj = new PagoPlanDTO(pagoPlan);
				}
				pagoPlanLista.add(obj);
			}
		}
		return pagoPlanLista;
	}

	@Transactional
	@Override
	public PagoPlanDTO stateSubscription(PagoPlanDTO r) throws Exception {
		PagoPlan sus = pagoPlanRepository.findByIdPagoPlan(r.getId());
		if (sus != null) {
			Usuario user = sus.getUsuario();
			if (!sus.getEstado().equals(r.getEstado())) {
				if (ValorEstadoPagoPlanEnum.VIGENTE.equals(r.getEstado())) {
					iSaldoService.guardar(user.getId(), sus.getPlan().getGrupo(), sus.getPlan().getCantidadContactos());
					sus.setEstado(r.getEstado());
					sus.setDocumento(r.getDocumento());
					sus.setFechaPago(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
				} else if (ValorEstadoPagoPlanEnum.PENDIENTE.equals(r.getEstado())) {
					iSaldoService.descartar(user.getId(), sus.getPlan().getGrupo(), sus.getPlan().getCantidadContactos());
					sus.setEstado(r.getEstado());
					sus.setDocumento(null);
					sus.setFechaPago(null);
				} else if (ValorEstadoPagoPlanEnum.CADUCADO.equals(r.getEstado())) {
					sus.setEstado(r.getEstado());
				}
			}
			pagoPlanRepository.save(sus);
		}
		return new PagoPlanDTO(sus);

	}

	@Transactional
	@Override
	public CuponDTO couponApplied(Integer idPLan, String codigoCupon) throws Exception {
		CuponDTO ret = new CuponDTO();
		ret.setIdCupon(-1);
		ret.setDescuento(BigDecimal.valueOf(0.0));
		ret.setSubtotal(BigDecimal.valueOf(0.0));
		ret.setIva(BigDecimal.valueOf(0.0));
		Plan plan = planRepository.findById(idPLan).orElse(null);
		if (plan != null) {
			ret.setTotal(plan.getValor());
			Cupon cupon = cuponRepository.findByCodigo(codigoCupon);
			if (cupon != null) {
				Parametro ivaParam = null;
				try {
					ivaParam = parametroRepository.buscarPorId("SUS_IVA");
				} catch (Exception e) {
					ivaParam = null;
				}
				if (cupon.getIdPlan() == plan.getId()) {
					BigDecimal valor_iva = BigDecimal.valueOf(0.0);
					BigDecimal porcentaje_iva = BigDecimal.valueOf(0.0);
					if (ivaParam != null) {
						valor_iva = BigDecimal.valueOf(Double.parseDouble(ivaParam.getValor()));
						porcentaje_iva = valor_iva.divide(new BigDecimal(100.00));
					}

					BigDecimal porcentaje = cupon.getPorciento().divide(new BigDecimal(100.00));
					BigDecimal descuento = plan.getValor().multiply(porcentaje);
					BigDecimal total = plan.getValor();
					BigDecimal subtotal = total.subtract(descuento);
					BigDecimal iva = subtotal.multiply(porcentaje_iva);
					String message = cupon.getMensaje().replace("[DESCUENTO]", descuento.toString());
					message = message.replace("[PORCENTAJE]", cupon.getPorciento().toString());
					total = subtotal.add(iva);
					ret.setMensaje(message);
					ret.setIdCupon(cupon.getId());
					ret.setDescuento(descuento);
					ret.setSubtotal(subtotal);
					ret.setIva(iva);
					ret.setTotal(total);
				} else {
					ret.setIdCupon(-2);
					ret.setMensaje("El cupon no es aplicable para el plan seleccionado.");
				}
			} else {
				ret.setIdCupon(-1);
				ret.setMensaje("El cupon no es válido");
			}

		}
		return ret;
	}

	@Transactional
	@Override
	public Boolean eliminar(Integer idPagoPlan) throws Exception {
		PagoPlan sus = pagoPlanRepository.findById(idPagoPlan).orElse(null);
		if (sus != null) {
			if (ValorEstadoPagoPlanEnum.PENDIENTE.equals(sus.getEstado())) {
				pagoPlanRepository.delete(sus);
			} else
				throw new Exception("No es posible eliminar una suscripción VIGENTE");
		}
		return true;

	}

	private String generateCodeSuscription() {
		String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
		String lowerCaseLetters = RandomStringUtils.random(2, 97, 122, true, true);
		String numbers = RandomStringUtils.randomNumeric(3);
		String totalChars = RandomStringUtils.randomAlphanumeric(1);
		String combinedChars = upperCaseLetters.concat(lowerCaseLetters).concat(numbers).concat(totalChars);
		List<Character> pwdChars = combinedChars.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
		Collections.shuffle(pwdChars);
		String password = pwdChars.stream().collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
				.toString();
		return password;
	}

}
