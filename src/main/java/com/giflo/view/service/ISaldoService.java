package com.giflo.view.service;

import com.giflo.model.entity.Saldo;
import com.giflo.model.enums.GrupoSaldoEnum;

public interface ISaldoService {

	/**
	 * 
	 * @param idUsuario
	 * @param codigoTipo
	 * @param recarga
	 */
	public void guardar(Integer idUsuario, GrupoSaldoEnum codigoTipo,Integer recarga);
	public void descartar(Integer idUsuario, GrupoSaldoEnum codigoTipo,Integer recarga);
	
	/**
	 * 
	 * @param idUsuario
	 * @param codigoTipo
	 * @param recarga
	 */
	public void usar(Integer idUsuario, GrupoSaldoEnum codigoTipo,Integer recarga);

	Saldo obtener(Integer idUsuario, GrupoSaldoEnum codigoTipo);
}
