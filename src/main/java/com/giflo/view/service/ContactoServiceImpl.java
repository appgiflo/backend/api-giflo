package com.giflo.view.service;

import java.sql.Timestamp;
import java.util.GregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.giflo.model.entity.Contacto;
import com.giflo.model.entity.Usuario;
import com.giflo.model.enums.GrupoSaldoEnum;
import com.giflo.model.enums.ValorSiNoEnum;
import com.giflo.model.repository.ContactoRepository;
import com.giflo.model.repository.UsuarioRepository;

@Service
public class ContactoServiceImpl implements IContactoService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private ContactoRepository contactoRepository;
	
	@Autowired
	private ISaldoService iSaldoService;

	// Registrar o-d notificacion
	@Override
	public Boolean crear(Integer origen, Integer destino) throws Exception {
		Boolean result = false;
		Usuario usOrigen = usuarioRepository.findById(origen).orElse(null);
		Usuario usDestino = usuarioRepository.findById(destino).orElse(null);
		if (origen != null && destino != null) {
			Contacto cont = contactoRepository.findByOrigenAndDestino(usOrigen, usDestino);
			if (cont == null) {
				cont = new Contacto();
				cont.setOrigen(usOrigen);
				cont.setDestino(usDestino);
			}
			cont.setActivo(ValorSiNoEnum.SI);
			cont.setFechaRegistro(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
			contactoRepository.save(cont);
			iSaldoService.usar(origen, GrupoSaldoEnum.CONTACTO, 1);
			usuarioRepository.save(usOrigen);
			result = true;
		}
		return result;
	}

	// Eliminar o-d notificacion
	@Override
	public Boolean eliminar(Integer origen, Integer destino) throws Exception {
		Usuario usOrigen = usuarioRepository.findById(origen).orElse(null);
		Usuario usDestino = usuarioRepository.findById(destino).orElse(null);
		Boolean result = false;
		Contacto np = contactoRepository.findByOrigenAndDestino(usOrigen, usDestino);
		if (np != null) {
			np.setActivo(ValorSiNoEnum.NO);
			contactoRepository.save(np);
			result = true;
		}
		return result;
	}

}
