package com.giflo.view.service;

import java.util.List;

import com.giflo.model.dto.RegistroDTO;
import com.giflo.model.dto.UsuarioDTO;
import com.giflo.model.entity.Usuario;

public interface IUsuarioService {

	UsuarioDTO findByUsername(String username);

	/**
	 * Obtiene y actualzia el token de un usuario
	 * 
	 * @param id
	 * @param token
	 * @return
	 * @throws Exception
	 */
	UsuarioDTO findByIdCustom(Integer id, String token, String device) throws Exception;

	String register(RegistroDTO register) throws Exception;

	/**
	 * Servicio para recupera la contraseña
	 * 
	 * @param username
	 * @return
	 * @throws Exception
	 */
	String recuperarContrasenia(String username) throws Exception;

	/**
	 * Servicio para actualziar la contraseña
	 * 
	 * @param usernameEncrypt
	 * @param passwordeEncrypt
	 * @return
	 * @throws Exception
	 */
	String cambiarContrasenia(String usernameEncrypt, String passwordeEncrypt) throws Exception;

	/**
	 * 
	 * @param register
	 * @return
	 * @throws Exception
	 */
	String actualizar(RegistroDTO register) throws Exception;

	/**
	 * Obtiene los token de los usuarios que cumplan los criterios
	 * 
	 * @param idUser
	 * @param idTipoEmpresa
	 * @param idTipoPublicacion
	 * @return
	 * @throws Exception
	 */
	List<Usuario> findTokens(Integer idUser) throws Exception;

	/**
	 * Eliminar dispositivo de usuario
	 * 
	 * @param idUser
	 * @param device
	 * @return
	 * @throws Exception
	 */
	Boolean removeDevice(Integer idUser, String device) throws Exception;

	/**
	 * Busca todos los datos de una empresa
	 * 
	 * @param idEmpresa
	 * @return
	 * @throws Exception
	 */
	UsuarioDTO findByIdEmpresa(Integer idEmpresa, Integer idUser) throws Exception;

	/**
	 * 
	 * @param idUsuario
	 * @param longitud
	 * @param latitud
	 * @return
	 * @throws Exception
	 */
	String updateLocation(Integer idUsuario, String longitud, String latitud) throws Exception;

	/**
	 * Obtiene el usuario por su Id
	 * 
	 * @param idUsuario
	 * @return
	 */
	UsuarioDTO findById(Integer idUsuario);

	/**
	 * Obtiene los usuarios para diferentes al usuario que contenga el id, filtra
	 * por nombre,apellido y nombre de empresa
	 * 
	 * @param filtro
	 * @param idUser
	 * @return
	 * @throws Exception
	 */
	List<UsuarioDTO> search(String filtro, Integer idUser) throws Exception;

	/**
	 * Valida el token de un dispositivo
	 * 
	 * @param device
	 * @throws Exception
	 */
	String validToken(String device) throws Exception;

	/**
	 * Obtiene usuarios del app
	 * 
	 * @param filtro
	 * @return
	 * @throws Exception
	 */
	List<UsuarioDTO> searchActive(String filtro) throws Exception;

	/**
	 * Obtiene la información resumida de un usuario
	 * 
	 * @param idUsuario
	 * @return
	 */
	UsuarioDTO findByIdResume(Integer idUsuario);

}
