
# Giflo Proyecto


### Java Spring template project


### CI/CD with Auto DevOps

##Instalación letsencrypt

1.  sudo yum install software-properties-common amazon-linux-extras install epel
2.  sudo amazon-linux-extras install epel
3.  sudo yum install certbot-nginx
4.  sudo certbot certonly --standalone -d apiprod.giflo.keysist.net

## Certificar de seguridad mediante letsencrypt

openssl pkcs12 -export -in fullchain.pem -inkey privkey.pem -out prod.p12 -name api-giflo  -CAfile chain.pem  -caname root

&copy; Desarrollado por Keyssist